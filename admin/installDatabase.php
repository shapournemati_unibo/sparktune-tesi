<?php require_once(dirname(dirname(__FILE__)) . '/load-config.php');
  require_once ABSPATH.'connection.php';

  $query = "DROP TABLE IF EXISTS `Attribute`;
            DROP TABLE IF EXISTS `Table`;
            DROP TABLE IF EXISTS `Database`;
            DROP TABLE IF EXISTS `Throughput`;
            DROP TABLE IF EXISTS `Cluster`;
            DROP TABLE IF EXISTS `User`;

            CREATE TABLE `User` (
              `memberID` int(11) NOT NULL AUTO_INCREMENT,
              `username` varchar(255) NOT NULL,
              `password` varchar(255) NOT NULL,
              `email` varchar(255) NOT NULL,
              `active` varchar(255) NOT NULL,
              `resetToken` varchar(255) DEFAULT NULL,
              `resetComplete` varchar(3) DEFAULT 'No',
              PRIMARY KEY (`memberID`)
            );

            CREATE TABLE `Cluster` (
              `cl_id` int(11) NOT NULL AUTO_INCREMENT,
              `cl_userId` int(11) NOT NULL,
              `cl_name` varchar(255) NOT NULL,
              `cl_#R` int(11) NOT NULL,
              `cl_#RN` int(11) NOT NULL,
              `cl_#C` int(11) NOT NULL,
              `cl_rf` int(11) NOT NULL DEFAULT '3',
              `cl_#SB` int(11) NOT NULL DEFAULT '200',
              `cl_sCmp` double NOT NULL,
              `cl_fCmp` double NOT NULL,
              `cl_hSel` double NOT NULL,
              `cl_intraRSpeed` double NOT NULL,
              `cl_extraRSpeed` double NOT NULL,
              PRIMARY KEY (`cl_id`),
              KEY `fk_Cluster_user_idx` (`cl_userId`),
              CONSTRAINT `fk_Cluster_user` FOREIGN KEY (`cl_userId`) REFERENCES `User` (`memberID`) ON DELETE CASCADE ON UPDATE CASCADE
            );

            CREATE TABLE `Throughput` (
              `th_clusterId` int(11) NOT NULL,
              `th_type` char(2) NOT NULL,
              `th_process` int(11) NOT NULL,
              `th_value` double NOT NULL,
              KEY `fk_Throughput_cluster_idx` (`th_clusterId`),
              CONSTRAINT `fk_Throughput_cluster` FOREIGN KEY (`th_clusterId`) REFERENCES `Cluster` (`cl_id`) ON DELETE CASCADE ON UPDATE CASCADE
            );

            CREATE TABLE `Database` (
              `db_id` int(11) NOT NULL AUTO_INCREMENT,
              `db_userId` int(11) NOT NULL,
              `db_name` varchar(255) NOT NULL,
              PRIMARY KEY (`db_id`),
              KEY `Database_idx` (`db_userId`,`db_name`),
              CONSTRAINT `fk_Database_user` FOREIGN KEY (`db_userId`) REFERENCES `User` (`memberID`) ON DELETE CASCADE ON UPDATE CASCADE
            );

            CREATE TABLE `Table` (
              `tbl_id` int(11) NOT NULL AUTO_INCREMENT,
              `tbl_databaseId` int(11) NOT NULL,
              `tbl_name` varchar(255) NOT NULL,
              `tbl_cardinality` int(11) NOT NULL,
              `tbl_size` bigint(20) NOT NULL,
              `tbl_#partitions` int(11) NOT NULL,
              `tbl_compressionEnabled` tinyint(1) NOT NULL DEFAULT '0',
              PRIMARY KEY (`tbl_id`),
              KEY `fk_DatabaseTable_idx` (`tbl_databaseId`),
              CONSTRAINT `fk_DatabaseTable` FOREIGN KEY (`tbl_databaseId`) REFERENCES `Database` (`db_id`) ON DELETE CASCADE ON UPDATE CASCADE
            );

            CREATE TABLE `Attribute` (
              `att_id` int(11) NOT NULL AUTO_INCREMENT,
              `att_tableId` int(11) NOT NULL,
              `att_name` varchar(255) NOT NULL,
              `att_cardinality` int(11) NOT NULL,
              `att_avgLength` double NOT NULL,
              `att_highestValue` varchar(255) DEFAULT NULL,
              `att_lowestValue` varchar(255) DEFAULT NULL,
              PRIMARY KEY (`att_id`),
              KEY `atttribute_idx` (`att_tableId`,`att_name`),
              CONSTRAINT `fk_Attribute_table` FOREIGN KEY (`att_tableId`) REFERENCES `Table` (`tbl_id`) ON DELETE CASCADE ON UPDATE CASCADE
            );";

  $conn = openConnection();
  echo $query;
  echo "</br>";
  echo "</br>";
  echo "</br>";
  if(mysqli_multi_query($conn, $query)) {
    echo "CREATO!";
  }
  else {
    echo "ERROR! Create query error";
    echo mysqli_error($conn);
  }
  closeConnection($conn);

?>
