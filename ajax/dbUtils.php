<?php
$g_link = false;


function GetMyConnection()
{
	$dbname = 'XXX';
	
	return GetMyOdsConnection($dbname);
}

function GetMyOdsConnection($dbname)
{
	global $g_link;
	if( $g_link )
		return $g_link;
		
	// Connecting, selecting database
	$g_link = pg_connect("host=137.204.72.88 dbname=$dbname user=XXX password=XXX") or die('Could not connect to server.' );
	if (!$g_link) {
		$e = pg_last_error();
		trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
	}
	
	return $g_link;
}

function CleanUpDB()
{
	global $g_link;
	if( $g_link != false )
		pg_close($g_link);
	$g_link = false;
}

function logString($s)
{
	file_put_contents('../log/log.txt',date('Y-m-d H:i:s ').$s. PHP_EOL, FILE_APPEND);
}  
function logQuery($s)
{
	file_put_contents('../log/logquery.txt',date('Y-m-d H:i:s ').$s. PHP_EOL, FILE_APPEND);
} 

/* Transform from DD/MM/YYYY to YYYY-MM-DD */
function dateTransform($d){
	$dateArray = explode('/',$d);
	return $dateArray[2] . '-' . $dateArray[1] . '-' . $dateArray[0];
}

/* Transform from DD/MM/YYYY to YYYY-MM-DD */
function dateTransform2($d){
	$dateArray = explode('-',$d);
	return $dateArray[2] . '/' . $dateArray[1] . '/' . $dateArray[0];
}
?>