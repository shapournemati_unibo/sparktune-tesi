<?php
/*
Questo file viene richiamato da tutte le pagine principali che hanno bisogno del pannello dei parametri.
La logica è: tutti i parametri vengono caricati in un file 'filters.json'
- se il file esiste, caricali semplicemente dal file
- se il file non esiste, crealo
Il file viene creato convertendo in JSON l'array associativo $result:
- gli indici dell'array sono i nomi dei parametri
- i valori sono gli elenchi dei possibili valori
Se cambiano i parametri che devono essere caricati, cancellare il file e fare F5 sul browser

ATTENZIONE: alcuni parametri dipendono dall'utente
- Opz 1: Creo un file per utente - non scala, troppi file
- Opz 2: Creo un unico file - non scala, file troppo grande
- Opz 3: Eventuali parametri "globali" li gestisco come solito, gli altri li ricarico tutte le volte - fattibile se le query non sono complesse
*/
include ('dbUtils.php');


$filename = 'filters.json';
$file = $filename;
$current = @file_get_contents($file);

// if(empty($current)){
if(empty($current) || isset($_POST['resetFilters'])){
// if(isset($_POST['selOpts']) && !empty($_POST['selOpts'])) {

	$dbc = GetMyConnection();

	$result = array();
	
	$selOpts[] = array("table" => "uso_agricolo_2016", "name" => "farming", "lookup" => "raggruppam", "where" => " ");
	$selOpts[] = array("table" => "prov2011", "name" => "provinces", "lookup" => "nome_pro", "where" => "where cod_reg = 8");
	
	foreach($selOpts as $selOpt){ 
		$query = "select distinct(".$selOpt['lookup'].") as \"name\" from ".$selOpt['table']." ".$selOpt['where']." order by 1"; 
		
		$res = pg_query($dbc,$query);
		$rows = array();
		while($r = pg_fetch_array($res)) {
			$rows[] = $r;
		}
		$result[$selOpt['name']] = $rows;
	}
	
	// Get available dates with bounding box
	$query = "select date, ST_XMin(env), ST_XMax(env), ST_YMin(env), ST_YMax(env) 
							,ST_X(ST_Transform(ST_GeomFromText('POINT('||ST_XMin(env)||' '||ST_YMin(env)||')',32632),4326)) ST_XMin4326
							,ST_X(ST_Transform(ST_GeomFromText('POINT('||ST_XMax(env)||' '||ST_YMax(env)||')',32632),4326)) ST_XMax4326
							,ST_Y(ST_Transform(ST_GeomFromText('POINT('||ST_XMin(env)||' '||ST_YMin(env)||')',32632),4326)) ST_YMin4326
							,ST_Y(ST_Transform(ST_GeomFromText('POINT('||ST_XMax(env)||' '||ST_YMax(env)||')',32632),4326)) ST_YMax4326
				from (
					select substr(cast(date_trunc('day',sensing_date) as text),1,10) date, 
						ST_Envelope(ST_Union(rast)) env
					from tiles
					where zoom_level = 7
					group by date_trunc('day',sensing_date) 
					order by 1
				) x";
	$res = pg_query($dbc,$query);
	$rows = array();
	while($r = pg_fetch_array($res)) {
		$rows[] = $r;
	}
	$result['availableDates'] = $rows;
	
	
	
	
	$rows = array();
	$rows[] = array("name" => "NDVI (absolute)", "value" => "ndvi_abs");
	$rows[] = array("name" => "NDVI (relative)", "value" => "ndvi_rel");
	$rows[] = array("name" => "NDVI RED (absolute)", "value" => "ndvired_abs");
	$rows[] = array("name" => "NDVI RED (relative)", "value" => "ndvired_rel");
	$rows[] = array("name" => "NDWI (absolute)", "value" => "ndwi_abs");
	$rows[] = array("name" => "NDWI (relative)", "value" => "ndwi_rel");
	$rows[] = array("name" => "MTCI (absolute)", "value" => "mtci_abs");
	$rows[] = array("name" => "MTCI (relative)", "value" => "mtci_rel");
	$result['index'] = $rows;
	
	

	$current = json_encode($result);
	CleanUpDb();
	
	file_put_contents($file, $current);
}

print $current;
	
?>