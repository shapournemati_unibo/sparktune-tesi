<?php

require_once(dirname(__FILE__) . '/load-config.php');
include_once(dirname(__FILE__) . '/src/view/GrammarDerivationView.php');
include_once(dirname(__FILE__) . '/src/GPSJGrammarParser/PhisicalPlan.php');
include_once(dirname(__FILE__) . '/src/costModel/CostModel.php');
include_once(dirname(__FILE__) . '/src/costModel/Database.php');
include_once(dirname(__FILE__) . '/src/costModel/Table.php');
include_once(dirname(__FILE__) . '/user/DatabaseQuery.php');


$_SESSION['currentWorkload'] = 0;

  const cloudProvider = array('Amazon','Google');

  const secondsInAMonth = 86400 * 30;
  //Google cost (monthly)
  const googleRamCost = 2.79;
  const googleCoreCost = 20.79;
  const googleStorageCost = 80;

  //Amazon cost
  const amazonCoreCost = 0.078; //hourly
  const amazonMemoryCost = 0.053; //gb per month

  const key_action = 'action';
  const key_query = 'selected_query';
  const key_cluster = 'key_cluster';
  const key_database = 'key_database';
  const key_nRE = 'nRE';
  const key_nEC = 'nEC';
  const key_workload = 'workload';
  const key_cloud = 'cloud';

  $action = NULL;
  $query = NULL;
  $cluster = NULL;
  $database = NULL;
  $nRE = NULL;
  $nEC = NULL;
  $nRack = NULL;
  $nNodes = NULL;
  $workload = NULL;
  $cloud = NULL;

  if (isset($_REQUEST[key_action])) {
    $action = $_REQUEST[key_action];
  }
  if (isset($_REQUEST[key_query])) {
    $query = $_REQUEST[key_query];
  }
  if (isset($_REQUEST[key_cluster])) {
    $cluster = $_REQUEST[key_cluster];
    $clusterParams = DatabaseQuery::getCluster($cluster);
    if (!is_null($clusterParams)) {
      $nRE = (int) $clusterParams[DatabaseValues::CL_N_RN];
      $nEC = (int) $clusterParams[DatabaseValues::CL_N_C];
      $nRack = (int) $clusterParams[DatabaseValues::CL_N_R];
      $nNodes = $nRE * $nRack;
    }
  }
  if (isset($_REQUEST[key_database])) {
    $database = $_REQUEST[key_database];
  }

  if (isset($_REQUEST[key_workload])) {
    $workload = $_REQUEST[key_workload];
  }

  if (isset($_REQUEST[key_cloud])) {
    $cloud = $_REQUEST[key_cloud];
  }



 ?>

<!doctype html>
<html>
  <head>
    <title>Spark cost</title>

    <!-- Il file contiene una serie di librerie utilizzate in tutto l'applcativo-->
    <?php
      include ABSTEMPLATEPATH . 'head.php';
      GrammarDerivationView::printHeaders();
    ?>

	<!-- JAVASCRIPT PAGINA -->
	<script src="template/js/sparkcost.js" type="text/javascript"></script>
	<script src="template/js/index.js" type="text/javascript"></script>
  <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
	<!--<script src="js/maintenance.js" type="text/javascript"></script>-->

	</head>
	<body style="<!--background-image: url('img/bg10.png');--> position: relative; ">

		<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
      <?php
        include ABSTEMPLATEPATH . 'header.php';
        echo '<link rel="stylesheet" href="'.TEMPLATEPATH.'/styles/style.css"/>';

      ?>

		<div id="contTopicCharts" class="" style="padding:0 15 5 15;">

			<div id="contTopicSelection" class="row" style="margin-bottom: 5px; padding:0px;">
			</div>


			<div class="row">

				<div id="selections" class="col-xs-2" style="">
          <div id="chartOptions" class="cont contLeft">
          	<div class="chartTitle">
          		<span id="chartOptionsTitleText" >Parameters</span>
          	</div>
          	<div id="chartOptionsCont" class="chart" style="padding: 5px;">

          			<div class="panel-group" id="selectionOptions" style="margin-bottom:0px;">

          				<div id="allOptionsContainer">
          					<?php
                    # Print cluster parameter
                    echo '<form class="form">' . PHP_EOL;
                      echo '<label class="card_label">';
                        echo "Cluster: " . printFormClusterParameters($cluster) . PHP_EOL;
                      echo '</label>';
                      # Print database parameter
                      echo '<label class="card_label">';
                        echo "Database: " . printFormDatabaseParameters($database) . PHP_EOL;
                      echo '</label>';
                      # Print the selection for a workload query

                      echo '<label class="card_label">';
                        echo "Workload: " . printFormWorkloadParameters($workload) . PHP_EOL;
                      echo '</label>';

                      echo '<label class="card_label">';
                        echo "Cloud Provider: " . printFormCloudParameters($cloud) . PHP_EOL;
                      echo '</label>';

                      echo '<div class="actions">' . PHP_EOL;
                        # Print the buttons for the cost computation and optimization
                        echo '<input type="submit" name=action value="Compute" />' . PHP_EOL;
                      echo '</div>' . PHP_EOL;
                    echo '</form>' . PHP_EOL;
                     ?>
          				</div>

          			</div>

          	</div>
          </div>
				</div>

				<div id="topicCharts" class="col-xs-10">
					<div class="col-xs-12" style="padding:0px 5px;">
            <?php
            if ($action=='Compute') {
              if (isset($_REQUEST[key_cluster])
                  && isset($_REQUEST[key_database])
                  && isset($_REQUEST[key_cloud])
                  && isset($_REQUEST[key_workload])) {

            if($_SESSION['currentWorkload'] != $workload){
                $_SESSION['currentWorkload'] = $workload;
                $_SESSION['sparkVersion'] = DatabaseQuery::getWorkloadVersion($workload);
                $_SESSION['workload'] = DatabaseQuery::getQueries($workload);
            }

            echo '<div class="cont">';
            echo '<div class="chartTitle" style="margin-top:1em;">
             <span>Cost Analysis</span><span class="titleStats"></span>
             <span style="float:right; font-size:12px; margin: 2px 5px;"></span>
           </div>';
           echo '<div id="graphDiv">';
           echo '</div>';
           echo '</div>';

           echo '<div class="cont">';
           echo '<div class="chartTitle" style="margin-top:1em;">
            <span>Cost/Time Comparison</span><span class="titleStats"></span>
            <span style="float:right; font-size:12px; margin: 2px 5px;"></span>
          </div>';
          echo '<div id="graph2D">';
          echo '</div>';
          echo '</div>';

            $xArray = array();
            for($i = 1; $i <= $nEC; $i++){
              $xArray[] = $i;
            }

            $yArray = array();
            for($i = 1; $i <= $nNodes; $i++){
              $yArray[] = $i;
            }

            $queries = $_SESSION['workload'];
            $derivationArr = array();
            foreach ($queries as $key => $value) {
              $derivationArr[] = (new PhisicalPlan($value, $_SESSION['sparkVersion']))->getGrammarDerivationJson();
            }

            $result = calculateMulti3DCost($nNodes,$nEC,$derivationArr,$cloud);

            ?>
            <script type="text/javascript" src="graphrendering/plotlyPlot/graph.js"></script>
            <script>
            setXName('Cores');
            setYName('Executors');
            setZName('Cost($)');
            setXCoords(<?php echo json_encode( $xArray )?>);
            setYCoords(<?php echo json_encode( $yArray )?>);
            setZCoordsByValue(<?php echo json_encode( $result[1] )?>);
            renderGraph();

            plot2DGraph(<?php echo json_encode( $result[3] )?>,<?php echo json_encode( $result[4] )?>,<?php echo json_encode( $result[2] )?>,'Cost/Time');
            </script>
            <?php

          }
        }
             ?>
					</div>
				</div>

			</div>
		</div>
		</div>


	</body>
</html>

<?php

/*
Calculate the cost for our graph
*/
function calculateSingle3DCost($nNodes, $nEC,$grammarDerivation){
  $result = array();
  for($x = 1; $x <= $nNodes; $x++){
    $arr = array();
    for($z = 1; $z <= $nEC; $z++){
      $database = getDatabase($_REQUEST[key_database]);
      $parameters = getClusterParameters($_REQUEST[key_cluster], $x, $z);
      $costModel = new CostModel($parameters,$database,$grammarDerivation);
        $arr[] = ($costModel->getCost())/60; //cost in minutes
    }
    $result[] = $arr;
  }
  return $result;
}

function calculateMulti3DCost($nNodes, $nEC,$grammarDerivationArr,$cloud){
  $result = array();
  $costArray = array();
  $timeArray = array();
  $flatCost = array();
  $flatTime = array();
  $tagArray = array();

  for($x = 1; $x <= $nNodes; $x++){
    $arrC = array();
    $arrT = array();
    for($z = 1; $z <= $nEC; $z++){
      $cost = 0;
      foreach ($grammarDerivationArr as $derivation){
        $database = getDatabase($_REQUEST[key_database]);
        $parameters = getClusterParameters($_REQUEST[key_cluster], $x, $z);
        $costModel = new CostModel($parameters,$database,$derivation);
          $cost += ($costModel->getCost())/60; //cost in minutes
      }

      $arrT[] = $cost;
      if($cloud == cloudProvider[0]){
        //amazon
        $arrC[] = calculateAmazonCost($z,$x,$cost,1500);
      } else {
        //google
        $arrC[] = calculateGoogleCost(32,$z,$x,$cost);
      }

      $flatTime[] = $cost;
      if($cloud == cloudProvider[0]){
        //amazon
        $flatCost[] = calculateAmazonCost($z,$x,$cost,1500);
      } else {
        //google
        $flatCost[] = calculateGoogleCost(32,$z,$x,$cost);
      }
      $tagArray[] = 'Executors: ' . $x . '<br>Cores: ' . $z;
    }
    $timeArray[] = $arrT;
    $costArray[] = $arrC;
  }
  $result[] = $timeArray;
  $result[] = $costArray;
  $result[] = $tagArray;
  $result[] = $flatTime;
  $result[] = $flatCost;

  return $result;
}

function calculateGoogleCost($ram, $cores,$nNodes,$cost){
  return (((googleRamCost*$ram)/secondsInAMonth) + (($cores*googleCoreCost)/secondsInAMonth) + (googleStorageCost/secondsInAMonth))*$nNodes*$cost*60;
}

function calculateAmazonCost($cores,$nNodes,$cost,$dimension){
  return (((amazonCoreCost*$cores)/60) + ((amazonMemoryCost*$dimension)/secondsInAMonth))*$nNodes*$cost*60;
}

function printFormClusterParameters($defaultCluster = NULL) {
  $res = '<select name="' . key_cluster . '">';
  $res .= '<option selected disabled>Choose the cluster</option>';
  foreach (DatabaseQuery::getClusters(LoginStatus::getUserId()) as $key => $value) {
    $name = $value[DatabaseValues::CL_Name];
    $id = $value[DatabaseValues::CL_ClusterId];
    $res .= '<option ';
      if ($defaultCluster && $id == $defaultCluster)
        $res .= 'selected="selected"';
      $res .= 'value="' . $key . '">';
      $res .= $name;
  }
  $res .= '</select>';
  return $res;
}

function printFormDatabaseParameters($defaultDatabase = NULL) {
  $res = '<select name="' . key_database . '">';
  $res .= '<option selected disabled>Choose the database</option>';
  foreach (DatabaseQuery::getDatabases(LoginStatus::getUserId()) as $key => $value) {
    $name = $value[DatabaseValues::DB_Name];
    $id = $value[DatabaseValues::DB_ID];
    $res .= '<option ';
      if ($defaultDatabase && $id == $defaultDatabase)
        $res .= 'selected="selected"';
      $res .= 'value="' . $key . '">';
      $res .= $name;
  }
  $res .= '</select>';
  return $res;
}

function printFormWorkloadParameters($defaultWorkload = NULL){
  $res = '<select name="' . key_workload . '">';
  $res .= '<option selected disabled>Choose the workload</option>';
  foreach (DatabaseQuery::getWorkloads(LoginStatus::getUserId()) as $key => $value) {
    $name = $value[DatabaseValues::WL_NAME];
    $id = $value[DatabaseValues::WL_ID];
    $res .= '<option ';
      if ($defaultWorkload && $id == $defaultWorkload)
        $res .= 'selected="selected"';
      $res .= 'value="' . $key . '">';
      $res .= $name;
  }
  $res .= '</select>';
  return $res;
}

function printFormCloudParameters($defaultCloud = NULL){
  $res = '<select name="' . key_cloud . '">';
  $res .= '<option selected disabled>Choose the cloud</option>';
  foreach (cloudProvider as $key => $value) {
    $res .= '<option ';
      if ($defaultCloud && $value == $defaultCloud)
        $res .= 'selected="selected"';
      $res .= 'value="' . $value . '">';
      $res .= $value;
  }
  $res .= '</select>';
  return $res;
}


function printWorkloadSelection($cluster,$database,$action,$workload){
  $res = "";
  if($_SESSION['currentWorkload'] != $workload){
    $_SESSION['currentWorkload'] = $workload;
    $_SESSION['sparkVersion'] = DatabaseQuery::getWorkloadVersion($workload);
    $_SESSION['workload'] = DatabaseQuery::getQueries($workload);
  }


  $res .= '<div class="actions" style="display:flex">';
  $buttonSize = 100/count($_SESSION['workload']);
  $queryArr = $_SESSION['workload'];
  $i = 1;
  foreach ($queryArr as $key => $value) {
    $res .= '<a style="width:'.$buttonSize.'%" href="'.URL.'performance_analysis.php'. getUrlOptions($cluster,$database,$action,$workload).'&chosen_query='.$key.'"> Query '.($i++).'</a>';
  }

  $res .= '</div>';

  return $res;
}

function getUrlOptions($cluster,$database,$action,$workload){
  $urlOptions = '?';
  $urlOptions .= key_cluster . '=' . $cluster.'&';
  $urlOptions .= key_database . '=' . $database . '&';
  $urlOptions .= key_workload . '=' . $workload . '&';
  $urlOptions .= key_action . '=' . $action;

  return $urlOptions;
}

function getDatabase($databaseId) {
  $databaseSql = DatabaseQuery::getDatabase($databaseId);
  $database = new Database($databaseSql[DatabaseValues::DB_Name]);
  $tablesSql = DatabaseQuery::getTables($databaseSql[DatabaseValues::DB_ID]);
  foreach ($tablesSql as $tableId => $tableSql) {
    // $name, $cardinality, $size, $nPartitions
    $table = new Table(
      $tableSql[DatabaseValues::TBL_Name],
      $tableSql[DatabaseValues::TBL_Cardinality],
      $tableSql[DatabaseValues::TBL_Size],
      $tableSql[DatabaseValues::TBL_N_Partitions]
    );
    $attributesSql = DatabaseQuery::getAttributes($tableId);
    foreach ($attributesSql as $attributeId => $attributeSql) {
      // $name, $cardinality, $length, $high, $low
      $attribute = new Attribute(
        $attributeSql[DatabaseValues::ATT_Name],
        $attributeSql[DatabaseValues::ATT_Cardinality],
        $attributeSql[DatabaseValues::ATT_AvgLength],
        $attributeSql[DatabaseValues::ATT_Highest],
        $attributeSql[DatabaseValues::ATT_Lowest]
      );
      $table->addAttribute($attribute);
    }
    $database->addTable($table);
  }
  return $database;
}

function getClusterParameters($clusterId, $nRE, $nEC) {
  $cluster = DatabaseQuery::getCluster($clusterId);
  if (!is_null($cluster)) {
    $parameters = new CostFunctionParameters(
      (int) $cluster[DatabaseValues::CL_N_R],
      (int) $cluster[DatabaseValues::CL_N_RN],
      (int) $cluster[DatabaseValues::CL_N_C],
      (int) $cluster[DatabaseValues::CL_Rf],
      function ($nProc) use ($clusterId) { // discR(#Proc)
        $values = DatabaseQuery::getThroughputs($clusterId, DatabaseValues::TR_Type_DR);
        $maxProc = max(array_keys($values));
        if ($nProc <= $maxProc) {

        return $values[$nProc-1] *1024*1024;
        } else {

          return $values[$maxProc-1] *1024*1024;
        }
      },
      function ($nProc) use ($clusterId) { // discW(#Proc)
        $values = DatabaseQuery::getThroughputs($clusterId, DatabaseValues::TR_Type_DW);
        $maxProc = max(array_keys($values));
        if ($nProc <= $maxProc) {
          return $values[$nProc-1] *1024*1024;
        } else {
          return $values[$maxProc-1] *1024*1024;
        }
      },
      function ($nProc) use ($cluster) { // netI(#Proc)
        return $nProc <= 0 ? $cluster[DatabaseValues::CL_IntraRSpeed] : $cluster[DatabaseValues::CL_IntraRSpeed] / $nProc;
      },
      function ($nProc) use ($cluster) { // netE(#Proc)
        return $nProc <= 0 ? $cluster[DatabaseValues::CL_ExtraRSpeed] : $cluster[DatabaseValues::CL_ExtraRSpeed] / $nProc;
      },
      (int) $cluster[DatabaseValues::CL_N_SB],
      (float) $cluster[DatabaseValues::CL_SCmp],
      (float) $cluster[DatabaseValues::CL_FCmp],
      (float) $cluster[DatabaseValues::CL_HSel],
      $nRE,
      $nEC);
  }
  return $parameters;
}

function seconds($a) {
  $sec = $a;
  $unim = array("s","m","h");
  $c = 0;
  while ($a>=60 && $c<sizeof($unim)-1) {
    $c++;
    $a = $a/60;
  }
  return number_format($a,($c ? 2 : 0),",",".")." ".$unim[$c] . " (" . formatNumber($sec) . "s)";
}

function formatNumber($number) {
  return number_format($number, 2, '.'," ");
}
 ?>
