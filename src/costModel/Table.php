<?php
include_once(dirname(__FILE__) . '/Math.php');

class PredicateOperator {
  const min='<';
  const minOrEqual='<=';
  const max='>';
  const maxOrEqual='>=';
  const equal='=';
  public static function getOperators() {
    return array(
      PredicateOperator::minOrEqual,
      PredicateOperator::maxOrEqual,
      PredicateOperator::min,
      PredicateOperator::max,
      PredicateOperator::equal
    );
  }
  public static function invertOperator($operator) {
    switch ($operator) {
      case PredicateOperator::min:
        return PredicateOperator::max;
        break;
      case PredicateOperator::minOrEqual:
        return PredicateOperator::maxOrEqual;
        break;
      case PredicateOperator::max:
        return PredicateOperator::min;
        break;
      case PredicateOperator::maxOrEqual:
        return PredicateOperator::minOrEqual;
        break;
      case PredicateOperator::equal:
        return PredicateOperator::equal;
        break;
      default:
        return NULL;
        break;
    }
  }
}

class SelectivityPredicate {

  private $selectivity = 1;
  private $tableName;

  public function __construct($predicate, $table) {
    $this->tableName = $table->getName();
    $conditions = explode('&&', $predicate); // All the conditions are connected by the AND operator
    foreach ($conditions as $key => $condition) {
      $csplit = SelectivityPredicate::splitCondition($condition, PredicateOperator::getOperators());
      $left = $csplit[0];
      $right = $csplit[1];
      $operator = $csplit[2];

      //correct errors in physical plan parsing
      $left = str_replace("(", "", $left);
      $left = str_replace(")", "", $left);
      $right = str_replace("(", "", $right);
      $right = str_replace(")", "", $right);

      if($this->tableName == ''){
      }

      // Fetch attribute features
      if (array_key_exists($left, $table->getAttributesAls()) && array_key_exists($right, $table->getAttributesAls())) {  // compare between two attributes
        $attr1 = $table->getAttribute($left);
        $attr2 = $table->getAttribute($right);
      } else if (array_key_exists($left, $table->getAttributesAls())) {
        $attr = $table->getAttribute($left);
        $then = $right;
      } else if (array_key_exists($right, $table->getAttributesAls())) {
        $attr = $table->getAttribute($right);
        $then = $left;
        $operator = PredicateOperator::invertOperator($operator);
      }
      // Compute selectivity
      if (isset($attr)) {
        if ($operator == PredicateOperator::min || $operator == PredicateOperator::minOrEqual) {

          $this->selectivity *= SelectivityPredicate::selOpThen($attr, $then, function($lowest, $value, $highest) {
            if ($lowest <= $value && $value <= $highest) {
              return ($value - $lowest) / ($highest - $lowest);
            } else {
              return 0;
            }
          });
        } else if ($operator == PredicateOperator::max || $operator == PredicateOperator::maxOrEqual) {

          $this->selectivity *= SelectivityPredicate::selOpThen($attr, $then, function($lowest, $value, $highest) {
            if ($lowest <= $value && $value <= $highest) {
              return ($highest - $value) / ($highest - $lowest);
            } else {
              return 0;
            }
          });
        } else if ($operator == PredicateOperator::equal) {
          $this->selectivity *= 1/$attr->getCardinality();
        }
      }
      if (isset($attr1) && $attr2 != NULL && $operator == PredicateOperator::equal) {
        $this->selectivity *= 1/max($attr1->getCardinality(), $attr2->getCardinality());
      }
    }
  }

  public function getSel() {
    if($this->tableName == 'orders'){
      return $this->selectivity;
    }
    return $this->selectivity;
  }

  private static function splitCondition($condition, $operations) {
    for ($i=0; $i < sizeof($operations); $i++) {
      $res = explode($operations[$i], $condition);
      if (sizeof($res) == 2) {
        $res[] = $operations[$i];
        return $res;
      }
    }
  }

  private static function selOpThen($attribute, $value, callable $function) {
    $high = $attribute->getHigh();
    $low = $attribute->getLow();
    // Convert string to number
    $numericVal = doubleVal($value);
    if ($numericVal) {  // If numeric
      $value = $numericVal;
    } elseif (($dateVal = DateTime::createFromFormat('Y-m-d', $value)) !== FALSE) { // If date
      $high = date_format(DateTime::createFromFormat('Y-m-d', $high), 'U');
      $low = date_format(DateTime::createFromFormat('Y-m-d', $low), 'U');
      $value = date_format($dateVal, 'U');
    } else { // If string
      // Convert strings to int value of the first char (to upper case)
      $high = ord(ucfirst($high));
      $low = ord(ucfirst($low));
      $value = ord(ucfirst($value));
    }
    return $function($low, $value, $high);
  }

}

class JoinPredicate {

  private $jCard;
  private $jSize;

  private $firstTable;

  public function __construct($predicate, $t1, $t2) {
    $this->firstTable = $t1->getName();
    $leftKeys = $predicate[0];
    $rightKeys = $predicate[1];
    $leftKeysCard = 1;
    $rightKeysCard = 1;
    for ($i=0; $i < sizeof($leftKeys); $i++) {
      $left = $leftKeys[$i];
      $right = $rightKeys[$i];
      // Fetch attributes
      if (array_key_exists($left, $t1->getAttributesAls()) && array_key_exists($right, $t2->getAttributesAls())) {
        $attr1 = $t1->getAttribute($left);
        $attr2 = $t2->getAttribute($right);
      } else if(array_key_exists($left, $t2->getAttributesAls()) && array_key_exists($right, $t1->getAttributesAls())) {
        $attr1 = $t1->getAttribute($right);
        $attr2 = $t2->getAttribute($left);
      }
      if (isset($attr1) && $attr1!=NULL) {
        $leftKeysCard *= $attr1->getCardinality();
        $rightKeysCard *= $attr2->getCardinality();
      }
    }
    // -----------------------
    // Compute join parameters
    // -----------------------
    // Join cardinality
    // (product of the tables cardinality) / (max (key_attributes))
    $this->jCard = (( $t1->getCardinality() * $t2->getCardinality() ) / ( max($leftKeysCard, $rightKeysCard) ));
    // Join size
    $length1 = Math::sumArray($t1->getAttributes(), function($a) {
      return $a->getLength();
    });
    $length2 = Math::sumArray($t2->getAttributes(), function($a) {
      return $a->getLength();
    });
    $this->jSize = $this->jCard * ($length1 + $length2);
  }

  public function getJCard() {
    if($this->firstTable == 'SC(5)'){
      return $this->jCard;
    }
    return $this->jCard;
  }

  public function getJSize() {
    if($this->firstTable == 'SC(5)'){
      return $this->jSize;
    }
    return $this->jSize;
  }
}

class Attribute {

  private $name;
  private $cardinality;
  private $length;
  private $high;
  private $low;

  public function __construct($name, $cardinality, $length, $high, $low)
  {
    $this->name = $name;
    $this->cardinality = $cardinality;
    $this->length = $length;
    $this->high = $high;
    $this->low = $low;
  }

  public static function get($name) {
    return new Attribute($name, NULL, NULL, NULL, NULL);
  }

  public function getName()
  {
    return $this->name;
  }

  public function getCardinality()
  {
    return $this->cardinality;
  }

  public function getLength()
  {
    return $this->length;
  }

  public function getHigh()
  {
    return $this->high;
  }

  public function getLow()
  {
    return $this->low;
  }

}

class Table {

  private $name;
  private $cardinality;
  private $size;
  private $nPartitions;
  private $isCompressed;
  private $nAttributes;

  private $attributes;
  private $attributesAls;

  public function __construct($name, $cardinality, $size, $nPartitions)
  {
    $this->name = $name;
    $this->cardinality = $cardinality;
    $this->size = $size;
    $this->nPartitions = $nPartitions;
    $this->isCompressed = false;
    $this->nAttributes = 0;
    $this->attributes = array();
    $this->attributesAls = array();
  }

  public function setCompressed($isCompressed) {
    $this->isCompressed = $isCompressed;
  }

  public function isCompressed() {
    return $this->isCompressed;
  }

  public function getName()
  {
    return $this->name;
  }

  public function getCardinality()
  {
    return $this->cardinality;
  }

  public function getSize()
  {
    return $this->size;
  }

  public function getPSize()
  {
    return $this->size / $this->nPartitions;
  }

  public function getNPartitions()
  {
    return $this->nPartitions;
  }

  public function getNAttributes()
  {
    return $this->nAttributes;
  }

  public function getAttributesAls()
  {
    return $this->attributesAls;
  }

  public function getAttributes()
  {
    return $this->attributes;
  }

  public function getAttribute($name)
  {
    if (strpos($name, '#')) { // Alias
      if (array_key_exists($name, $this->getAttributesAls())) {
        return $this->getAttributesAls()[$name];
      }
    } else { // Standard column
      if (array_key_exists($name, $this->getAttributes())) {
        return $this->getAttributes()[$name];
      }
    }
  }

  public function addAttribute($attribute, $key = NULL)
  {
    if (is_null($key)) {
      $key = $attribute->getName();
    }
    $this->attributes[$key] = $attribute;
    $this->nAttributes++;
  }

  public function addAttributeByAlias($attribute, $alias)
  {
    if (is_null($alias)) {
      $key = $attribute->getName();
    } else {
      $key = explode("#", $alias)[0];
    }
    $this->addAttribute($attribute, $key);
    $this->addAttributeAlias($alias);
  }

  public function addAttributeAlias($alias) {
    if (!is_null($alias)) {
      $attributeName = explode("#", $alias)[0];
      $this->attributesAls[$alias] = $this->getAttribute($attributeName);
    }
  }

}

?>
