<?php

class CostFunctionParameters {

   # Cluster parameters
   private $nR;
   private $nRN;
   private $nC;
   private $rf;
   private $fcDiscR;
   private $fcDiscW;
   private $fcNetI;
   private $fcNetE;
   private $nSB;
   private $sCmp;
   private $fCmp;
   private $hSel;
   # Spark parameters
   private $nRE;
   private $nEC;
   # Percentage factor
   private $discFactor;
   private $netFactor;

   function __construct($nR, $nRN, $nC, $rf,
          callable $discR, callable $discW, callable $netI, callable $netE,
          $nSB, $sCmp, $fCmp, $hSel, $nRE, $nEC) {
       $this->nR = $nR;
       $this->nRN = $nRN;
       $this->nC = $nC;
       $this->rf = $rf;
       $this->fcDiscR = $discR;
       $this->fcDiscW = $discW;
       $this->fcNetI = $netI;
       $this->fcNetE = $netE;
       $this->nSB = $nSB;
       $this->sCmp = $sCmp;
       $this->fCmp = $fCmp;
       $this->hSel = $hSel;

       $this->nRE = $nRE;
       $this->nEC = $nEC;

       $this->discFactor = 1;
       $this->netFactor = 1;
   }

   # Cluster parameters
   function nR() {
       return $this->nR;
   }

   function nRN() {
       return $this->nRN;
   }

   function nN() {
       return $this->nR * $this->nRN;
   }

   function nC() {
       return $this->nC;
   }

   function rf() {
       return $this->rf;
   }

   function discR($nProc) {
       return call_user_func($this->fcDiscR, $nProc)*($this->discFactor);
   }

   function discW($nProc) {
       return call_user_func($this->fcDiscW, $nProc)*($this->discFactor);
   }

   function netI($nProc) {
       return call_user_func($this->fcNetI, $nProc)*($this->netFactor);
   }

   function netE($nProc) {
       return call_user_func($this->fcNetE, $nProc)*($this->netFactor);
   }

   function nSB() {
       return $this->nSB;
   }

   function sCmp() {
       return $this->sCmp;
   }

   function fCmp() {
       return $this->fCmp;
   }

   function hSel() {
       return $this->hSel;
   }

   # Spark parameters
   function nRE() {
       return $this->nRE;
   }

   function nE() {
       return $this->nRE * $this->nR;
   }

   function nEC() {
       return $this->nEC;
   }

   function alterDiscFactor($factor){
     $this->discFactor = $factor;
   }

    function alterNetFactor($factor){
      $this->netFactor = $factor;
    }

   public function __toString() {
     return var_export($this, true);
   }

}

?>
