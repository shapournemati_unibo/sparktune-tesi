<?php
include_once(dirname(__FILE__) . '/CostFunctionParameters.php');
include_once(dirname(__FILE__) . '/Math.php');
include_once(dirname(__FILE__) . '/Table.php');
include_once(dirname(__FILE__) . '/BasicBrick.php');

abstract class Details {
  const GENERIC = 'generic';
  const TABLE_OUT = 'tbl_out';
  const TABLE_IN = 'tbl_in';
}

abstract class TaskType {

  protected static function addTableDetails(&$array, &$table) {
    if ($table == NULL) {
      echo "Input table null in task: " . debug_backtrace()[1]['class'];
    } else {
      $array[$table->getName()] = array();
      $array[$table->getName()]["Cardinality"] = $table->getCardinality();
      $array[$table->getName()]["Size"] = $table->getSize();
      $array[$table->getName()]["N. partitions"] = $table->getNPartitions();
    }
  }

  private $id;
  protected $params;
  private $computed = false;
  private $cost = NULL;
  private $nodeCost = NULL;
  private $resultTable = NULL;
  protected $readT = 0;
  protected $writeT = 0;
  protected $networkT = 0;
  protected $functionDetails = array();
  protected $basicBricks = array();

  protected function __construct($id) {
    $this->id = $id;
  }

  public function __toString() {
    return get_class($this) . "($this->id)";
  }

  public function getId() {
    return $this->id;
  }

  public function getFunctionDetails() {
    return $this->functionDetails;
  }

  public function getBasicBricks() {
    return $this->basicBricks;
  }

  public function setParameters(&$costFunctionParameters)
  {
    $this->params = $costFunctionParameters;
  }

  public function askRecomputation() {
    $this->computed = false;
  }

  private function justComputed() {
    $this->computed = true;
  }

  private function needsRecomputation() {
    return !$this->computed;
  }

  private function checkAndRecompute() {
    if ($this->needsRecomputation()) {
      $this->nodeCost = $this->computeNodeCost();
      $this->cost = $this->computeCost($this->nodeCost);
      $this->resultTable = $this->computeResultTable();

      $this->functionDetails[Details::TABLE_OUT] = array();
      $this->functionDetails[Details::TABLE_OUT]["Cardinality"] = $this->resultTable->getCardinality();
      $this->functionDetails[Details::TABLE_OUT]["Size"] = $this->resultTable->getSize();
      $this->functionDetails[Details::TABLE_OUT]["N. partitions"] = $this->resultTable->getNPartitions();

      $this->justComputed();
    }
  }

  public function getCost() {
    $this->checkAndRecompute();
    return $this->cost;
  }

  public function getNodeCost() {
    $this->checkAndRecompute();
    return $this->nodeCost;
  }

  public function getResultTable() {
    $this->checkAndRecompute();
    return $this->resultTable;
  }

  abstract protected function computeCost($nodeCost);
  abstract protected function computeNodeCost();
  abstract protected function computeResultTable();

  public function readAffectsCost() {
    $this->checkAndRecompute();
    return false;
  }

  public function getReadT() {
    $this->checkAndRecompute();
    return $this->readT;
  }

  public function writeAffectsCost() {
    $this->checkAndRecompute();
    return false;
  }

  public function getWriteT() {
    $this->checkAndRecompute();
    return $this->writeT;
  }

  public function networkAffectsCost() {
    $this->checkAndRecompute();
    return false;
  }

  public function getNetworkT() {
    $this->checkAndRecompute();
    return $this->networkT;
  }

  /*
  *
  * $t the table
  * $cols the attributes
  */
  protected static function proj($t, $cols) {
    if (is_null($cols)) {
      return 1;
    }
    if (is_null($t) || is_null($cols)) {
      return NULL;
    }
    $cLen = 0;
    $tLen = 0;
    foreach ($cols as $key => $a) {
      $cLen += $a->getLength();
    }
    foreach ($t->getAttributes() as $key => $a) {
      $tLen += $a->getLength();
    }
    return $cLen/$tLen;
  }

  /*
   *
   * $nTuples
   * $nGroups
   */
   protected static function group($nTuples, $nGroups) {
     if (is_null($nGroups)) {
       return 1;
     } else if($nTuples == 0){
       return 0;
     }
     return Math::cardenas($nTuples, $nGroups) / $nTuples;
   }
}

/*
 * ------------------------------------------------------------
 * Scan
 * ------------------------------------------------------------
 */
class SC extends TaskType {

  protected $table;
  protected $pred;
  protected $cols;
  protected $groups;
  protected $write;
  protected $readAffectsCost;
  protected $writeAffectsCost;
  protected $networkAffectsCost;

   public function __construct($id, $table, $pred, $cols, $groups, $write) {
     parent::__construct($id);
     $this->table = $table;
     $this->pred = new SelectivityPredicate($pred, $table);
     $this->cols = $cols;
     if ($groups == NULL) {
       $groups = array();
     }
     $this->groups = $groups;
     $this->write = $write;
   }

   public static function get($id, &$costFunctionParameters, $table, $pred, $cols, $groups, $write = false) {
     $tmp = new SC($id, $table, $pred, $cols, $groups, $write);
     $tmp->setParameters($costFunctionParameters);
     return $tmp;
   }

   public function computeCost($nodeCost) {
     return $nodeCost;
   }

   public function computeResultTable() {
     $this->functionDetails[Details::TABLE_IN] = array();
     TaskType::addTableDetails($this->functionDetails[Details::TABLE_IN], $this->table);

     $cardinality = $this->table->getCardinality() * $this->pred->getSel()
        * TaskType::group($this->table->getCardinality() * $this->pred->getSel(), Math::prodArray($this->groups, function($a) {
          return $a->getCardinality();
        }));
     $size = $this->WSize() * $this->nTableP();
     $nPartitions = $this->nTableP();
     $table = new Table(get_class($this)."(".$this->getId().")", $cardinality, $size, $nPartitions);
     foreach ($this->cols as $name => $value) {
       $table->addAttributeByAlias($value, $name);
     }

     return $table;
   }

   public function readAffectsCost() {
     parent::readAffectsCost();
     return $this->readAffectsCost;
   }

   public function writeAffectsCost() {
     parent::writeAffectsCost();
     return $this->writeAffectsCost;
   }

   public function networkAffectsCost() {
     parent::networkAffectsCost();
     return $this->networkAffectsCost;
   }

   protected function nTableP() {
     $res = $this->table->getSize() / $this->table->getPSize();
     if ($this->table->isCompressed()) {
       $res *= $this->params->fCmp();
     }
     return $res;
   }

   protected function P($X) {
     switch ($X) {
       case DataPlace::L:
         $case = "L";
         $res = $this->Pl();
         break;
       case DataPlace::R:
         $case = "R";
         $res = $this->Pr();
         break;
       case DataPlace::C:
         $case = "C";
         $res = $this->Pc();
         break;
     }
     return $res;
   }

   protected function Pl() {  //EDIT

     if(Math::binomialCoefficient($this->params->nN(), $this->params->nE()) <= 0){
       return 1;
     } else {
       return 1
         - (Math::binomialCoefficient($this->params->nN() - $this->params->rf(), $this->params->nE())
            / Math::binomialCoefficient($this->params->nN(), $this->params->nE()));
     }


   }

   protected function Pc() {
     return Math::sum(1, min($this->params->nR(), $this->params->rf()), function($x) {
       return $this->Ppart($x) * Math::sum(1, min($this->params->nR(), $this->params->nE()), function($y) use ($x) {
         return $this->Pexe($y) * (Math::binomialCoefficient($this->params->nR() - $x, $y)
                                    / Math::binomialCoefficient($this->params->nR(), $y));
       });
     });
   }

   protected function Pr() {
     return 1 - $this->Pl() - $this->Pc();
   }

   protected function Ppart($x) { //EDIT
     if(Math::binomialCoefficient($this->params->nN(), $this->params->rf()) <= 0){
       return 0;
     }
     return Math::binomialCoefficient($this->params->nR(), $x) * Math::sum(0, $x, function($j) use ($x) {


         return pow(-1, $j)
              * Math::binomialCoefficient($x, $j)
              * Math::binomialCoefficient($this->params->nRN() * ($x - $j), $this->params->rf())
              / Math::binomialCoefficient($this->params->nN(), $this->params->rf());



     });
   }

   protected function Pexe($y) { //EDIT
     if(Math::binomialCoefficient($this->params->nN(), $this->params->nE()) <= 0){
       return 0;
     }
     return Math::binomialCoefficient($this->params->nR(), $y) * Math::sum(0, $y, function($j) use ($y) {
         return pow(-1, $j) * Math::binomialCoefficient($y, $j)
                            * Math::binomialCoefficient($this->params->nRN() * ($y - $j), $this->params->nE())
                            / Math::binomialCoefficient($this->params->nN(), $this->params->nE());


     });
   }

   protected function RSize() {
     return $this->table->getPSize();
   }

   protected function WSize() {
     //echo $this->pred->getSel();
     return $this->table->getPSize()
            * $this->pred->getSel()
            * TaskType::proj($this->table, $this->cols)
            * TaskType::group($this->table->getCardinality() * $this->pred->getSel(),
                            Math::prodArray($this->groups, function($a) {
                               return $a->getCardinality();
                             }));
   }

   protected function computeNodeCost() {
     $this->readAffectsCost = array();
     $this->writeAffectsCost = array();
     $this->readT = array();
     $this->writeT = array();
     $this->networkAffectsCost = array();
     $this->networkT = array();

     $waves = ceil($this->nTableP() / ($this->params->nE() * $this->params->nEC()));
     $cost = Math::sum(0 /* L */, 2 /* C */, function($X) use($waves) {
        switch ($X) {
          case 0:
            $caseName = "local";
            break;
          case 1:
            $caseName = "rack";
            break;
          case 2:
            $caseName = "cluster";
            break;
        }
        $P = $this->P($X);
        $rBB = Read::get($this->params, $this->RSize(), $X);
        $this->basicBricks["Read $caseName"] = &$rBB;

        //the basic read brick returns a network time??
        if($rBB->isRead() == 1){
          $this->readT[$X] = $rBB->getCost() * $P * $waves;
          $this->networkT[$X] = $rBB->otherTime() * $P * $waves;
        } else {
          $this->readT[$X] = $rBB->otherTime() * $P * $waves;
          $this->networkT[$X] = $rBB->getCost() * $P * $waves;
        }


        if ($this->write) {
          $wBB = Write::get($this->params, $this->WSize());
          $this->basicBricks["Write $caseName"] = &$wBB;
          $this->writeT[$X] = $wBB->getCost() * $P * $waves;
        } else {
          $this->writeT[$X] = 0;
        }

        if ($this->groups) { // If grouping

          if($rBB->isRead() == 1){
            $f = $this->readT[$X] + $this->writeT[$X];
            $this->readAffectsCost[$X] = true;
            $this->networkAffectsCost[$X] = false;
          } else {
            $f = $this->networkT[$X] + $this->writeT[$X];
            $this->networkAffectsCost[$X] = true;
            $this->readAffectsCost[$X] = false;
          }

          $this->writeAffectsCost[$X] = true;
        } else {  // If not grouping
          if ($this->readT[$X] > $this->writeT[$X]) {

            if($rBB->isRead() == 1){
              $f = $this->readT[$X];
              $this->readAffectsCost[$X] = true;
              $this->networkAffectsCost[$X] = false;
            } else {
              $f = $this->networkT[$X];
              $this->networkAffectsCost[$X] = true;
              $this->readAffectsCost[$X] = false;
            }

            $this->writeAffectsCost[$X] = false;
          } else {
            $f = $this->writeT[$X];
            $this->readAffectsCost[$X] = false;
            $this->writeAffectsCost[$X] = true;
            $this->networkAffectsCost[$X] = false;
          }
        }

        // Cost details
        $this->functionDetails[$X][Details::GENERIC] = array();
        $this->functionDetails[$X][Details::GENERIC]["Number of waves"] = $waves * $P;
        $this->functionDetails[$X][Details::GENERIC]["Task read size"] = $this->RSize();
        $this->functionDetails[$X][Details::GENERIC]["Task write size"] = $this->WSize();
        $this->functionDetails[$X][Details::GENERIC]["Selectivity"] = $this->pred->getSel();

        return  $f;
      });
     return $cost;
   }

}

/*
 * ------------------------------------------------------------
 * Scan and broadcast
 * ------------------------------------------------------------
 */
class SB extends SC {

  public function __construct($id, $table, $pred, $cols) {
    parent::__construct($id, $table, $pred, $cols, NULL, false /* Write (pipe) parameter is exclusive of the SCAN operation */);
    $this->writeAffectsCost = array(false, false, false);
    $this->writeT = array(0,0,0);
  }

public static function get($id, &$costFunctionParameters, $table, $pred, $cols, $groups = NULL, $write=false /* groups and write are useless: just for matching parent method*/) {
    $tmp = new SB($id, $table, $pred, $cols);
    $tmp->setParameters($costFunctionParameters);
    return $tmp;
  }

  public function computeResultTable() {
    $this->functionDetails[Details::TABLE_IN] = array();
    TaskType::addTableDetails($this->functionDetails[Details::TABLE_IN], $this->table);

    $cardinality = $this->table->getCardinality() * $this->pred->getSel();
    $size = $this->WSize() * $this->nTableP();
    $nPartitions = $this->nTableP();
    $table = new Table(get_class($this)."(".$this->getId().")", $cardinality, $size, $nPartitions);
    foreach ($this->cols as $name => $value) {
      $table->addAttributeByAlias($value, $name);
    }
    return $table;
  }

  private function BrSize() {
    return $this->table->getPSize() * $this->pred->getSel() * TaskType::proj($this->table, $this->cols);
  }

  protected function computeNodeCost() {
    $this->readAffectsCost = array();
    $this->networkAffectsCost = array();
    $this->readT = array();
    $this->networkT = array();

    $waves = ceil($this->nTableP() / ($this->params->nE() * $this->params->nEC()));
    $cost = Math::sum(0 /* L */, 2 /* C */, function($X) use($waves) {
      switch ($X) {
        case 0:
          $caseName = "local";
          break;
        case 1:
          $caseName = "rack";
          break;
        case 2:
          $caseName = "cluster";
          break;
      }
      $P = $this->P($X);
      $rBB = Read::get($this->params, $this->RSize(), $X);
      $this->basicBricks["Read $caseName"] = &$rBB;
      $this->readT[$X] = $rBB->getCost() * $P * $waves;
      $bBB = Broadcast::get($this->params, $this->BrSize());
      $this->basicBricks["Broadcast $caseName"] = &$bBB;
      $this->networkT[$X] = $bBB->getCost() * $P * $waves;

      if ($this->readT[$X] > $this->networkT[$X]) {
        if($rBB->isRead() == 1){
          $f = $this->readT[$X];
          $this->networkT[$X] = max($this->networkT[$X],$rBB->otherTime());
          $this->readAffectsCost[$X] = true;
          $this->networkAffectsCost[$X] = false;
        } else {
          $f = $this->readT[$X]; //in this case it is network time
          $this->readT[$X] = $rBB->otherTime(); //the real read cost
          $this->readAffectsCost[$X] = false;
          $this->networkAffectsCost[$X] = true;
        }
      } else {
        $f = $this->networkT[$X];
        $this->readAffectsCost[$X] = false;
        $this->networkAffectsCost[$X] = true;
      }

      // Cost details
      $this->functionDetails[$X][Details::GENERIC] = array();
      $this->functionDetails[$X][Details::GENERIC]["Number of waves"] = $waves * $P;
      $this->functionDetails[$X][Details::GENERIC]["Task read size"] = $this->RSize();
      $this->functionDetails[$X][Details::GENERIC]["Task broadcast size"] = $this->BrSize();
      $this->functionDetails[$X][Details::GENERIC]["Selectivity"] = $this->pred->getSel();
      return  $f;
    });
    return $cost;
  }
}

/*
 * ------------------------------------------------------------
 * Shuffle Join
 * ------------------------------------------------------------
 */
class SJ extends TaskType {

   private $expr1;
   private $expr2;
   private $t1;
   private $t2;
   private $pred;
   private $cols;
   private $groups;
   private $write;
   private $joinTable;

   private $mem_jSize;
   private $mem_jProj;
   private $mem_jCard;

   protected $readAffectsCost;
   protected $writeAffectsCost;
   protected $networkAffectsCost;

   public function __construct($id, $expr1, $expr2, $pred, $cols, $groups, $write) {
     parent::__construct($id);
     $this->expr1 = $expr1;
     $this->expr2 = $expr2;
     $this->t1 = $expr1->getResultTable();
     $this->t2 = $expr2->getResultTable();
     $this->pred = new JoinPredicate($pred, $this->t1, $this->t2);
     $this->cols = $cols;
     $this->groups = $groups;
     $this->write = $write;
     $this->joinTable = new Table("join", 0, 0, 0);
     foreach ($this->t1->getAttributesAls() as $name => $attr) {
       $this->joinTable->addAttributeByAlias($attr, $name);
     }
     foreach ($this->t2->getAttributesAls() as $name => $attr) {
       $this->joinTable->addAttributeByAlias($attr, $name);
     }
   }

   public static function get($id, &$costFunctionParameters, $expr1, $expr2, $pred, $cols, $groups, $write) {
     $tmp = new SJ($id, $expr1, $expr2, $pred, $cols, $groups, $write);
     $tmp->setParameters($costFunctionParameters);
     return $tmp;
   }

   public function computeCost($nodeCost) {
     return $this->expr1->getCost() + $this->expr2->getCost() + $nodeCost;
   }

   public function computeResultTable() {
     $this->functionDetails[Details::TABLE_IN] = array();
     TaskType::addTableDetails($this->functionDetails[Details::TABLE_IN], $this->t1);
     TaskType::addTableDetails($this->functionDetails[Details::TABLE_IN], $this->t2);

     $cardinality = $this->pred->getJCard()
        * TaskType::group($this->pred->getJCard(), Math::prodArray($this->groups, function($a) {
          return $a->getCardinality();
        }));
     $size = $this->WSize() * $this->params->nSB();
     $nPartitions = $this->params->nSB();
     $table = new Table(get_class($this)."(".$this->getId().")", $cardinality, $size, $nPartitions);
     if ($this->cols != NULL) {
       foreach ($this->cols as $name => $value) {
         $table->addAttributeByAlias($value, $name);
       }
     } else {
       foreach ($this->joinTable->getAttributesAls() as $name => $value) {
         $table->addAttributeByAlias($value, $name);
       }
     }
     return $table;
   }

   public function readAffectsCost() {
     parent::readAffectsCost();
     return $this->readAffectsCost;
   }

   public function writeAffectsCost() {
     parent::writeAffectsCost();
     return $this->writeAffectsCost;
   }

   public function networkAffectsCost(){
     parent::networkAffectsCost();
     return $this->networkAffectsCost;
   }

   private function RSize() {
     return ($this->t1->getSize() + $this->t2->getSize()) / $this->params->nSB();
   }

   private function WSize() {
     if (!$this->write) {
       return 0;
     }
     $this->mem_jSize = $this->pred->getJSize();
     $this->mem_jCard = $this->pred->getJCard();
     $this->mem_jProj = TaskType::proj($this->joinTable, $this->cols);
     return (($this->mem_jSize * $this->mem_jProj) / $this->params->nSB())
        * TaskType::group($this->mem_jCard,
                          Math::prodArray($this->groups, function($a) {
                           return $a->getCardinality();
                         }));
   }

   protected function computeNodeCost() {
     $waves = ceil($this->params->nSB() / ($this->params->nE() * $this->params->nEC()));
     $rBB = SRead::get($this->params, $this->RSize());
     $this->basicBricks["SRead"] = &$rBB;
     if($rBB->isRead() == 1){
       $this->readT = $rBB->getCost() * $waves;
       $this->networkT = $rBB->otherTime() * $waves;
       $this->readAffectsCost = true;
       $this->networkAffectsCost = false;
     } else {
       $this->readT = $rBB->otherTime() * $waves;
       $this->networkT = $rBB->getCost() * $waves;
       $this->readAffectsCost = false;
       $this->networkAffectsCost = true;
     }

     $wBB = Write::get($this->params, $this->WSize());
     $this->basicBricks["Write"] = &$wBB;
     $this->writeT = $wBB->getCost() * $waves;
     $this->writeAffectsCost = true;

     // Cost details
     $this->functionDetails[Details::GENERIC]["Number of waves"] = $waves;
     $this->functionDetails[Details::GENERIC]["Task read size"] = $this->RSize();
     $this->functionDetails[Details::GENERIC]["Task write size"] = $this->WSize();
     $this->functionDetails[Details::GENERIC]["jSize"] = $this->mem_jSize;
     $this->functionDetails[Details::GENERIC]["jCard"] = $this->mem_jCard;
     $this->functionDetails[Details::GENERIC]["jProj"] = $this->mem_jProj;

     if($this->readT > $this->networkT){
       return $this->readT + $this->writeT;
     } else {
       return $this->networkT + $this->writeT;
     }


   }
}

/*
 * ------------------------------------------------------------
 * Broadcast Join
 * ------------------------------------------------------------
 */
class BJ extends TaskType {

   private $expr1;
   private $expr2;
   private $t1;
   private $t2;
   private $pred;
   private $cols;
   private $groups;
   private $write;
   private $buildRight;
   private $joinTable;

   private $mem_jSize;
   private $mem_jProj;
   private $mem_jCard;

   public function __construct($id, $expr1, $expr2, $pred, $cols, $groups, $write, $buildRight) {
     parent::__construct($id);
     $this->expr1 = $expr1;
     $this->expr2 = $expr2;
     $this->t1 = $expr1->getResultTable();
     $this->t2 = $expr2->getResultTable();
     $this->pred = new JoinPredicate($pred, $this->t1, $this->t2);;
     $this->cols = $cols;
     $this->groups = $groups;
     $this->write = $write;
     $this->buildRight = $buildRight;
     $this->joinTable = new Table("join", 0, 0, 0);
     foreach ($this->t1->getAttributesAls() as $name => $attr) {
       $this->joinTable->addAttributeByAlias($attr, $name);
     }
     foreach ($this->t2->getAttributesAls() as $name => $attr) {
       $this->joinTable->addAttributeByAlias($attr, $name);
     }
   }

   public static function get($id, &$costFunctionParameters, $expr1, $expr2, $pred, $cols, $groups, $write, $buildRight) {
     $tmp = new BJ($id, $expr1, $expr2, $pred, $cols, $groups, $write, $buildRight);
     $tmp->setParameters($costFunctionParameters);
     return $tmp;
   }

   public function computeCost($nodeCost) {
     return $this->expr1->getCost() + $this->expr2->getCost() + $nodeCost;
   }

   public function computeResultTable() {
     $this->functionDetails[Details::TABLE_IN] = array();
     TaskType::addTableDetails($this->functionDetails[Details::TABLE_IN], $this->t1);
     TaskType::addTableDetails($this->functionDetails[Details::TABLE_IN], $this->t2);

     $cardinality = $this->pred->getJCard()
        * TaskType::group($this->pred->getJCard(), Math::prodArray($this->groups, function($a) {
          return $a->getCardinality();
        }));
     $size = $this->WSize() * $this->nTableP();
     $nPartitions = $this->nTableP();
     $table = new Table(get_class($this)."(".$this->getId().")", $cardinality, $size, $nPartitions);
     if ($this->cols != NULL) {
       foreach ($this->cols as $name => $value) {
         $table->addAttributeByAlias($value, $name);
       }
     } else {
       foreach ($this->joinTable->getAttributesAls() as $name => $value) {
         $table->addAttributeByAlias($value, $name);
       }
     }
     return $table;
   }

   public function writeAffectsCost() {
     parent::writeAffectsCost();
     return true;
   }

   protected function nTableP() {
     if ($this->buildRight) { //edited, switched the 2 tables
       return $this->t1->getNPartitions();
     } else {
       return $this->t2->getNPartitions();

     }
   }

   private function WSize() {
     if (!$this->write) {
       return 0;
     }
     $this->mem_jSize = $this->pred->getJSize();
     $this->mem_jCard = $this->pred->getJCard();
     $this->mem_jProj = TaskType::proj($this->joinTable, $this->cols);
     return ( ($this->mem_jSize * $this->mem_jProj) / $this->nTableP())
      * TaskType::group($this->mem_jCard, Math::prodArray($this->groups, function($a) {
       return $a->getCardinality();
     }));
   }

   protected function computeNodeCost() {
     $waves = ceil($this->nTableP() / ($this->params->nE() * $this->params->nEC())); //in Q5 nTableP è basso si vede, da verificare
     $wBB = Write::get($this->params, $this->WSize());
     $this->basicBricks["Write"] = &$wBB;
     $this->writeT = $wBB->getCost() * $waves;

     // Cost details
     $this->functionDetails[Details::GENERIC]["Number of waves"] = $waves;
     $this->functionDetails[Details::GENERIC]["Task write size"] = $this->WSize();
     $this->functionDetails[Details::GENERIC]["jSize"] = $this->mem_jSize;
     $this->functionDetails[Details::GENERIC]["jCard"] = $this->mem_jCard;
     $this->functionDetails[Details::GENERIC]["jProj"] = $this->mem_jProj;
     $this->functionDetails[Details::GENERIC]["Build"] = $this->buildRight == true ? 'Right' : 'Left';

     return $this->writeT;
   }
}

/*
 * ------------------------------------------------------------
 * Group By
 * ------------------------------------------------------------
 */
class GB extends TaskType {

   private $expr;
   private $t;
   private $pred;
   private $cols;
   private $groups;


   protected $readAffectsCost;
   protected $writeAffectsCost;
   protected $networkAffectsCost;

   public function __construct($id, $expr, $pred, $cols, $groups) {
     parent::__construct($id);
     $this->expr = $expr;
     $this->t = $expr->getResultTable();
     $this->pred = $pred;
     $this->cols = $cols;
     $this->groups = $groups;
   }

   public static function get($id, &$costFunctionParameters, $expr, $pred, $cols, $groups) {
     $tmp = new GB($id, $expr, $pred, $cols, $groups);
     $tmp->setParameters($costFunctionParameters);
     return $tmp;
   }

   public function computeCost($nodeCost) {
     return $this->expr->getCost() + $nodeCost;
   }

   public function computeResultTable() {
     $this->functionDetails[Details::TABLE_IN] = array();
     TaskType::addTableDetails($this->functionDetails[Details::TABLE_IN], $this->t);

     $cardinality = $this->t->getCardinality()
        * TaskType::group($this->t->getCardinality(), Math::prodArray($this->groups, function($a) {
          return $a->getCardinality();
        }));
     $size = $this->WSize() * $this->params->nSB();
     $nPartitions = $this->params->nSB();
     $table = new Table(get_class($this)."(".$this->getId().")", $cardinality, $size, $nPartitions);
     foreach ($this->cols as $name => $value) {
       $table->addAttributeByAlias($value, $name);
     }
     return $table;
   }

   public function readAffectsCost() {
     parent::readAffectsCost();
     return $this->readAffectsCost;
   }

   public function writeAffectsCost() {
     parent::writeAffectsCost();
     return $this->writeAffectsCost;
   }

   public function networkAffectsCost(){
     parent::networkAffectsCost();
     return $this->networkAffectsCost;
   }

   private function RSize() {
     return $this->t->getSize() / $this->params->nSB();
   }

   private function WSize() {
     $hsel = is_null($this->pred) ? 1 : $this->params->hSel();
     return $this->RSize() * $hsel
      * TaskType::proj($this->t, $this->cols)
      * TaskType::group($this->t->getCardinality(), Math::prodArray($this->groups, function($a) {
        return $a->getCardinality();
     }));
   }

   protected function computeNodeCost() {
     $waves = ceil($this->params->nSB() / ($this->params->nE() * $this->params->nEC()));
     $rBB = SRead::get($this->params, $this->RSize());
     $this->basicBricks["SRead"] = &$rBB;
     if($rBB->isRead() == 1){
       $this->readT = $rBB->getCost() * $waves;
       $this->networkT = $rBB->otherTime() * $waves;
       $this->readAffectsCost = true;
       $this->networkAffectsCost = false;
     } else {
       $this->readT = $rBB->otherTime() * $waves;
       $this->networkT = $rBB->getCost() * $waves;
       $this->readAffectsCost = false;
       $this->networkAffectsCost = true;
     }
     $wBB = Write::get($this->params, $this->WSize());
     $this->writeAffectsCost = true;
     $this->basicBricks["Write"] = &$wBB;
     $this->writeT = $wBB->getCost() * $waves;

     // Cost details
     $this->functionDetails[Details::GENERIC]["Number of waves"] = $waves;
     $this->functionDetails[Details::GENERIC]["Task read size"] = $this->RSize();
     $this->functionDetails[Details::GENERIC]["Task write size"] = $this->WSize();

     if($this->readT > $this->networkT){
       return $this->readT + $this->writeT;
     } else {
       return $this->networkT + $this->writeT;
     }
   }
}

?>
