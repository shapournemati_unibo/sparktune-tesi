<?php

class Math {
  public static function factorial($number) {
      if ($number < 2) {
          return 1;
      } else {
          return ($number * Math::factorial($number-1));
      }
  }

  public static function binomialCoefficient($n, $k) {
      // the binomial coefficient is defined as n! / [ (n-k)! * k! ]
      if ($k > $n) {
        return 0;
      }
      return Math::factorial($n) / (Math::factorial($n - $k) * Math::factorial($k));
  }

  public static function cardenas($R, $N) {
      if ($N <= 0) {
        return NULL;
      }
      // The cardenas algorithm is defined as N*(1-(1-1/N)^R)
      return $N * (1 - pow(1 - 1/$N, $R) );
  }

  public static function sum($start, $end, callable $function) {
    $cost = 0;
    for ($i=$start; $i <= $end; $i++) {
      $cost+=$function($i);
    }
    return $cost;
  }

  public static function sumArray($array, callable $function) {
    if ($array == NULL) {
      return NULL;
    }
    $res = 0;
    foreach ($array as $key => $value) {
      $res += $function($value);
    }
    return $res;
  }

  public static function prod($start, $end, callable $function) {
    $cost = 1;
    for ($i=$start; $i <= $end; $i++) {
      $cost*=$function($i);
    }
    return $cost;
  }

  public static function prodArray($array, callable $function) {
    if ($array == NULL) {
      return NULL;
    }
    if (empty($array)) {
      return NULL;
    }
    $res = 1;
    foreach ($array as $key => $value) {
      $res *= $function($value);
    }
    return $res;
  }
}

?>
