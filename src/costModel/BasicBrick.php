<?php
include_once "CostFunctionParameters.php";
include_once "Math.php";

abstract class BBDetails {
  const DISC_THROUGHPUT_READ = 'Disc read throughput';
  const DISC_THROUGHPUT_WRITE = 'Disc write throughput';
  const NET_THROUGHPUT_INTRA = 'Intra network throughput';
  const NET_THROUGHPUT_EXTRA = 'Extra network throughput';
}

abstract class BasicBrick {

   private $costFunctionParameters;
   private $computed = false;
   private $cost = NULL;
   protected $functionDetails = array();

   public function getFunctionDetails() {
     return $this->functionDetails;
   }

   public function setParameters(&$costFunctionParameters)
   {
     $this->costFunctionParameters = $costFunctionParameters;
   }

   protected function getParams() {
     return $this->costFunctionParameters;
   }

   protected function psr($v) {
     if ($v > $this->getParams()->nRN()) {
       return 0;
     }
     return ( Math::binomialCoefficient($this->getParams()->nRN(), $v) / Math::binomialCoefficient($this->getParams()->nN(), $v) )
       * $this->getParams()->nR();
   }

   public function askRecomputation() {
     $this->computed = false;
   }

   private function justComputed() {
     $this->computed = true;
   }

   private function needsRecomputation() {
     return !$this->computed;
   }

   private function checkAndRecompute() {
     if ($this->needsRecomputation()) {
       $this->cost = $this->computeCost();

       $this->justComputed();
     }
   }

   public function getCost() {
     $this->checkAndRecompute();
     return $this->cost;
   }

   abstract protected function computeCost();

}

/*
 * Read
 */
abstract class DataPlace {
    const L = 0;
    const R = 1;
    const C = 2;
}

class Read extends BasicBrick {

   private $size;
   private $X;

   //added to avoid losing info about read/network
   //Read Brick always considers its cost as READ time, while it can be Net
   //time sometimes or at least work with a net time != 0

   //1 when it is read, 0 if it is network
   private $isRead;
   private $otherTime;

   public function __construct($size, $dataPlace) {
     $this->size = $size;
     $this->X = $dataPlace;
   }
   public static function get(&$costFunctionParameters, $size, $dataPlace) {
     $tmp = new Read($size, $dataPlace);
     $tmp->setParameters($costFunctionParameters);
     $tmp->computeCost();
     return $tmp;
   }

   public function isRead(){
     return $this->isRead;
   }

   public function otherTime(){
     return $this->otherTime;
   }

   protected function computeCost() {
     switch ($this->X) {
       case DataPlace::L:
         $readTx = $this->readTL();
         $transTx = 0;
         break;
       case DataPlace::R:
         $readTx = $this->readTR();
         $transTx = $this->transTR();
         break;
       case DataPlace::C:
         $readTx = $this->readTC();
         $transTx = $this->transTC();
         break;
     }

     if($readTx > $transTx){
       $this->isRead = 1;
       $this->otherTime = $transTx;
     } else {
       $this->isRead = 0;
       $this->otherTime = $readTx;
     }

     return max($readTx, $transTx);
   }

   # LOCAL
   private function readTL() {
     $read_th = $this->getParams()->discR($this->getParams()->nEC());
     $this->functionDetails[BBDetails::DISC_THROUGHPUT_READ . " local"] = $read_th;
     return $this->size / $read_th;
   }

   # RACK
   private function readTR() {
     $div;
     if(($this->getParams()->nRN() <= $this->getParams()->nRE())){
       return 0;
     } else {
       $div = ($this->getParams()->nRN() - $this->getParams()->nRE());
     }

     $read_th = $this->getParams()->discR(ceil(
       ($this->getParams()->nRE() * $this->getParams()->nEC()) / $div )); //EDIT
     $this->functionDetails[BBDetails::DISC_THROUGHPUT_READ . " rack"] = $read_th;
     return $this->size / $read_th;
   }

   private function transTR() {
     $div;
     if(($this->getParams()->nRN() <= $this->getParams()->nRE())){
       return 0;
     } else {
       $div = ($this->getParams()->nRN() - $this->getParams()->nRE());
     }

     $trans_th = $this->getParams()->netI(ceil(
       $this->getParams()->nEC() / $div )); //EDIT
     $this->functionDetails[BBDetails::NET_THROUGHPUT_INTRA . " rack"] = $trans_th;
     return $this->size / $trans_th;
   }

   # CLUSTER
   private function readTC() {
     $div;
     if(($this->getParams()->nRN() <= $this->getParams()->nRE())){
       return 0;
     } else {
       $div = ($this->getParams()->nRN() - $this->getParams()->nRE());
     }
     $read_th = $this->getParams()->discR(ceil(
       ($this->getParams()->nRE() * $this->getParams()->nEC()) / $div )); //EDIT
     $this->functionDetails[BBDetails::DISC_THROUGHPUT_READ . " cluster"] = $read_th;
     return $this->size / $read_th;
   }

   private function transTC() {
     if ($this->getParams()->nR() == 1 || ($this->getParams()->nRN() <=$this->getParams()->nRE())) {
       return 0;
     }
     $trans_th = $this->getParams()->netE(ceil(
       $this->getParams()->nEC() / ( ($this->getParams()->nR() - 1) * ($this->getParams()->nRN() - $this->getParams()->nRE())) ));
     $this->functionDetails[BBDetails::NET_THROUGHPUT_EXTRA . " cluster"] = $trans_th;
     return $this->size / $trans_th;
   }

}

/*
 * Write
 */
class Write extends BasicBrick {

   private $size;

   public function __construct($size) {
     $this->size = $size;
   }

   public static function get(&$costFunctionParameters, $size) {
     $tmp = new Write($size);
     $tmp->setParameters($costFunctionParameters);
     return $tmp;
   }

   protected function computeCost() {
     $write_th = $this->getParams()->discW($this->getParams()->nEC());
     $this->functionDetails[BBDetails::DISC_THROUGHPUT_WRITE] = $write_th;
     $sCmp = $this->getParams()->sCmp();

     return ($this->size * $sCmp) / $write_th; //il write al denominatore cala all'aumentare del numero dei core????
   }
}

/*
 * Shuffle read
 */
class SRead extends BasicBrick {

   private $size;

   //added to avoid losing info about read/network
   //Read Brick always considers its cost as READ time, while it can be Net
   //time sometimes or at least work with a net time != 0

   private $isRead;
   private $otherTime;

   public function __construct($size) {
     $this->size = $size;
   }
   public static function get(&$costFunctionParameters, $size) {
     $tmp = new SRead($size);
     $tmp->setParameters($costFunctionParameters);
     $tmp->computeCost();
     return $tmp;
   }

   public function isRead(){
     return $this->isRead;
   }

   public function otherTime(){
     return $this->otherTime;
   }

   protected function computeCost() {
     if($this->readT() > $this->transT()){
       $this->isRead = 1;
       $this->otherTime = $this->transT();
     } else {
       $this->isRead = 0;
       $this->otherTime = $this->readT();
     }
     return max($this->readT(), $this->transT());
   }

   private function readT() {
     $read_th = $this->getParams()->discR($this->getParams()->nE() * $this->getParams()->nEC());
     $this->functionDetails[BBDetails::DISC_THROUGHPUT_READ] = $read_th;
     return $this->execBucketSize() / $read_th;
   }

   private function transT() {
     $neti_th = $this->getParams()->netI($this->getParams()->nEC());
     $this->functionDetails[BBDetails::NET_THROUGHPUT_INTRA] = $neti_th;
     $nete_th = $this->getParams()->netE($this->getParams()->nEC());
     $this->functionDetails[BBDetails::NET_THROUGHPUT_EXTRA] = $nete_th;
     return $this->execBucketSize() /
       ( $this->psr($this->getParams()->nE()) * $neti_th //Capire meglio cosa fa questo PSR
         + (1 - $this->psr($this->getParams()->nE())) * $nete_th );
   }

   private function execBucketSize() {
     return $this->size / $this->getParams()->nE();
   }
}

/*
 * Broadcast
 */
class Broadcast extends BasicBrick {

   private $size;

   public function __construct($size) {
     $this->size = $size;
   }
   public static function get(&$costFunctionParameters, $size) {
     $tmp = new Broadcast($size);
     $tmp->setParameters($costFunctionParameters);
     return $tmp;
   }

   protected function computeCost() {
     return $this->collectT() + $this->distributeT();
   }

   private function collectT() {
     $neti_th = $this->getParams()->netI($this->getParams()->nEC());
     $this->functionDetails[BBDetails::NET_THROUGHPUT_INTRA . " collect"] = $neti_th;
     $nete_th = $this->getParams()->netE($this->getParams()->nEC());
     $this->functionDetails[BBDetails::NET_THROUGHPUT_EXTRA . " collect"] = $nete_th;
     return $this->size
       / ( $this->psr($this->getParams()->nE() + 1) * $neti_th //Ecco qui lo spike, è nE + 1 perchè è incluso l'applcation driver
         + (1 - $this->psr($this->getParams()->nE() + 1)) * $nete_th );
   }

   private function distributeT() {
     $neti_th = $this->getParams()->netI(1);
     $this->functionDetails[BBDetails::NET_THROUGHPUT_INTRA . " distribute"] = $neti_th;
     $nete_th = $this->getParams()->netE(1);
     $this->functionDetails[BBDetails::NET_THROUGHPUT_EXTRA . " distribute"] = $nete_th;
     return ($this->size * $this->getParams()->nE() * $this->getParams()->nEC())
       / ( $this->psr($this->getParams()->nE() + 1) * $neti_th
         + (1 - $this->psr($this->getParams()->nE() + 1)) * $nete_th );
   }
}

?>
