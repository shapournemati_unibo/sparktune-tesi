<?php
require_once (dirname(__FILE__) . "/CostFunctionParameters.php");
require_once (dirname(__FILE__) . "/TaskType.php");
require_once (dirname(dirname(__FILE__)) . '/GPSJGrammarParser/GrammarSyntax.php');

class CostModel {

  private $gpsjGrammarDerivation;
  private $parameters;
  private $database;
  private $attributeAliases;
  private $nodesArray;

  public function __construct( $parameters, $database,$jsonGpsjGrammarDerivation) {
    $this->attributeAliases = array();
    $this->nodesArray = array();

    $this->parameters = $parameters;
    $this->database = $database;

    $this->gpsjGrammarDerivation = $this->parseJson($jsonGpsjGrammarDerivation, $this->parameters);
  }


  public function getCost() {
    return $this->gpsjGrammarDerivation->getCost();
  }

  public function getGrammarDerivation() {
    return $this->gpsjGrammarDerivation;
  }

  public function getNodesArray() {
    return $this->nodesArray;
  }

  private function parseJson($json, $parameters) {
    $gpsjArray = json_decode($json, true);
    $converted = $this->convertNode($gpsjArray[0], $this->nodesArray);
    return $converted;
  }

  private function convertNode($node, &$nodesArray = NULL) {
    // Recursive for the children
    if (array_key_exists(GrammarSyntax::CHILDREN, $node)) {
      $children = array();
      foreach ($node[GrammarSyntax::CHILDREN] as $child) {
        $children[] = $this->convertNode($child, $nodesArray);
      }
    }
    if (array_key_exists(GrammarSyntax::ID, $node)) {
      $id = $node[GrammarSyntax::ID];
    } else {
      $id = -1;
    }
    // Convert the node
    switch ($node[GrammarSyntax::OPERATION]) {
      case GrammarSyntax::OP_SCAN:
      case GrammarSyntax::OP_SCANBROADCAST:
        $table = $this->database->getTable($node[GrammarSyntax::HDFS][GrammarSyntax::TABLE_NAME]);
        if (array_key_exists(GrammarSyntax::COLUMNS, $node)) {
          $this->addAliases($table, $node[GrammarSyntax::COLUMNS]);
        }
        if (array_key_exists(GrammarSyntax::FILTER, $node)) {
          $pred = $node[GrammarSyntax::FILTER];
        } else {
          $pred = NULL;
        }
        if (array_key_exists(GrammarSyntax::PROJECT, $node)) {
          $cols = $this->getCols($node[GrammarSyntax::PROJECT]);
        } else {
          $cols = $table->getAttributesAls();
        }
        if (array_key_exists(GrammarSyntax::AGGREGATE, $node)) {
          $groups = $this->getGroupKeys($node[GrammarSyntax::AGGREGATE]);
        } else {
          $groups = NULL;
        }
        if ($node[GrammarSyntax::OPERATION] == GrammarSyntax::OP_SCAN) {
          $table = $this->database->getTable($node[GrammarSyntax::HDFS][GrammarSyntax::TABLE_NAME]);
          $write = !array_key_exists(GrammarSyntax::PIPE, $node) || $node[GrammarSyntax::PIPE] == false;
          $nodeOut = SC::get($id, $this->parameters, $table, $pred, $cols, $groups, $write);
        } else {
          $nodeOut = SB::get($id, $this->parameters, $table, $pred, $cols, $groups);
        }
        break;
      case GrammarSyntax::OP_SHUFFLEJOIN:
      case GrammarSyntax::OP_BROADCASTJOIN:
        $expr1 = $children[0];
        $expr2 = $children[1];
        $pred = $node[GrammarSyntax::JOINKEY];
        if (array_key_exists(GrammarSyntax::PROJECT, $node)) {
          $cols = $this->getCols($node[GrammarSyntax::PROJECT]);
        } else {
          $cols = NULL;
        }
        if (array_key_exists(GrammarSyntax::AGGREGATE, $node)) {
          $groups = $this->getGroupKeys($node[GrammarSyntax::AGGREGATE]);
        } else {
          $groups = NULL;
        }
        $write = !array_key_exists(GrammarSyntax::PIPE, $node) || $node[GrammarSyntax::PIPE] == false;
        if ($node[GrammarSyntax::OPERATION] == GrammarSyntax::OP_SHUFFLEJOIN) {
          $nodeOut = SJ::get($id, $this->parameters, $expr1, $expr2, $pred, $cols, $groups, $write);
        } else {
          $buildRight = $node[GrammarSyntax::JOINBUILD] == GrammarSyntax::JOINBUILD_RIGHT ? true : false;
          $nodeOut = BJ::get($id, $this->parameters, $expr1, $expr2, $pred, $cols, $groups, $write, $buildRight);
        }
        break;
      case GrammarSyntax::OP_GROUPBY:
        $expr = $children[0];
        if (array_key_exists(GrammarSyntax::FILTER, $node)) {
          $pred = $node[GrammarSyntax::FILTER];
        } else {
          $pred = NULL;
        }
        if (array_key_exists(GrammarSyntax::AGGREGATE, $node)) {
          $groups = $this->getGroupKeys($node[GrammarSyntax::AGGREGATE]);
          $cols = $this->getGroupsOutput($node[GrammarSyntax::AGGREGATE]);
        } else {
          $groups = NULL;
        }
        $nodeOut = GB::get($id, $this->parameters, $expr, $pred, $cols, $groups);
        break;
    }
    // add the node to the nodes array
    if (!is_null($nodesArray)) {
      $nodesArray[] = $nodeOut;
    }
    return $nodeOut;
  }

  private function addAliases($table, $aliases) {
    foreach ($aliases as $key => $alias) {
      $table->addAttributeAlias($alias);
      $this->attributeAliases[$alias] = $table->getAttribute($alias);
    }
  }

  private function getCols($cols) {
    $res = array();
    $attributes = $cols;
    foreach ($attributes as $key => $attribute) {
      if (array_key_exists($attribute, $this->attributeAliases)) {
        $res[$attribute] = $this->attributeAliases[$attribute];
      } else {
        $res[$attribute] = NULL;
      }
    }
    return $res;
  }

  private function getGroupKeys($groups) {
    return $this->getCols($groups[GrammarSyntax::AGG_KEYS]);
  }

  private function getGroupsOutput($groups) {
    $colsOut = $this->getCols($groups[GrammarSyntax::AGG_OUTPUT]);
    foreach ($colsOut as $name => $attribute) {
      if ($attribute == NULL) {
        $colsOut[$name] = Attribute::get($name);
      }
    }
    return $colsOut;
  }

  public function __toString() {
    return var_export($this->getGrammarDerivation(), true);
  }
}

?>
