<?php
include_once (dirname(__FILE__) . "/Table.php");

class Database {

  private $name;
  private $tables;

  public function __construct($name) {
    $this->name = $name;
    $this->tables = array();
  }

  public function addTable($table) {
    $this->tables[$table->getName()] = $table;
  }

  public function getTables() {
    return $this->tables;
  }

  public function setTables($newTables){
    $this->tables = $newTables;
  }

  public function getTable($name) {
    return $this->getTables()[$name];
  }

  public function __toString() {
    return var_export($this, true);
  }

}

?>
