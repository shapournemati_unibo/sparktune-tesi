<?php
include (dirname(__FILE__).'/AbstractSparkcostParser.php');

class SparkcostParser extends AbstractSparkcostParser {

    // Operations
    const OP_CONVERTTOSAFE = 'ConvertToSafe';
    const OP_CONVERTTOUNSAFE = 'ConvertToUnsafe';
    const OP_PROJECT = 'Project';
    const OP_FILTER = 'Filter';
    const OP_HIVETABLESCAN = 'HiveTableScan';
    const OP_SORTMERGEJOIN = 'SortMergeJoin';
    const OP_BROADCASTHASHJOIN = 'BroadcastHashJoin';
    const OP_TUNGSTENSORT = 'TungstenSort';
    const OP_TUNGSTENPROJECT = 'TungstenProject';
    const OP_TUNGSTENAGGREGATE = 'TungstenAggregate';
    const OP_TUNGSTENEXCHANGE = 'TungstenExchangehashpartitioning';

    public function parsePhisicalPlan($phisicalPlan) {
      $a = self::analyze_phisical_plan($phisicalPlan);
      $c = self::compat($a);
      $e = self::applay_exceptions($c);
      return $e;
    }

    private static function applay_exceptions($nodes) {
      foreach ($nodes as $key => &$node) {
        switch ($node[GrammarSyntax::OPERATION]) {
          case GrammarSyntax::OP_SCAN:
            if (array_key_exists(GrammarSyntax::PIPE, $node) && $node[GrammarSyntax::PIPE] == true) {
              unset($node[GrammarSyntax::PIPE]);
              $node[GrammarSyntax::OPERATION] = GrammarSyntax::OP_SCANBROADCAST;
            }
            break;
          case GrammarSyntax::OP_GROUPBY:
            unset($node[GrammarSyntax::PIPE]);
            break;
        }

      }
      $firstNode = &$nodes[0];
      $firstNode[GrammarSyntax::PIPE] = false;
      if (sizeof($nodes) == 1 && $firstNode[GrammarSyntax::OPERATION] == GrammarSyntax::OP_SCANBROADCAST) {
        // If has only one node and it is a scan and broadcast, then:
        $firstNode[GrammarSyntax::OPERATION] = GrammarSyntax::OP_SCAN;
      }
      return $nodes;
    }

    private static function compat($linear_plan) {
      $params = array();
      $nodes = array();
      $index = 1;
      foreach ($linear_plan as $idx => $content) {
        $operation = key($content);
        $parameters = current($content);
        switch ($operation) {
          case self::OP_TUNGSTENAGGREGATE:
            $params[GrammarSyntax::AGGREGATE] = $parameters;
            break;
          case self::OP_FILTER:
            // Filter
            // Removes all the casts
            $patterns = array();
            $patterns[0] = '/cast\(/';
            $patterns[1] = '/as[^\)]+\)/';
            $replacements = array();
            $replacements[2] = '';
            $replacements[1] = '';
            $res = preg_replace($patterns, $replacements, $parameters[0]);

            $params[GrammarSyntax::FILTER] = $res;
            break;
          case self::OP_HIVETABLESCAN:
            // Operation name
            $params[GrammarSyntax::OPERATION] = GrammarSyntax::OP_SCAN;
            // HDFS
            $params[GrammarSyntax::HDFS] = array(
              GrammarSyntax::TABLE_NAME => $parameters[1][1],
              "0" => $parameters[1][0],
              "2" => $parameters[1][2]
            );
            // Columns
            $params[GrammarSyntax::COLUMNS] = $parameters[0];

            $nodes[] = self::buildNode($params, $index);
            break;
          case self::OP_BROADCASTHASHJOIN:
          case self::OP_SORTMERGEJOIN:
            // Operation name
            if ($operation == self::OP_SORTMERGEJOIN) {
              $params[GrammarSyntax::OPERATION] = GrammarSyntax::OP_SHUFFLEJOIN;
            } else {
              $params[GrammarSyntax::OPERATION] = GrammarSyntax::OP_BROADCASTJOIN;
              $params[GrammarSyntax::JOINBUILD] = $parameters[2] == "BuildLeft" ?
                  GrammarSyntax::JOINBUILD_LEFT : GrammarSyntax::JOINBUILD_RIGHT;
            }
            $params[GrammarSyntax::JOINKEY] = array();
            $params[GrammarSyntax::JOINKEY][] = self::checkColumns($parameters[0]);
            $params[GrammarSyntax::JOINKEY][] = self::checkColumns($parameters[1]);

            $nodes[] = self::buildNode($params, $index);
            break;
          case self::OP_PROJECT:
          case self::OP_TUNGSTENPROJECT:
            $params[GrammarSyntax::PROJECT] = $parameters;
            break;
          case self::OP_TUNGSTENEXCHANGE:
            // If an operation of aggregate is directly preceded by a tungstenExchange, then it is a group by operation
            if (array_key_exists(GrammarSyntax::AGGREGATE, $params)) {
              $params[GrammarSyntax::OPERATION] = GrammarSyntax::OP_GROUPBY;

              $nodes[] = self::buildNode($params, $index);
            }
            // To set AFTER the build node, otherwise it is set to the wrong node
            $params[GrammarSyntax::PIPE] = false;
            break;
          // Following operations are correctly analyzed but aren't inserted in the DAG Json
          case self::OP_CONVERTTOSAFE:
          case self::OP_CONVERTTOUNSAFE:
          case self::OP_TUNGSTENSORT:
            break;
          default:
            echo "<!-- WARNING: default management of $operation-->\n";
        }
      }
      return $nodes;
    }

    private static function checkColumns($columns) {
      // Remove each cast containment from the column names
      foreach ($columns as $key => &$col) {
        if (preg_match('/cast/',$col)) {
          $regex = '/cast\(([^\)]*?)as/i';
          preg_match($regex, $col, $matches_arr);
          $col = $matches_arr[1];
        }
      }
      return $columns;
    }

    // Return an array representing the node (all his properties) and clear the $params array
    private static function buildNode(&$params, &$index) {
      $params[GrammarSyntax::ID] = $index;
      if (!array_key_exists(GrammarSyntax::PIPE, $params)) {
        $params[GrammarSyntax::PIPE] = true;
      }
      $node = $params;
      $params = array();
      $index++;
      return $node;
    }

    // Return an array containing all the operations executed according to the phisical plan
    private static function analyze_phisical_plan($string_plan){
      // Split each row
      $string_plan = str_replace(' ', '', $string_plan);
      $rows = self::parse_string_branches($string_plan, '[', ']');

      if ($rows > 0) {
        if ($rows[0] == '==PhysicalPlan==') {
          array_shift($rows);
        }

        // Analyze each row of the phisical plan
        $result = array();
        foreach($rows as $row => $data) {
          $params = null;

          // OPERATION
          $operation = explode("(", $data, 2);
          $operation = explode("[", $operation[0], 2);
          $operation = $operation[0];
          $operation = str_replace(' ', '', $operation);

          // PARAMETERS
          switch ($operation) {
            case self::OP_TUNGSTENAGGREGATE:
              // One single argument between round branches
              $tmp = self::parse_string_branches($data, '(', ')');
              $params = array();
              $tmp = explode('],',$tmp[1], 2);  // The keys
              $tmp1 = explode('=[',$tmp[0], 2);
              $tmp1 = explode(',',$tmp1[1]);
              $params[GrammarSyntax::AGG_KEYS] = $tmp1;
              $tmp = explode('],',$tmp[1], 2);  // The function
              $tmp1 = explode('=[',$tmp[0], 2);
              $tmp1 = self::parse_string_branches($tmp1[1], '(', ')');
              $params[GrammarSyntax::AGG_FUNCTION] = $tmp1[0];
              $tmp = explode('],',$tmp[1], 2);  // The output
              $tmp1 = explode('=[',$tmp[0], 2);
              $tmp1 = explode(',',$tmp1[1]);
              $params[GrammarSyntax::AGG_OUTPUT] = $tmp1;
              break;
            case self::OP_TUNGSTENEXCHANGE:
            case self::OP_FILTER:
              // One single argument between round branches
              $params = self::parse_string_branches($data, '(', ')');
              array_shift($params); //parse string branches puts the text before first open parenthesis
              //into the first position of the result array.
              break;
            case self::OP_HIVETABLESCAN:
              // Split square branches content of the row
              $params = self::parse_string_branches($data, '[', ']');
              array_shift($params);
              $params[0] = explode(',',$params[0]);
              // Split round branches content of the row and keep only the text between them (remove the first comma)
              $params[1] = self::parse_string_branches($params[1], '(', ')');
              $params[1] = $params[1][1];
              $params[1] = explode(',',$params[1]);
              break;
            case self::OP_PROJECT:
            case self::OP_TUNGSTENPROJECT:
              $params = self::parse_string_branches($data, '[', ']');
              array_shift($params);
              $tmp = explode(',',$params[0]);
              $params = $tmp;
              break;
            case self::OP_TUNGSTENSORT:
              $params = self::parse_string_branches($data, '[', ']');
              array_shift($params);
              $tmp = explode(',',$params[1]);
              $params[1] = $tmp[1];
              $params[2] = $tmp[2];
              break;
            case self::OP_BROADCASTHASHJOIN:
            case self::OP_SORTMERGEJOIN:
              $tmp = self::parse_string_branches($data, '[', ']');
              $leftKey = $tmp[1];
              $rightKey = $tmp[3];
              $params[0] = explode(',',$leftKey);
              $params[1] = explode(',',$rightKey);
              if ($operation == self::OP_BROADCASTHASHJOIN) {
                $params[2] = explode(',',$tmp[4])[1];
              }
              break;
            default:
              $params = $data;
          }

          if ($params != null) {
            $result[] = array($operation => $params);
          }
        }
        return $result;
      }
    }
}

?>
