<?php
include (dirname(__FILE__).'/AbstractSparkcostParser.php');

class SparkcostParser extends AbstractSparkcostParser {

    // Operations
    const OP_CONVERTTOSAFE = 'ConvertToSafe';
    const OP_CONVERTTOUNSAFE = 'ConvertToUnsafe';
    const OP_PROJECT = 'Project';
    const OP_FILTER = 'Filter';
    const OP_HIVETABLESCAN = 'HiveTableScan';
    const OP_SORTMERGEJOIN = 'SortMergeJoin';
    const OP_BROADCASTHASHJOIN = 'BroadcastHashJoin';
    const OP_SORT = 'Sort';
    const OP_AGGREGATE = 'HashAggregate';
    const OP_EXCHANGE = 'Exchangehashpartitioning';

    public function parsePhisicalPlan($phisicalPlan) {
      $a = self::convertPlan($phisicalPlan);
      $b = self::analyze_phisical_plan($a);
      $c = self::compat($b);
      $d = self::applay_exceptions($c);
      return $d;
    }

    /*
    Since the upgrade from Spark 1.5 to Spark 2.2, the physical plan changed its
    sintax, leaving the overall semantics unchanged. Here we convert part of the
    sintax to spark 1.5, which is more machine-readable.
    */
    private static function convertPlan($string_plan){
      $string_plan = str_replace("\n", '][', $string_plan);
      $string_plan = str_replace('isnotnull', '', $string_plan);
      return $string_plan;
    }

    private static function applay_exceptions($nodes) {
      foreach ($nodes as $key => &$node) {
        switch ($node[GrammarSyntax::OPERATION]) {
          case GrammarSyntax::OP_SCAN:
            if (array_key_exists(GrammarSyntax::PIPE, $node) && $node[GrammarSyntax::PIPE] == true) {
              unset($node[GrammarSyntax::PIPE]);
              $node[GrammarSyntax::OPERATION] = GrammarSyntax::OP_SCANBROADCAST;
            }
            break;
          case GrammarSyntax::OP_GROUPBY:
            unset($node[GrammarSyntax::PIPE]);
            break;
        }

      }
      $firstNode = &$nodes[0];
      $firstNode[GrammarSyntax::PIPE] = false;
      if (sizeof($nodes) == 1 && $firstNode[GrammarSyntax::OPERATION] == GrammarSyntax::OP_SCANBROADCAST) {
        // If has only one node and it is a scan and broadcast, then:
        $firstNode[GrammarSyntax::OPERATION] = GrammarSyntax::OP_SCAN;
      }
      return $nodes;
    }

    private static function compat($linear_plan) {
      $params = array();
      $nodes = array();
      $index = 1;
      foreach ($linear_plan as $idx => $content) {
        $operation = key($content);
        $parameters = current($content);
        switch ($operation) {
          case self::OP_AGGREGATE:
            $params[GrammarSyntax::AGGREGATE] = $parameters;
            break;
          case self::OP_FILTER:
            // Filter
            // Removes all the casts
            $patterns = array();
            $patterns[0] = '/cast\(/';
            $patterns[1] = '/as[^\)]+\)/';
            $replacements = array();
            $replacements[2] = '';
            $replacements[1] = '';
            $res = preg_replace($patterns, $replacements, $parameters[0]);

            $params[GrammarSyntax::FILTER] = $res;
            break;
          case self::OP_HIVETABLESCAN:
            // Operation name
            $params[GrammarSyntax::OPERATION] = GrammarSyntax::OP_SCAN;
            // HDFS
            $params[GrammarSyntax::HDFS] = array(
              //parameter [1][0] is an empty string
              GrammarSyntax::TABLE_NAME => $parameters[1][2],
              "0" => $parameters[1][1]
            );
            // Columns
            $params[GrammarSyntax::COLUMNS] = $parameters[0];

            $nodes[] = self::buildNode($params, $index);
            break;
          case self::OP_BROADCASTHASHJOIN:
          case self::OP_SORTMERGEJOIN:
            // Operation name
            if ($operation == self::OP_SORTMERGEJOIN) {
              $params[GrammarSyntax::OPERATION] = GrammarSyntax::OP_SHUFFLEJOIN;
            } else {
              $params[GrammarSyntax::OPERATION] = GrammarSyntax::OP_BROADCASTJOIN;
              $params[GrammarSyntax::JOINBUILD] = $parameters[2] == "BuildLeft" ?
                  GrammarSyntax::JOINBUILD_LEFT : GrammarSyntax::JOINBUILD_RIGHT;
            }
            $params[GrammarSyntax::JOINKEY] = array();
            $params[GrammarSyntax::JOINKEY][] = self::checkColumns($parameters[0]);
            $params[GrammarSyntax::JOINKEY][] = self::checkColumns($parameters[1]);

            $nodes[] = self::buildNode($params, $index);
            break;
          case self::OP_PROJECT:
            $params[GrammarSyntax::PROJECT] = $parameters;
            break;
          case self::OP_EXCHANGE:
            // If an operation of aggregate is directly preceded by a tungstenExchange, then it is a group by operation
            if (array_key_exists(GrammarSyntax::AGGREGATE, $params)) {
              $params[GrammarSyntax::OPERATION] = GrammarSyntax::OP_GROUPBY;

              $nodes[] = self::buildNode($params, $index);
            }
            // To set AFTER the build node, otherwise it is set to the wrong node
            $params[GrammarSyntax::PIPE] = false;
            break;
          // Following operations are correctly analyzed but aren't inserted in the DAG Json
          case self::OP_CONVERTTOSAFE:
          case self::OP_CONVERTTOUNSAFE:
          case self::OP_SORT:
            break;
          default:
            echo "<!-- WARNING: default management of $operation-->\n";
        }
      }
      return $nodes;
    }

    private static function checkColumns($columns) {
      // Remove each cast containment from the column names
      foreach ($columns as $key => &$col) {
        if (preg_match('/cast/',$col)) {
          $regex = '/cast\(([^\)]*?)as/i';
          preg_match($regex, $col, $matches_arr);
          $col = $matches_arr[1];
        }
      }
      return $columns;
    }

    // Return an array representing the node (all his properties) and clear the $params array
    private static function buildNode(&$params, &$index) {
      $params[GrammarSyntax::ID] = $index;
      if (!array_key_exists(GrammarSyntax::PIPE, $params)) {
        $params[GrammarSyntax::PIPE] = true;
      }
      $node = $params;
      $params = array();
      $index++;
      return $node;
    }

    // Return an array containing all the operations executed according to the phisical plan
    private static function analyze_phisical_plan($string_plan){
      // Split each row
      $string_plan = str_replace(' ', '', $string_plan);
      $rows = self::parse_string_branches($string_plan, '[', ']');

      if ($rows > 0) {
        if ($rows[0] == '==PhysicalPlan==') {
          array_shift($rows);
        }

        // Analyze each row of the phisical plan
        $result = array();
        foreach($rows as $row => $data) {
          $params = null;

          // OPERATION
          $operation = explode("(", $data, 2);
          $operation = explode("[", $operation[0], 2);
          $operation = $operation[0];
          $operation = str_replace(' ', '', $operation);

          // PARAMETERS
          switch ($operation) {
            case self::OP_AGGREGATE:
              // One single argument between round branches
              $tmp = self::parse_string_branches($data, '(', ')');
              $params = array();
              $tmp = explode('],',$tmp[1], 2);  // The keys
              $tmp1 = explode('=[',$tmp[0], 2);
              $tmp1 = explode(',',$tmp1[1]);
              $params[GrammarSyntax::AGG_KEYS] = $tmp1;
              $tmp = explode('],',$tmp[1], 2);  // The function
              $tmp1 = explode('=[',$tmp[0], 2);
              //$tmp1 = self::parse_string_branches($tmp1[1], '(', ')');
              $params[GrammarSyntax::AGG_FUNCTION] = $tmp1[1]; //set to "1" and
              //uncomment the upward line to show only the function without params
              //no output for Spark 2.2 is provided in the physical plan
              break;
            case self::OP_EXCHANGE:
            case self::OP_FILTER:
              // One single argument between round branches
              $params = self::parse_string_branches($data, '(', ')');
              array_shift($params);
              break;
            case self::OP_HIVETABLESCAN:
              // Split square branches content of the row
              $params = self::parse_string_branches($data, '[', ']');
              array_shift($params);
              $params[0] = explode(',',$params[0]);
              $params[1] = explode(',',$params[1]);
              break;
            case self::OP_PROJECT:
              $params = self::parse_string_branches($data, '[', ']');
              array_shift($params);
              $tmp = explode(',',$params[0]);
              $params = $tmp;
              break;
            case self::OP_SORT:
              $params = self::parse_string_branches($data, '[', ']');
              array_shift($params);
              $tmp = explode(',',$params[1]);
              $params[1] = $tmp[1];
              $params[2] = $tmp[2];
              break;
            case self::OP_BROADCASTHASHJOIN:
            case self::OP_SORTMERGEJOIN:
              $tmp = self::parse_string_branches($data, '[', ']');
              $leftKey = $tmp[1];
              $rightKey = $tmp[3];
              $params[0] = explode(',',$leftKey);
              $params[1] = explode(',',$rightKey);
              if ($operation == self::OP_BROADCASTHASHJOIN) {
                $params[2] = explode(',',$tmp[4])[2];
              }
              break;
            default:
              $params = $data;
          }

          if ($params != null) {
            $result[] = array($operation => $params);
          }
        }
        return $result;
      }
    }

    /*
    Splits the input into rows, where each row is between input branches
    if $keepBranches = true, initial branches are kept.

    This updated version of the method gets rid of the 4 special chars
    "+","-","*",":" before each row of the physical plan. This allows to correctly
    read and parse it.
    */
    protected static function parse_string_branches($input, $openParenthesis, $closeParenthesis, $keepBranches = false) {
        $len = strlen($input);
        $substrings = array();
        $paren_count = 0;
        $cur_string = '';
        $special_char = true;
        // Text previous of the first openParenthesis
        for ($i = 0; $i < $len && ($char = $input[$i]) != $openParenthesis; $i++) {
          $cur_string .= $char;
        }
        if (strlen($cur_string)) {
            $substrings[] = $cur_string;
            $cur_string = '';
        }
        // Recursive parenthesis analysis
        for ($i; $i < $len; $i++) {
            $char = $input[$i];
            if ($char == $openParenthesis) {
              if ($paren_count != 0 || $keepBranches) {
                $cur_string .= $char;
              }
              $paren_count += 1;
              $parenthesis = true;
            } elseif ($char == $closeParenthesis) {
              $paren_count -= 1;
              if ($paren_count != 0 || $keepBranches) {
                $cur_string .= $char;
              }
              $parenthesis = true;
            } else {
              if($special_char && ($char == '+' || $char == '-' || $char == '*' || $char == ':')){
                $special_char = true;
              } else {
                $cur_string .= $char;
                $special_char = false;
              }
              $parenthesis = false;
            }
            if ($paren_count == 0 && strlen($cur_string)) {
              $special_char = true;
              if ($justUpdated) {
                $substrings[count($substrings)-1] = $substrings[count($substrings)-1] . $cur_string;
              } else {
                $substrings[] = $cur_string;
              }
              $cur_string = '';
              if ($parenthesis) {
                $justUpdated = false;
              } else {
                $justUpdated = true;
              }
            } else {
              $justUpdated = false;
            }
        }
        return $substrings;
    }
}

?>
