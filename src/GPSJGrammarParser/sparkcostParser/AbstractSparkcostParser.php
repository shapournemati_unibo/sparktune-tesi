<?php
include_once (dirname(dirname(__FILE__)).'/GrammarSyntax.php');

abstract class AbstractSparkcostParser {

    public abstract function parsePhisicalPlan($phisicalPlan);

    /*
    Splits the input into rows, where each row is between input branches
    if $keepBranches = true, initial branches are kept.
    */
    protected static function parse_string_branches($input, $openParenthesis, $closeParenthesis, $keepBranches = false) {
        $len = strlen($input);
        $substrings = array();
        $paren_count = 0;
        $cur_string = '';
        // Text previous of the first openParenthesis
        for ($i = 0; $i < $len && ($char = $input[$i]) != $openParenthesis; $i++) {
          $cur_string .= $char;
        }
        if (strlen($cur_string)) {
            $substrings[] = $cur_string;
            $cur_string = '';
        }
        // Recursive parenthesis analysis
        for ($i; $i < $len; $i++) {
            $char = $input[$i];
            if ($char == $openParenthesis) {
              if ($paren_count != 0 || $keepBranches) {
                $cur_string .= $char;
              }
              $paren_count += 1;
              $parenthesis = true;
            } elseif ($char == $closeParenthesis) {
              $paren_count -= 1;
              if ($paren_count != 0 || $keepBranches) {
                $cur_string .= $char;
              }
              $parenthesis = true;
            } else {
              $cur_string .= $char;
              $parenthesis = false;
            }
            if ($paren_count == 0 && strlen($cur_string)) {
              if ($justUpdated) {
                $substrings[count($substrings)-1] = $substrings[count($substrings)-1] . $cur_string;
              } else {
                $substrings[] = $cur_string;
              }
              $cur_string = '';
              if ($parenthesis) {
                $justUpdated = false;
              } else {
                $justUpdated = true;
              }
            } else {
              $justUpdated = false;
            }
        }
        return $substrings;
    }
}

?>
