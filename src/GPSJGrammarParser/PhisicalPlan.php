<?php

class PhisicalPlan {

  private $phisicalPlan;
  private $sparkVersion;
  private $grammarDerivation;

  public function __construct($phisicalPlan, $sparkVersion) {
    $this->phisicalPlan = $phisicalPlan;
    $this->sparkVersion = $sparkVersion;
    $this->parseToGrammar();
  }

  public function getGrammarDerivationJson() {
    return $this->grammarDerivation;
  }

  private function parseToGrammar() {
    if ($this->phisicalPlan) {
      $parsed;

      switch ($this->sparkVersion) {
        case 'sp1.5.0':
        if (!class_exists('SparkcostParser')) {
          include (dirname(__FILE__).'/sparkcostParser/Sp_1-5-0.php');
        }

          $parsed = (new SparkcostParser)->parsePhisicalPlan($this->phisicalPlan);
          break;

        case 'sp2.2.0':
        if (!class_exists('SparkcostParser')) {
          include (dirname(__FILE__).'/sparkcostParser/Sp_2-2-0.php');
        }

          $parsed = (new SparkcostParser)->parsePhisicalPlan($this->phisicalPlan);
          break;

        default:
          // TODO
          break;
      }

      $reversed = array_reverse($parsed);
      $dag = PhisicalPlan::fetchChildren($reversed, 1);
      PhisicalPlan::applyExceptions($dag[0]);
      $this->grammarDerivation = json_encode($dag, JSON_PRETTY_PRINT);
    }
  }

  private static function applyExceptions(&$node) {
    if ($node[GrammarSyntax::OPERATION] == GrammarSyntax::OP_BROADCASTJOIN) {
      if ($node[GrammarSyntax::JOINBUILD] == GrammarSyntax::JOINBUILD_RIGHT) { // Build Right
        $child = &$node[GrammarSyntax::CHILDREN][0];
      } else { // Build left
        $child = &$node[GrammarSyntax::CHILDREN][1];
      }
      if ($child[GrammarSyntax::OPERATION] == GrammarSyntax::OP_SCANBROADCAST) {
        $child[GrammarSyntax::OPERATION] = GrammarSyntax::OP_SCAN;
        $child[GrammarSyntax::PIPE] = true;
      }
    }
    if (array_key_exists(GrammarSyntax::CHILDREN, $node)) {
      foreach ($node[GrammarSyntax::CHILDREN] as $key => &$value) {
        PhisicalPlan::applyExceptions($value);
      }
    }
  }

  private static function fetchChildren(&$linear_plan, $numChildren) {
    include_once (dirname(__FILE__).'/GrammarSyntax.php');
    $result = array();

    while (count($result) < $numChildren) {
      $node = array_pop($linear_plan);
      switch ($node[GrammarSyntax::OPERATION]) {
        case GrammarSyntax::OP_GROUPBY:
          $children = PhisicalPlan::fetchChildren($linear_plan, 1);
          $node[GrammarSyntax::CHILDREN] = $children;
          $result[] = $node;
          break;
        case GrammarSyntax::OP_SCAN:
        case GrammarSyntax::OP_SCANBROADCAST:
          $result[] = $node;
          break;
        case GrammarSyntax::OP_SHUFFLEJOIN:
        case GrammarSyntax::OP_BROADCASTJOIN:
          $children = PhisicalPlan::fetchChildren($linear_plan, 2);
          $node[GrammarSyntax::CHILDREN] = $children;
          $result[] = $node;
          break;
      }
    }

    return $result;
  }
}

?>
