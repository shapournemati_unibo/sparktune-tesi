<?php

abstract class GrammarSyntax {
    // Node attributes
    const ID = 'id';
    const CHILDREN = 'children';
    const OPERATION = 'operation';
    const PIPE = 'pipe';
    // Operations
    const OP_SCAN = 'Scan';
    const OP_SCANBROADCAST = 'Scan_&_Broadcast';
    const OP_SHUFFLEJOIN = 'Shuffle_join';
    const OP_BROADCASTJOIN = 'Broadcast_join';
    const OP_GROUPBY = 'Group_By';
    // Node SCAN-WRITE
    const HDFS = 'HDFS';
    const COLUMNS = 'columns';
    const FILTER = 'filter';
    const PROJECT = 'project';
    const AGGREGATE = 'aggregate';
    // Node JOIN
    const JOINKEY = 'join_keys';
    const JOINBUILD = 'join_build';
    const JOINBUILD_RIGHT = 'Right';
    const JOINBUILD_LEFT = 'Left';
    // Table
    const TABLE_NAME = 't_name';
    // Aggregate
    const AGG_KEYS = 'agg_keys';
    const AGG_FUNCTION = 'agg_function';
    const AGG_OUTPUT = 'agg_output';
}

?>
