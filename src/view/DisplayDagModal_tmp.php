<script>
  function initModal(id) {
    // Get the modal
    var modal = document.getElementById('myModal'+id);

    // Get the <span> element that closes the modal
    var span = document.getElementById("x"+id);

    // When the user clicks on <span> (x), close the modal
    span.addEventListener("click", function() {
        modal.style.display = "none";
    });

    // When the user clicks anywhere outside of the modal, close it
    window.addEventListener("click", function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    });
  }
</script>

<?php
  include_once (dirname(dirname(__FILE__)) . '/GPSJGrammarParser/GrammarSyntax.php');

  function displayDAG($dagJson) {
    $dag = json_decode($dagJson, true);
    return displayNode($dag[0]);
  }

  function displayNode($node) {
    $res = "";
    if (array_key_exists(GrammarSyntax::CHILDREN, $node)) {
      foreach ($node[GrammarSyntax::CHILDREN] as $child) {
        $res .= displayNode($child);
      }
    }
    // Create the modal
    $res .= displayModalView($node);
    // Modal listener for close
    $res .= "<script> initModal(" . $node[GrammarSyntax::ID] . ")</script>";
    return $res;
  }

  function displayModalView($node) {
    $x = $node[GrammarSyntax::ID];
    $res = "";
    $res .= '<div id="myModal'. $x.'" class="modal">';
      $res .= '<div class="modal-content">';
        $res .= '<span id="x'.$x.'" class="close">×</span>';
        $res .= "<h3>Details of the node $x (" . $node[GrammarSyntax::OPERATION] . ")</h3>";

        if (array_key_exists(GrammarSyntax::PIPE, $node)) {
          $res .= GrammarSyntax::PIPE . ": <p>" . ($node[GrammarSyntax::PIPE] == true ? 'True' : 'False') . '</p></br>';
        }
        if (array_key_exists(GrammarSyntax::HDFS, $node)) {
          $res .= GrammarSyntax::HDFS . ": <p>" . implode(", ",$node[GrammarSyntax::HDFS]) . '</p></br>';
        }
        if (array_key_exists(GrammarSyntax::COLUMNS, $node)) {
          $res .= GrammarSyntax::COLUMNS . ": <p>" . implode(", ",$node[GrammarSyntax::COLUMNS]) . '</p></br>';
        }
        if (array_key_exists(GrammarSyntax::FILTER, $node)) {
          $res .= GrammarSyntax::FILTER . ": <p>" . $node[GrammarSyntax::FILTER] . '</p></br>';
        }
        if (array_key_exists(GrammarSyntax::PROJECT, $node)) {
          $res .= GrammarSyntax::PROJECT . ": <p>" . implode(", ",$node[GrammarSyntax::PROJECT]) . '</p></br>';
        }
        if (array_key_exists(GrammarSyntax::JOINBUILD, $node)) {
          $res .= GrammarSyntax::JOINBUILD . ": <p>" . $node[GrammarSyntax::JOINBUILD] . '</p></br>';
        }
        if (array_key_exists(GrammarSyntax::AGGREGATE, $node)) {
          $res .= GrammarSyntax::AGGREGATE . ":";
          $res .= "<div class=box>";
          $res .= GrammarSyntax::AGG_KEYS . ": <p>" . implode(", ",$node[GrammarSyntax::AGGREGATE][GrammarSyntax::AGG_KEYS]) . '</p></br>';
          $res .= GrammarSyntax::AGG_FUNCTION . ": <p>" . $node[GrammarSyntax::AGGREGATE][GrammarSyntax::AGG_FUNCTION] . '</p></br>';
          //$res .= GrammarSyntax::AGG_OUTPUT . ": <p>" . implode(", ",$node[GrammarSyntax::AGGREGATE][GrammarSyntax::AGG_OUTPUT]) . '</p></br>';
          //this works only with spark 1.5
          $res .= "</div>";
        }
        if (array_key_exists(GrammarSyntax::JOINKEY, $node)) {
          $res .= GrammarSyntax::JOINKEY . ":";
          $res .= "<div class=box>";
          $res .= "Left keys: <p>" . implode(", ",$node[GrammarSyntax::JOINKEY][0]) . '</p></br>';
          $res .= "Right keys: <p>" . implode(", ",$node[GrammarSyntax::JOINKEY][1]) . '</p></br>';
          $res .= "</div>";
        }
      $res .= '</div>';
    $res .= '</div>';
    return $res;
  }

  function displayModalEdit($node) {
// TODO da implementare
    $x = $node[GrammarSyntax::ID];
    $res = "";
    $res .= '<div id="myModal'. $x.'" class="modal">';
      $res .= '<div class="modal-content">';
        $res .= '<span id="x'.$x.'" class="close">×</span>';
        $res .= "<p>Modifica parametri di $x (" . $node[GrammarSyntax::OPERATION] . ")</p>";
        $res .= '<form method="post">';

        if (array_key_exists(GrammarSyntax::HDFS, $node)) {
          $res .= GrammarSyntax::HDFS. '<input type="text" name="' . GrammarSyntax::HDFS.'" value="' . implode(", ",$node[GrammarSyntax::HDFS]).'"><br>';
        }
        if (array_key_exists(GrammarSyntax::COLUMNS, $node)) {
          $res .= GrammarSyntax::COLUMNS.'<input type="text" name="' . GrammarSyntax::COLUMNS.'" value="' . implode(", ",$node[GrammarSyntax::COLUMNS]).'"><br>';
        }
        if (array_key_exists(GrammarSyntax::FILTER, $node)) {
          $res .= GrammarSyntax::FILTER. '<input type="text" name="' . GrammarSyntax::FILTER.'" value="' . $node[GrammarSyntax::FILTER].'"><br>';
        }
        if (array_key_exists(GrammarSyntax::PROJECT, $node)) {
          $res .= GrammarSyntax::PROJECT. '<input type="text" name="' . GrammarSyntax::PROJECT.'" value="' . implode(", ",$node[GrammarSyntax::PROJECT]).'"><br>';
        }
        if (array_key_exists(GrammarSyntax::AGGREGATE, $node)) {
          $res .= GrammarSyntax::AGGREGATE. '<input type="text" name="' . GrammarSyntax::AGGREGATE.'" value="' . implode(", ",$node[GrammarSyntax::AGGREGATE]).'"><br>';
        }
        if (array_key_exists(GrammarSyntax::JOINKEY, $node)) {
          // $res .= GrammarSyntax::JOINKEY. '<input type="text" name="' . GrammarSyntax::JOINKEY.'" value="' . implode(", ",$node[GrammarSyntax::JOINKEY]).'"><br>';
        }

        $res .= '</form>';
      $res .= '</div>';
    $res .= '</div>';
    return $res;
  }
?>
