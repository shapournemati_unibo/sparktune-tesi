<script>
  function initModal(id) {
    // Get the modal
    var modal = document.getElementById('myModal'+id);

    // Get the <span> element that closes the modal
    var span = document.getElementById("x"+id);

    // When the user clicks on <span> (x), close the modal
    span.addEventListener("click", function() {
        modal.style.display = "none";
    });

    // When the user clicks anywhere outside of the modal, close it
    window.addEventListener("click", function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    });
  }
</script>

<?php
  include_once (dirname(dirname(__FILE__)) . '/GPSJGrammarParser/GrammarSyntax.php');

  function displayDAG($dagJson,$costModel) {
    $dag = json_decode($dagJson, true);
    $identifier = count($costModel->getNodesArray());
    return displayNode($dag[0],$costModel,$identifier-1);
  }

  function displayNode($node,$costModel,$identifier) {
    $res = "";
    if (array_key_exists(GrammarSyntax::CHILDREN, $node)) {
      $tmp = $identifier;
      foreach (array_reverse($node[GrammarSyntax::CHILDREN]) as $child) {
        $res .= displayNode($child,$costModel,--$tmp);
      }
    }
    // Create the modal
    $res .= displayModalView($node,$costModel,$identifier);
    // Modal listener for close
    $res .= "<script> initModal(" . $node[GrammarSyntax::ID] . ")</script>";
    return $res;
  }

  function displayModalView($node,$costModel,$identifier) {
    $x = $node[GrammarSyntax::ID];
    $res = "";
    $res .= '<div id="myModal'. $x.'" class="modal">';
      $res .= '<div class="modal-content">';
        $res .= '<span id="x'.$x.'" class="close">×</span>';
        $res .= "<h3>Details of the node $x (" . $node[GrammarSyntax::OPERATION] . ")</h3>";
        $res .= "<table class='compact-table'>
              <tr>
                <th>" . 'Node' . "</th>
                <th>" . 'Time' . "</th>
                <th>" . 'ReadTime' . "</th>
                <th>" . 'WriteTime' . "</th>
                <th>" . 'NetworkTime' . "</th>


              </tr>";

        $array = $costModel -> getNodesArray();
        $costNode = $array[$identifier];
        $opacityCoeff = (($costNode -> getNodeCost())/($costModel -> getCost()));
          echo "<script type='text/javascript'>
          alterNodeOpacity($x,$opacityCoeff);
          </script>" . PHP_EOL;
        $res .= printCostRow($costNode, "#ffacac", "''");
        $res .= "</table>";
        $res .= printTaskTypeDetails($array[$identifier]);
      $res .= '</div>';
    $res .= '</div>';
    return $res;
  }

  function displayModalEdit($node) {
// TODO da implementare
    $x = $node[GrammarSyntax::ID];
    $res = "";
    $res .= '<div id="myModal'. $x.'" class="modal">';
      $res .= '<div class="modal-content">';
        $res .= '<span id="x'.$x.'" class="close">×</span>';
        $res .= "<p>Modifica parametri di $x (" . $node[GrammarSyntax::OPERATION] . ")</p>";
        $res .= '<form method="post">';

        if (array_key_exists(GrammarSyntax::HDFS, $node)) {
          $res .= GrammarSyntax::HDFS. '<input type="text" name="' . GrammarSyntax::HDFS.'" value="' . implode(", ",$node[GrammarSyntax::HDFS]).'"><br>';
        }
        if (array_key_exists(GrammarSyntax::COLUMNS, $node)) {
          $res .= GrammarSyntax::COLUMNS.'<input type="text" name="' . GrammarSyntax::COLUMNS.'" value="' . implode(", ",$node[GrammarSyntax::COLUMNS]).'"><br>';
        }
        if (array_key_exists(GrammarSyntax::FILTER, $node)) {
          $res .= GrammarSyntax::FILTER. '<input type="text" name="' . GrammarSyntax::FILTER.'" value="' . $node[GrammarSyntax::FILTER].'"><br>';
        }
        if (array_key_exists(GrammarSyntax::PROJECT, $node)) {
          $res .= GrammarSyntax::PROJECT. '<input type="text" name="' . GrammarSyntax::PROJECT.'" value="' . implode(", ",$node[GrammarSyntax::PROJECT]).'"><br>';
        }
        if (array_key_exists(GrammarSyntax::AGGREGATE, $node)) {
          $res .= GrammarSyntax::AGGREGATE. '<input type="text" name="' . GrammarSyntax::AGGREGATE.'" value="' . implode(", ",$node[GrammarSyntax::AGGREGATE]).'"><br>';
        }
        if (array_key_exists(GrammarSyntax::JOINKEY, $node)) {
          // $res .= GrammarSyntax::JOINKEY. '<input type="text" name="' . GrammarSyntax::JOINKEY.'" value="' . implode(", ",$node[GrammarSyntax::JOINKEY]).'"><br>';
        }

        $res .= '</form>';
      $res .= '</div>';
    $res .= '</div>';
    return $res;
  }

  function printCostRow($node, $colorAffectsCost, $colorNotAffectsCost) {
      $res = "";
    if (is_array($node->getReadT())) {  // Array of values
      $size = sizeof($node->getReadT());
      for ($i=0; $i < sizeof($node->getReadT()); $i++) {
        $GLOBALS["TotReadT"] += $node->getReadT()[$i];
        if ($node->readAffectsCost()[$i]) {
          $GLOBALS["TotAffectsReadT"] += $node->getReadT()[$i];
          $bgReadT = $colorAffectsCost;
        } else {
          $bgReadT = $colorNotAffectsCost;
        }
        $GLOBALS["TotWriteT"] += $node->getWriteT()[$i];
        if ($node->writeAffectsCost()[$i]) {
          $GLOBALS["TotAffectsWriteT"] += $node->getWriteT()[$i];
          $bgWriteT = $colorAffectsCost;
        } else {
          $bgWriteT = $colorNotAffectsCost;
        }
        $GLOBALS["TotNetworkT"] += $node->getNetworkT()[$i];
        if ($node->networkAffectsCost()[$i]) {
          $GLOBALS["TotAffectsNetworkT"] += $node->getNetworkT()[$i];
          $bgNetworkT = $colorAffectsCost;
        } else {
          $bgNetworkT = $colorNotAffectsCost;
        }
        switch ($i) {
          case 0:
            $iPrnt = "L";
            break;
          case 1:
            $iPrnt = "R";
            break;
          case 2:
            $iPrnt = "C";
            break;
        }
        $res .= "<tr>";
        if ($i == 0) {
          $res .= "<td rowspan=$size>" . $node . "</td>";
          $res .= "<td rowspan=$size>" . seconds($node->getNodeCost()) . "</td>";
        }
        $res .= "<td bgcolor=$bgReadT>" . "$iPrnt: " . formatNumber($node->getReadT()[$i]) . "</td>";
        $res .= "<td bgcolor=$bgWriteT>" . "$iPrnt: " . formatNumber($node->getWriteT()[$i]) . "</td>";
        $res .= "<td bgcolor=$bgNetworkT>" . "$iPrnt: " . formatNumber($node->getNetworkT()[$i]) . "</td>";
        }
        $res .= "</tr>";

    } else {  // Simple value
      $GLOBALS["TotReadT"] += $node->getReadT();
      if ($node->readAffectsCost()) {
        $GLOBALS["TotAffectsReadT"] += $node->getReadT();
        $bgReadT = $colorAffectsCost;
      } else {
        $bgReadT = $colorNotAffectsCost;
      }
      $GLOBALS["TotWriteT"] += $node->getWriteT();
      if ($node->writeAffectsCost()) {
        $GLOBALS["TotAffectsWriteT"] += $node->getWriteT();
        $bgWriteT = $colorAffectsCost;
      } else {
        $bgWriteT = $colorNotAffectsCost;
      }
      $GLOBALS["TotNetworkT"] += $node->getNetworkT();
      if ($node->networkAffectsCost()) {
        $GLOBALS["TotAffectsNetworkT"] += $node->getNetworkT();
        $bgNetworkT = $colorAffectsCost;
      } else {
        $bgNetworkT = $colorNotAffectsCost;
      }
      $res .= "<tr>";
      $res .= "<td>" . $node . "</td>";
      $res .= "<td>" . seconds($node->getNodeCost()) . "</td>";
      $res .= "<td bgcolor=$bgReadT>" . formatNumber($node->getReadT()) . "</td>";
      $res .= "<td bgcolor=$bgWriteT>" . formatNumber($node->getWriteT()) . "</td>";
      $res .= "<td bgcolor=$bgNetworkT>" . formatNumber($node->getNetworkT()) . "</td>";

      $res .= "</tr>";
    }
    return $res;
  }

  function printTaskTypeDetails($node){
    $res = "";
    if (is_array($node->getReadT())) { //array of values
      $res .= "<table class='compact-table'>
            <tr>
              <th>" . 'Local' . "</th>
              <th>" . 'Rack' . "</th>
              <th>" . 'Cluster' . "</th>
              <th colspan=". sizeof($node->getFunctionDetails()[Details::TABLE_IN]) .">" . "<img src='". RESPATH . "dataset_in.png' width='30' vspace='1' align='left'>" .'Input Table(s)' . "</th>
              <th>" . "<img src='". RESPATH . "dataset_out.png' width='30' vspace='1' align='left'>" . 'Output Table' . "</th>
            </tr>";
      $res .= "<tr>";
      for ($i=0; $i < sizeof($node->getReadT()); $i++) {
        $res .= "<td>" . formatFunctionDetails($node->getFunctionDetails()[$i][Details::GENERIC]) . "</td>";
      }

    } else { //single value
      $res .= "<table class='compact-table'>
            <tr>
              <th>" . 'Details' . "</th>
              <th colspan=". sizeof($node->getFunctionDetails()[Details::TABLE_IN]) .">" . "<img src='". RESPATH . "dataset_in.png' width='30' vspace='1' align='left'>" . 'Input Table(s)' . "</th>
              <th>" . "<img src='". RESPATH . "dataset_out.png' width='30' vspace='1' align='left'>" .'Output Table' . "</th>
            </tr>";
      $res .= "<td>" . formatFunctionDetails($node->getFunctionDetails()[Details::GENERIC]) . "</td>";
    }

    $inputTables = $node->getFunctionDetails()[Details::TABLE_IN];
    foreach ($inputTables as $key => $tbl){

      $res .= "<td>" . "<b>Name: ".$key."</b><br>" .formatFunctionDetails($tbl) . "</td>";
    }
    $outputTable = $node->getFunctionDetails()[Details::TABLE_OUT];
    $res .= "<td>" . formatFunctionDetails($outputTable) . "</td>";
    $res .= "</table>";

    return $res;
  }

  function printBasicBricksDetails($basicBricks_list) {
    $res = "<div class=actions>";
    foreach ($basicBricks_list as $key => $bb) {
      $res .= "<div title='";
      $res .= formatFunctionDetails($bb->getFunctionDetails());
      $res .= "'>";
      $res .= "<p>" . $key . "</p>";
      $res .= "<img src='". RESPATH . "BB.png' width='30' vspace='1' align='middle'>";
      $res .= "</div>";
    }
    $res .= "</div>";
    return $res;
  }

  function printTablesDetails($tablesList) {
    $res = "<div class=actions>";
    foreach ($tablesList as $key => $tbl) {
      $res .= "<div title='";
      $res .= formatFunctionDetails($tbl);
      $res .= "'>";
      $res .= "<p>" . $key . "</p>";
      $res .= "<img src='". RESPATH . "dataset_in.png' width='30' vspace='1' align='middle'>";
      $res .= "</div>";
    }
    $res .= "</div>";
    return $res;
  }

  function formatFunctionDetails($functionDetails) {
    $strRes = "";
    $newline = " <br>";
    foreach ($functionDetails as $label => $value) {
      if (strtolower($label) == "size" || strpos(strtolower($label), "size")) {
        $value = bytes($value);
      } else if (strtolower($label) == "throughput" || strpos(strtolower($label), "throughput")) {
        $value = bytesSec($value);
      }
      if (is_numeric($value)) {
        $value = formatNumber($value);
      }
      $strRes .= "$label: $value $newline";
    }
    return $strRes;
  }





  function bytesSec($a) {
    $unim = "MBps";
    $a = $a/1024/1024;
    return formatNumber($a) . $unim;
  }

  function bytes($a) {
    $unim = array("B","KB","MB","GB","TB","PB");
    $c = 0;
    while ($a>=1024 && $c<sizeof($unim)-1) {
      $c++;
      $a = $a/1024;
    }
    return number_format($a,($c ? 2 : 0),",",".")." ".$unim[$c];
  }
?>
