function createSVG(id, idParent) {
  var diagonal = d3.svg.diagonal()
    .projection(function(d) {
        return [d.y, d.x];
    });

  return d3.select("#" + idParent)
   .append("svg")
   .attr("id", id);
}

function alterNodeOpacity(id,opacity) {
  var nodeID = "#node_" + id;
  d3.select(nodeID).style("fill-opacity", opacity);
}

function initSVG(id, idParent, margin, rootTree, clickCallback) {
  // Initialize the svg and the strucontaining structure (here is also calculated the depth of the tree)
  var treeDepth = getTreeDepth(rootTree);
  var svg = createSVG(id, idParent);

  var width = $("#" + idParent).width() - margin.right - margin.left,
      height = width/3.5 - margin.top - margin.bottom;

  svg.attr("preserveAspectRatio", "xMinYMin meet")
    .attr("viewBox", "0 0 " + width + " " + height)
    // Rotate RtL
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var tree = d3.layout.tree().size([height, width]);
  var nodes = tree.nodes(rootTree),
      links = tree.links(nodes);

  var diagonal = d3.svg.diagonal()
    .projection(function(d) {
        return [d.y, d.x];
    });

  // Normalize for fixed-depth.
  nodes.forEach(function(d) {
      d.y = (width/treeDepth) * (treeDepth-d.depth) - (width/treeDepth)*0.5;
  });

  // Declare the nodes…
  var i = 0;
  var node = svg.selectAll("g.node")
      .data(nodes, function(d) {
          return d.id || (d.id = ++i);
      });

  // Tree nodes title:
  var nodeTitle = function(d) {
    return "(" + d.id + ") " + d.operation;
  };

  var nodeID = function(d) {
    return "node_" + d.id;
  }

  // Tree node box size. It is dinamically calculated based on the title length.
  var rectW = function(d) {
        return nodeTitle(d).length * 10;
      },
      rectX = function(d) {
        return -rectW(d) / 2;
      },
      rectH = 30,
      rectY = -rectH/2;

  // Enter the nodes.
  var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) {
          return "translate(" + d.y + "," + d.x + ")";
      });
  if (clickCallback && typeof(clickCallback) === "function") {
      nodeEnter.on("click", clickCallback);
  }

  //double rect, this is used to create a plain white background,
  //in order to hide the links below due to the opacity < 1
  nodeEnter.append("rect")
      .attr("width", rectW)
      .attr("height", rectH)
      .attr("x", rectX)
      .attr("y", rectY)
      .attr("stroke", "black")
      .attr("stroke-width", 1)
      .style("fill", function(d) {
          return "#fff";
      })
      .style("fill-opacity", 1);

  nodeEnter.append("rect")
      .attr("width", rectW)
      .attr("height", rectH)
      .attr("x", rectX)
      .attr("y", rectY)
      .attr("id", nodeID)
      .attr("stroke", "black")
      .attr("stroke-width", 1)
      .style("fill", function(d) {
          return "#ff0000";
      })
      .style("fill-opacity", 1);

  nodeEnter.append("text")
      .attr("x", "0")
      .text(nodeTitle)
      .style("text-anchor", "middle")
      .style("fill-opacity", 1);

  // Declare the links…
  var link = svg.selectAll("path.link")
      .data(links, function(d) {
          return d.target.id;
      });

  // Enter the links.
  link.enter().insert("path", "g")
      .attr("class", "link")
      .attr("d", diagonal)
      .style("stroke", function(o) {
          return o.target.pipe === undefined ? "green" : o.target.pipe === true ? "blue" : "black";
      });

}

function getTreeDepth(root) {
  if (root.children) {
    var depthMax = 0;
    root.children.forEach(function(d) {
      var depth = getTreeDepth(d);
      depthMax = Math.max(depth, depthMax);
    });
    return depthMax+1;
  } else {
    return 1;
  }
}
