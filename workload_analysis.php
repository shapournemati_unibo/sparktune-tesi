<?php
require_once(dirname(__FILE__) . '/load-config.php');
include_once(dirname(__FILE__) . '/src/view/GrammarDerivationView.php');
include_once(dirname(__FILE__) . '/src/GPSJGrammarParser/PhisicalPlan.php');
include_once(dirname(__FILE__) . '/src/costModel/CostModel.php');
include_once(dirname(__FILE__) . '/src/costModel/Database.php');
include_once(dirname(__FILE__) . '/src/costModel/Table.php');
include_once(dirname(__FILE__) . '/user/DatabaseQuery.php');

$_SESSION['currentWorkload'] = 0;

  const key_action = 'action';
  const key_query = 'selected_query';
  const key_cluster = 'key_cluster';
  const key_database = 'key_database';
  const key_nRE = 'nRE';
  const key_nEC = 'nEC';
  const key_workload = 'workload';

  $action = NULL;
  $query = NULL;
  $cluster = NULL;
  $database = NULL;
  $nRE = NULL;
  $nEC = NULL;
  $workload = NULL;


  if (isset($_REQUEST[key_action])) {
    $action = $_REQUEST[key_action];
  }
  if (isset($_REQUEST[key_query])) {
    $query = $_REQUEST[key_query];
  }
  if (isset($_REQUEST[key_cluster])) {
    $cluster = $_REQUEST[key_cluster];
    $clusterParam = DatabaseQuery::getCluster($cluster);
  } else {
    $clusterParam = DatabaseQuery::getCluster(1);
  }
  if (isset($_REQUEST[key_database])) {
    $database = $_REQUEST[key_database];
  }
  if (isset($_REQUEST[key_nRE])) {
    $nRE = $_REQUEST[key_nRE];
  }
  if (isset($_REQUEST[key_nEC])) {
    $nEC = $_REQUEST[key_nEC];
  }

  if (isset($_REQUEST[key_workload])) {
    $workload = $_REQUEST[key_workload];
  }




 ?>


<!doctype html>
<html>
  <head>
    <title>Spark cost</title>

    <!-- Il file contiene una serie di librerie utilizzate in tutto l'applcativo-->
    <?php
      include ABSTEMPLATEPATH . 'head.php';
      GrammarDerivationView::printHeaders();
    ?>

	<!-- JAVASCRIPT PAGINA -->
	<script src="template/js/sparkcost.js" type="text/javascript"></script>
	<script src="template/js/index.js" type="text/javascript"></script>
  <script type="text/javascript">
  var cluster = <?php echo $cluster ?>;
  var database = <?php echo $database ?>;
  var workload = <?php echo $workload ?>;
  var cores = <?php echo $nEC ?>;
  var executors = <?php echo $nRE ?>;

  $(document).on('change keyup paste','.checkChange',function(){
    if(cluster != $("#checkCluster").val() ||
       database != $("#checkDatabase").val() ||
       workload != $("#checkWorkload").val() ||
       cores != $("#checkCores").val() ||
       executors != $("#checkExecutors").val()){
         $(".checkEnabled").bind("click",false);
         $(".checkEnabled").css("background-color","grey");
       } else {
         $(".checkEnabled").unbind("click",false);
         $(".checkEnabled").css("background-color","");
       }
 });

 $(document).ready(function() {
   // Get the modal
   var modal = document.getElementById('sqlModal');

   // Get the <span> element that closes the modal
   var span = document.getElementById("x");

   // When the user clicks on <span> (x), close the modal
   span.addEventListener("click", function() {
       modal.style.display = "none";
   });

   // When the user clicks anywhere outside of the modal, close it
   window.addEventListener("click", function(event) {
       if (event.target == modal) {
           modal.style.display = "none";
       }
   });

   var button = document.getElementById('displaySQL');
   button.addEventListener("click", function() {
       modal.style.display = "block";
   });

 });
 </script>

	<!--<script src="js/maintenance.js" type="text/javascript"></script>-->

	</head>
	<body style="<!--background-image: url('img/bg10.png');--> position: relative; ">

		<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
      <?php
        include ABSTEMPLATEPATH . 'header.php';
        echo '<link rel="stylesheet" href="'.TEMPLATEPATH.'/styles/style.css"/>';

      ?>

		<div id="contTopicCharts" class="" style="padding:0 15 5 15;">

			<div id="contTopicSelection" class="row" style="margin-bottom: 5px; padding:0px;">
			</div>


			<div class="row">

				<div id="selections" class="col-xs-2" style="">
          <div id="chartOptions" class="cont contLeft">
          	<div class="chartTitle">
          		<span id="chartOptionsTitleText" >Parameters</span>
          	</div>
          	<div id="chartOptionsCont" class="chart" style="padding: 5px;">

          			<div class="panel-group" id="selectionOptions" style="margin-bottom:0px;">

          				<div id="allOptionsContainer">
          					<?php
                    # Print cluster parameter
                    echo '<form class="form">' . PHP_EOL;
                      echo '<label class="card_label">';
                        echo "Cluster: " . printFormClusterParameters($cluster) . PHP_EOL;
                      echo '</label>';
                      # Print database parameter
                      echo '<label class="card_label">';
                        echo "Database: " . printFormDatabaseParameters($database) . PHP_EOL;
                      echo '</label>';
                      # Print the selection for a workload query

                      echo '<label class="card_label">';
                        echo "Workload: " . printFormWorkloadParameters($workload) . PHP_EOL;
                      echo '</label>';

                      # Print query parameters
                      echo printFormQueryParameters($nRE, $nEC,(int) $clusterParam[DatabaseValues::CL_N_R]*(int) $clusterParam[DatabaseValues::CL_N_RN],(int) $clusterParam[DatabaseValues::CL_N_C]) . PHP_EOL;
                      echo '<div class="actions">' . PHP_EOL;
                        # Print the buttons for the cost computation and optimization
                        echo '<input type="submit" name=action value="Compute" />' . PHP_EOL;
                      echo '</div>' . PHP_EOL;
                    echo '</form>' . PHP_EOL;
                     ?>
          				</div>

          			</div>

          	</div>
          </div>
				</div>

				<div id="topicCharts" class="col-xs-10">
					<div class="col-xs-12" style="padding:0px 5px;">
						<div id="olMapContainer" class="cont" style="text-align:center">
							<div class="chartTitle">
								<span id="olMapTitle">Workload Analysis</span><span id="olMapStats" class="titleStats"></span>
								<span style="float:right; font-size:12px; margin: 2px 5px;"></span>
							</div>
              <div>


              <?php
              if ($action=='Compute') {
                if (isset($_REQUEST[key_cluster])
                    && isset($_REQUEST[key_database])
                    && isset($_REQUEST[key_nRE])
                    && isset($_REQUEST[key_nEC])
                    && isset($_REQUEST[key_workload])) {

                echo printWorkloadSelection($cluster,$database,$nRE,$nEC,$action,$workload);

                $queries = $_SESSION['workload'];
                $workloadCost = 0;
                $parameters = getClusterParameters($_REQUEST[key_cluster], $_REQUEST[key_nRE], $_REQUEST[key_nEC]);


                foreach ($queries as $key => $value) {
                  $physicalDatabase = getDatabase($_REQUEST[key_database]);
                  $grammarDerivation = (new PhisicalPlan($value, $_SESSION['sparkVersion']))->getGrammarDerivationJson();
                  $costModel = new CostModel($parameters,$physicalDatabase,$grammarDerivation);
                  $workloadCost += $costModel->getCost();

                }

                echo "<section class='no-border'>";
                echo "<table class='table-total'>";
                echo "<tr>
                        <th>" . 'Total Workload Time' . "</th>
                      </tr>";
                echo "<tr>
                      <td rowspan=2>" . seconds($workloadCost) . "</td>
                      </tr>";
                echo "</table>";
                echo "</section>";
                }
              }
               ?>
               </div>

						</div>

            <?php
            if ($action=='Compute') {
              if (isset($_REQUEST[key_cluster])
                  && isset($_REQUEST[key_database])
                  && isset($_REQUEST[key_nRE])
                  && isset($_REQUEST[key_nEC])) {
            if(isset($_REQUEST["chosen_query"])){
              echo '<div class="cont">';
              echo '<div class="chartTitle" style="margin-top:1em;">
               <span>Query '. $_REQUEST["queryNum"] .'</span><button id="displaySQL" style="margin:5px;background-color:blue;">Display SQL</button><span class="titleStats"></span>
               <span style="float:right; font-size:12px; margin: 2px 5px;"></span>
             </div>';

             echo '<div id="sqlModal" class="modal">
             <div class="modal-content">
             <span id="x" class="close">×</span>
             <h3>SQL of the selected query</h3>
             <div>';
             echo str_replace(["\r\n", "\r", "\n"], "<br/>", DatabaseQuery::getQuerySQL($_REQUEST["chosen_query"]));
             echo '</div>
             </div>
             </div>';

             echo '<div>';


            //accessed and setted during the processing of the costModel by the grammarDerivationView
            $TotReadT = 0;
            $TotAffectsReadT = 0;
            $TotWriteT = 0;
            $TotAffectsWriteT = 0;
            $TotNetworkT = 0;
            $TotAffectsNetworkT = 0;


            $physicalDatabase = getDatabase($_REQUEST[key_database]);
            $grammarDerivation = (new PhisicalPlan($queries[$_REQUEST['chosen_query']], $_SESSION['sparkVersion']))->getGrammarDerivationJson();
            $costModel = new CostModel($parameters,$physicalDatabase,$grammarDerivation);

            $grammarDerivationView = new GrammarDerivationView($grammarDerivation,$costModel);

            echo "<section id='svg-container' class='full-width'>";
            $grammarDerivationView->printGPSJD3("svg-container");
            $grammarDerivationView->printGPSJModals("svg-container");
            echo "</section>";
            echo "<section class='no-border'>";
            echo "<table class='table-query'>";
            echo "<tr>
                    <th>" . 'Time' . "</th>
                    <th>" . 'ReadTime' . "</th>
                    <th>" . 'WriteTime' . "</th>
                    <th>" . 'NetworkTime' . "</th>
                  </tr>";
                  echo "<tr>
                   <td rowspan=2>" . seconds($costModel->getCost()) . "</td>
                   <td>AFFECTED: " . seconds($GLOBALS["TotAffectsReadT"]) . "</td>
                   <td>AFFECTED: " . seconds($GLOBALS["TotAffectsWriteT"]) . "</td>
                   <td>AFFECTED: " . seconds($GLOBALS["TotAffectsNetworkT"]) . "</td>
                   </tr>
                   <tr>
                   <td>WORKED: " . seconds($GLOBALS["TotReadT"]) . "</td>
                   <td>WORKED: " . seconds($GLOBALS["TotWriteT"]) . "</td>
                   <td>WORKED: " . seconds($GLOBALS["TotNetworkT"]) . "</td>
                   </tr>";
            echo "</table>";
            echo "</section>";
            echo '</div>';
            echo '</div>';
            }
          }
        }
             ?>
					</div>
				</div>

			</div>
		</div>
		</div>


	</body>
</html>

<?php
function printFormQueryParameters($nRE = NULL, $nEC = NULL,$maxExecutors,$maxCores) {
  $res = '<label class="card_label">';
    $res .= 'Number of Executors in each Rack (#RE):';
    $res .= '<input class="checkChange" id="checkExecutors" type="number" name="nRE" min=0 max='.$maxExecutors.' autocomplete="off"';
    $res .= 'title="Only numbers are allowed."';
    if ($nRE) {
      $res .= "value='$nRE'";
    }
    $res .= 'placeholder="Number of Executors">';
  $res .= '</label>';

  $res .= '<label class="card_label">';
    $res .= 'Number of Cores on each Executor (#EC):';
    $res .= '<input class="checkChange" id="checkCores" type="number" name="nEC" min=0 max='.$maxCores.' autocomplete="off"';
    $res .= 'title="Only numbers are allowed."';
    if ($nEC) {
      $res .= "value='$nEC'";
    }
    $res .= 'placeholder="Number of Cores">';
  $res .= '</label>';
  return $res;
}

function printFormClusterParameters($defaultCluster = NULL) {
  $res = '<select class="checkChange" id="checkCluster" name="' . key_cluster . '">';
  $res .= '<option selected disabled>Choose the cluster</option>';
  foreach (DatabaseQuery::getClusters(LoginStatus::getUserId()) as $key => $value) {
    $name = $value[DatabaseValues::CL_Name];
    $id = $value[DatabaseValues::CL_ClusterId];
    $res .= '<option ';
      if ($defaultCluster && $id == $defaultCluster)
        $res .= 'selected="selected"';
      $res .= 'value="' . $key . '">';
      $res .= $name;
  }
  $res .= '</select>';
  return $res;
}

function printFormDatabaseParameters($defaultDatabase = NULL) {
  $res = '<select class="checkChange" id="checkDatabase" name="' . key_database . '">';
  $res .= '<option selected disabled>Choose the database</option>';
  foreach (DatabaseQuery::getDatabases(LoginStatus::getUserId()) as $key => $value) {
    $name = $value[DatabaseValues::DB_Name];
    $id = $value[DatabaseValues::DB_ID];
    $res .= '<option ';
      if ($defaultDatabase && $id == $defaultDatabase)
        $res .= 'selected="selected"';
      $res .= 'value="' . $key . '">';
      $res .= $name;
  }
  $res .= '</select>';
  return $res;
}

function printFormWorkloadParameters($defaultWorkload = NULL){
  $res = '<select class="checkChange" id="checkWorkload" name="' . key_workload . '">';
  $res .= '<option selected disabled>Choose the workload</option>';
  foreach (DatabaseQuery::getWorkloads(LoginStatus::getUserId()) as $key => $value) {
    $name = $value[DatabaseValues::WL_NAME];
    $id = $value[DatabaseValues::WL_ID];
    $res .= '<option ';
      if ($defaultWorkload && $id == $defaultWorkload)
        $res .= 'selected="selected"';
      $res .= 'value="' . $key . '">';
      $res .= $name;
  }
  $res .= '</select>';
  return $res;
}


function printWorkloadSelection($cluster,$database,$nRE,$nEC,$action,$workload){
  $res = "";
  if($_SESSION['currentWorkload'] != $workload){
    $_SESSION['currentWorkload'] = $workload;
    $_SESSION['sparkVersion'] = DatabaseQuery::getWorkloadVersion($workload);
    $_SESSION['workload'] = DatabaseQuery::getQueries($workload);
  }


  $res .= '<div class="actions" style="display:flex">';
  $buttonSize = 100/(count($_SESSION['workload'])+1);
  $queryArr = $_SESSION['workload'];
  $i = 1;
  $res .= '<a class="checkEnabled" style="width:'.$buttonSize.'%" href="'.URL.'workload_analysis.php'. getUrlOptions($cluster,$database,$nRE,$nEC,$action,$workload).'">Workload</a>';

  foreach ($queryArr as $key => $value) {
    $res .= '<a class="checkEnabled" style="width:'.$buttonSize.'%" href="'.URL.'workload_analysis.php'. getUrlOptions($cluster,$database,$nRE,$nEC,$action,$workload,$i).'&chosen_query='.$key.'"> Query '.($i).'</a>';
    $i++;
  }

  $res .= '</div>';

  return $res;
}

function getUrlOptions($cluster,$database,$nRE,$nEC,$action,$workload,$queryNum = null){
  $urlOptions = '?';
  if($queryNum != null){
    $urlOptions .= 'queryNum=' . $queryNum . '&' ;
  }
  $urlOptions .= key_cluster . '=' . $cluster.'&';
  $urlOptions .= key_database . '=' . $database . '&';
  $urlOptions .= key_nRE . '=' . $nRE . '&';
  $urlOptions .= key_nEC . '=' . $nEC . '&';
  $urlOptions .= key_workload . '=' . $workload . '&';
  $urlOptions .= key_action . '=' . $action;

  return $urlOptions;
}

function getDatabase($databaseId) {
  $databaseSql = DatabaseQuery::getDatabase($databaseId);
  $database = new Database($databaseSql[DatabaseValues::DB_Name]);
  $tablesSql = DatabaseQuery::getTables($databaseSql[DatabaseValues::DB_ID]);
  foreach ($tablesSql as $tableId => $tableSql) {
    // $name, $cardinality, $size, $nPartitions
    $table = new Table(
      $tableSql[DatabaseValues::TBL_Name],
      $tableSql[DatabaseValues::TBL_Cardinality],
      $tableSql[DatabaseValues::TBL_Size],
      $tableSql[DatabaseValues::TBL_N_Partitions]
    );
    $attributesSql = DatabaseQuery::getAttributes($tableId);
    foreach ($attributesSql as $attributeId => $attributeSql) {
      // $name, $cardinality, $length, $high, $low
      $attribute = new Attribute(
        $attributeSql[DatabaseValues::ATT_Name],
        $attributeSql[DatabaseValues::ATT_Cardinality],
        $attributeSql[DatabaseValues::ATT_AvgLength],
        $attributeSql[DatabaseValues::ATT_Highest],
        $attributeSql[DatabaseValues::ATT_Lowest]
      );
      $table->addAttribute($attribute);
    }
    $database->addTable($table);
  }
  return $database;
}

function getClusterParameters($clusterId, $nRE, $nEC) {
  $cluster = DatabaseQuery::getCluster($clusterId);
  if (!is_null($cluster)) {
    $parameters = new CostFunctionParameters(
      (int) $cluster[DatabaseValues::CL_N_R],
      (int) $cluster[DatabaseValues::CL_N_RN],
      (int) $cluster[DatabaseValues::CL_N_C],
      (int) $cluster[DatabaseValues::CL_Rf],
      function ($nProc) use ($clusterId) { // discR(#Proc)
        $values = DatabaseQuery::getThroughputs($clusterId, DatabaseValues::TR_Type_DR);
        $maxProc = max(array_keys($values));
        if ($nProc <= $maxProc) {

        return $values[$nProc-1] *1024*1024;
        } else {

          return $values[$maxProc-1] *1024*1024;
        }
      },
      function ($nProc) use ($clusterId) { // discW(#Proc)
        $values = DatabaseQuery::getThroughputs($clusterId, DatabaseValues::TR_Type_DW);
        $maxProc = max(array_keys($values));
        if ($nProc <= $maxProc) {
          return $values[$nProc-1] *1024*1024;
        } else {
          return $values[$maxProc-1] *1024*1024;
        }
      },
      function ($nProc) use ($cluster) { // netI(#Proc)
        return $nProc <= 0 ? $cluster[DatabaseValues::CL_IntraRSpeed] : $cluster[DatabaseValues::CL_IntraRSpeed] / $nProc;
      },
      function ($nProc) use ($cluster) { // netE(#Proc)
        return $nProc <= 0 ? $cluster[DatabaseValues::CL_ExtraRSpeed] : $cluster[DatabaseValues::CL_ExtraRSpeed] / $nProc;
      },
      (int) $cluster[DatabaseValues::CL_N_SB],
      (float) $cluster[DatabaseValues::CL_SCmp],
      (float) $cluster[DatabaseValues::CL_FCmp],
      (float) $cluster[DatabaseValues::CL_HSel],
      $nRE,
      $nEC);
  }
  return $parameters;
}

function seconds($a) {
  $sec = $a;
  $unim = array("s","m","h");
  $c = 0;
  while ($a>=60 && $c<sizeof($unim)-1) {
    $c++;
    $a = $a/60;
  }
  return number_format($a,($c ? 2 : 0),",",".")." ".$unim[$c] . " (" . formatNumber($sec) . "s)";
}

function formatNumber($number) {
  return number_format($number, 2, '.'," ");
}
 ?>
