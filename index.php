<?php
  require_once(dirname(__FILE__) . '/load-config.php');
  include_once(ABSAUTHPATH . "loginStatus.php");

  LoginStatus::checkLogin();

  if (LoginStatus::isLogged()){
    header('Location: '. URL . 'workload_analysis.php'); //default location after login
  }
 ?>
