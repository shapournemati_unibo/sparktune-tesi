<?php
	if(file_exists('../analytics.php')) include '../analytics.php';
	else if(file_exists('../../analytics.php')) include '../../analytics.php';
?>

<?php
$template=TEMPLATEPATH;
$host=dirname($_SERVER['HTTP_HOST']);
require_once ABSAUTHPATH."loginStatus.php";
if (session_status()==PHP_SESSION_NONE) {
  session_start();
}
LoginStatus::checkLogin();

echo '<link rel="stylesheet" href="'.$template.'/styles/material.min.css">';
echo '<script src="'.$template.'/js/material.min.js"></script>';

?>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<header class="mdl-layout__header">
	<div class="mdl-layout__header-row">
	  <!-- Title -->
	  <span class="mdl-layout-title" style="text-align: left;">SparkTune<br/><span style="font-size:14px">Tuning Spark SQL through query cost modeling</span></span>
	  <!-- Add spacer, to align navigation to the right -->
	  <div class="mdl-layout-spacer"></div>
	  <!-- Navigation. We hide it in small screens. -->
    <nav class="mdl-navigation mdl-layout--large-screen-only">
    <a id="submenu_setup" class="mdl-navigation__link" href="#">SETUP</a>
    <a id="submenu_analysis" class="mdl-navigation__link" href="#">ANALYSIS</a>
		<?php include("login.php");?>
		<a href="http://big.csr.unibo.it"><img src=<?php echo RELPATH . "img/10simple.png";?> class="img-responsive" style="height:55px; margin-left: 50px;"></a>
	  </nav>


	</div>
</header>
<ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect"
for="submenu_analysis">
  <a class="mdl-menu__item" href=<?php echo RELPATH . "workload_analysis.php";?>>WORKLOAD ANALYSIS</a>
  <a class="mdl-menu__item" href=<?php echo RELPATH . "cost_analysis.php";?>>COST ANALYSIS</a>
  <a class="mdl-menu__item" href=<?php echo RELPATH . "cluster_analysis.php";?>>CLUSTER ANALYSIS</a>
  <a class="mdl-menu__item" href=<?php echo RELPATH . "performance_analysis.php";?>>PERFORMANCE ANALYSIS</a>
</ul>
<ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect"
for="submenu_setup">
<a class="mdl-menu__item" href=<?php echo RELPATH . "user/clusters.php";?>>CLUSTERS</a>
<a class="mdl-menu__item" href=<?php echo RELPATH . "user/databases.php";?>>DATABASES</a>
<a class="mdl-menu__item" href=<?php echo RELPATH . "user/setup.php";?>>WORKLOAD</a>
</ul>

<div class="mdl-layout__drawer" style="text-align: left;">
    	<span class="mdl-layout-title" style="border-bottom: 1px solid rgba(0,0,0,0.1);">Mo.Re.Farming</span>
    	<nav class="mdl-navigation" style="padding-top: 0px;">
    		<a class="mdl-navigation__link mdl-navigation__link-custom" href=<?php echo RELPATH . "workload_analysis.php";?>>WORKLOAD ANALYSIS</a>
        <a class="mdl-navigation__link mdl-navigation__link-custom" href=<?php echo RELPATH . "cost_analysis.php";?>>COST ANALYSIS</a>
    		<a class="mdl-navigation__link mdl-navigation__link-custom" href=<?php echo RELPATH . "cluster_analysis.php";?>>CLUSTER ANALYSIS</a>
    		<a class="mdl-navigation__link mdl-navigation__link-custom" href=<?php echo RELPATH . "performance_analysis.php";?>>PERFORMANCE ANALYSIS</a>
    		<a class="mdl-navigation__link mdl-navigation__link-custom" href="http://big.csr.unibo.it">About B.I.G.</a>
    	</nav>
    </div>
