<?php
  require_once(dirname(dirname(__FILE__)) . '/load-config.php');
  require_once ABSAUTHPATH."loginStatus.php";



    $current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    if (LoginStatus::isLogged()) {
      echo "<a class='mdl-navigation__link' href='" . AUTHPATH . "logout.php?redirect=" . $current_url . "'>LOGOUT</a>";
    } else {
      echo "<a class='mdl-navigation__link' href='" . AUTHPATH . "login.php?redirect=" . $current_url . "'>LOGIN</a>";
    }


?>
