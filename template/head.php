<?php
$template=TEMPLATEPATH;

echo '<!-- LIBRERIE JAVASCRIPT -->
<script src="'.$template.'/js/jquery.min.js"></script>
<script src="'.$template.'/js/bootstrap.min.js"></script>
<script src="'.$template.'/js/bootstrap-datepicker.js"></script>
<script src="'.$template.'/js/bootstrap-select.js"></script>
<script src="'.$template.'/js/bootstrap-slider.js"></script>
<script src="'.$template.'/js/bootstrap-switch.js"></script>
<script src="'.$template.'/js/moment.min.js"></script>

<!-- CSS LIBRERIE -->
<link rel="stylesheet" href="'.$template.'/styles/bootstrap.css"/>
<link rel="stylesheet" href="'.$template.'/styles/slider.css"/>
<link rel="stylesheet" href="'.$template.'/styles/bootstrap-switch.css"/>
<link rel="stylesheet" href="'.$template.'/styles/bootstrap-select.css"/>
<link rel="stylesheet" href="'.$template.'/styles/datepicker.css"/>
<link rel="stylesheet" href="https://openlayers.org/en/v3.20.1/css/ol.css" type="text/css">
<link rel="stylesheet" href="'.$template.'/styles/blueimp-gallery.min.css">

<!-- CSS PAGINA -->
<link href="http://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="'.$template.'/styles/sparkcost.css"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<link rel="shortcut icon" type="image/x-icon" href="'. $template .'favicon.ico" />';

?>
<script>
$(document).ready(function(){
  $(".compactable").hide();

  $(".expandeable").click(function(){
    if ($(this).siblings(".compactable").is(":visible")) {
      $(this).siblings(".compactable").slideUp("slow", "linear");
    } else {
      $(this).siblings(".compactable").slideDown("slow", "linear");
    }
  });
});
</script>
