window.onload = function() {
	$.blockUI.defaults.css.border = 'none';
	$.blockUI.defaults.css.padding = '15px', 
	$.blockUI.defaults.css.backgroundColor = '#000';
	$.blockUI.defaults.css.opacity = .5;
	$.blockUI.defaults.css.color = '#fff';
	$.blockUI.defaults.css.cursor = null;
	$.blockUI.defaults.message = 'Temporarily down for maintenance. We will be back online shortly.';
	
	$.blockUI(); 
};

	