function escapeName(name){
	return name.replace(/'/g, "\\'");
}
			
function initializeComponents() {
	$('.slider').each(function(){
		if(this.id=="sl_content_length"){
			var valMap = [0, 25, 50, 100, 250, 500];
			$(this).slider({
				min: 0, max: 5, value: [0,5], step: 1, 
				slide: function(event, ui) {           
					$('#sl_content_length').closest('.sliderCont').find('div[class~="tooltip-inner"]').html(valMap[ui.values[0]] + ' : ' + valMap[ui.values[1]])
				}  
			});      
			$('#sl_content_length').closest('.sliderCont').find('div[class~="tooltip-inner"]')
				.html("0 : 500");			
		}
		else $(this).slider();
	});
	
	$('#to_topic').selectpicker();
	$('#to_groupby').selectpicker();
	$('#to_semantic').selectpicker();
	$('#se_SourcechannelName').selectpicker();
	
	$(".selOptCheckEnabler").change(function(){
		if(this.checked){
			$(this).closest('div[class~="input-group"]').find('input[class~="selOptInput"]').removeAttr('disabled');
			$(this).closest('div[class~="input-group"]').find('span[class~="sliderCont"]').removeAttr('disabled');
			$(this).closest('div[class~="date"]').datepicker({ 
				autoclose: true, 
				format: "dd/mm/yyyy", 
				startDate: moment(Math.min.apply(null,global_availableDates.map(function(o){return moment(o.date,'YYYY-MM-DD').toDate();}))).format('DD/MM/YYYY'), 
				endDate: moment(Math.max.apply(null,global_availableDates.map(function(o){return moment(o.date,'YYYY-MM-DD').toDate();}))).format('DD/MM/YYYY'), 
				beforeShowDay: function(d) {
					var df = moment(d).format('YYYY-MM-DD');
					var selectable = global_availableDates.findIndex(x => x.date == df) > -1;
					return {enabled: selectable, classes: 'selectableDate'};
				}
			}).on('changeDate', function (ev) { if($(this).attr("id")=='dt_date') updateMap(); });
			$(this).closest('div[class~="input-group"]').find('select[class~="selOptInput"]').prop('disabled',false);
			$(this).closest('div[class~="input-group"]').find('select[class~="selOptInput"]').selectpicker('refresh');
			if($(this).hasClass('radioOpt')){
				// var radioName = $(this).attr('name');
				$('.selOptCheckEnabler.radioOpt').filter('[name="'+($(this).attr('name'))+'"]').filter('[id!="'+($(this).attr('id'))+'"]').attr('checked', false);
			}
		}
		else{
			$(this).closest('div[class~="input-group"]').find('input[class~="selOptInput"]').attr('disabled','disabled');
			$(this).closest('div[class~="input-group"]').find('span[class~="sliderCont"]').attr('disabled','disabled');
			$(this).closest('div[class~="date"]').datepicker("remove");
			$(this).closest('div[class~="input-group"]').find('select[class~="selOptInput"]').prop('disabled',true);
			$(this).closest('div[class~="input-group"]').find('select[class~="selOptInput"]').selectpicker('refresh');
		}
	});
	$('.date:has(.selOptCheckEnabler:checked)').datepicker({ 
		autoclose: true, 
		format: "dd/mm/yyyy", 
		startDate: moment(Math.min.apply(null,global_availableDates.map(function(o){return moment(o.date,'YYYY-MM-DD').toDate();}))).format('DD/MM/YYYY'), 
		endDate: moment(Math.max.apply(null,global_availableDates.map(function(o){return moment(o.date,'YYYY-MM-DD').toDate();}))).format('DD/MM/YYYY'), 
		beforeShowDay: function(d) {
			var df = moment(d).format('YYYY-MM-DD');
			var selectable = global_availableDates.findIndex(x => x.date == df) > -1;
			return {enabled: selectable, classes: 'selectableDate'};
		} 
	}).on('changeDate', function (ev) { if($(this).attr("id")=='dt_date') updateMap(); });
	
	$(".addSelOpt").click(function(){
		//alert("something");
		//$(this).closest('div[class~="input-group"]').find('select[class~="selOptSelect"]').attr('id')
		var name = $(this).closest('div[class~="input-group"]').find('span[class~="inputLabel"]').text();
		var id = $(this).closest('div[class~="input-group"]').find('select[class~="selOptSelect"]').attr('id');
		
		$('#modalAddSelOptTitle').html('Add a custom option to the '+name+' list');
		$('#modalAddSelOptId').html('#'+id);
		
		$('#modalAddSelOptConfirm').unbind('click'); 
		$('#modalAddSelOptConfirm').click(function(){
			if($('#modalAddSelOptValue').val()!=""){
				$($('#modalAddSelOptId').html()).append($('<option/>', { 
					value: $('#modalAddSelOptValue').val(),
					text : $('#modalAddSelOptValue').val()
				}));
				$($('#modalAddSelOptId').html()).selectpicker('refresh');
			}
			$('#modalAddSelOpt').modal('hide');
		});
		$('#modalAddSelOpt').modal('show');
	});
	
}




function ajaxCompleted(){
	/*actions--;
	if(actions==0){
		$.unblockUI(); 
		//loading = false;
		if(errors>0){
			$.blockUI({ message: 'Unable to load data from server!', css: {backgroundColor : 'rgba(255,0,0,0.5)'} }); 
			setTimeout($.unblockUI, 2000);
			//alert("Unable to load data from server!");
		}
		else{
			if(elementToShow){
				//$(elementToShow).show("fast");
				$(elementToShow).slideDown();
			}
			elementToShow = null;
		}
		errors = 0;
	}*/
}

function ajaxError(jqXHR, textStatus, errorThrown ){ 
	//errors++; 
}


//TODO: parametrizzare la funzione per creare l'elenco dei parametri in funzione della pagina da caricare
//function populateSelectionOptions(layers){
function populateSelectionOptions(){
	$.ajax({ 
		type: 'post',
		url: 'ajax/loadSelOptions.php',
		complete: ajaxCompleted,
		error: ajaxError,
		async: false,
		success: function(output) {
			var data = jQuery.parseJSON(output);
			
			global_availableDates = data.availableDates;
					
			var addCustomOption = true;
			
			// addSeparator('allOptionsContainer','sepLayers','Options',false);
			
			addOptionGroup('allOptionsContainer','collapseRasterLayers','Raster layers',true);
			addLayerOption('collapseRasterLayers','osm','OpenStreetMap',layers.osm.visible);
			addLayerRadioOption('collapseRasterLayers','raster','sat','Satellite image',layers.sat.visible);
			addSelectionOption('collapseRasterLayers',true,'select','ndvi','Index',data['index']);
			// addLayerRadioOption('collapseRasterLayers','raster','ndvi','NDVI',layers.ndvi.visible);
			addLayerRadioOption('collapseRasterLayers','raster','spectral','False colors image',layers.spectral.visible);
			
			addOptionGroup('allOptionsContainer','collapseVectorLayers','Vector layers',true);
			addLayerOption('collapseVectorLayers','agrea','AGREA (min zoom: '+layers.agrea.minZoom+')',layers.agrea.visible);
			addLayerOption('collapseVectorLayers','traps','Traps',layers.traps.visible);
			addLayerOption('collapseVectorLayers','meteo','Meteo stations',layers.meteo.visible);
			addLayerOption('collapseVectorLayers','custom','Custom polygon',layers.custom.visible);
			addLayerOption('collapseVectorLayers','reg','Regions',layers.reg.visible);
			addLayerOption('collapseVectorLayers','prov','Provinces',layers.prov.visible);
			addLayerOption('collapseVectorLayers','com','Municipalities (min zoom: '+layers.com.minZoom+')',layers.com.visible);
			
			addOptionGroup('allOptionsContainer','collapseFilters','Filters',true);
			addSelectionOption('collapseFilters',false,'date','date','Date');
			
			addOptionGroup('allOptions2Container','collapseFilters2','Filters',false);
			addSelectionOption('collapseFilters2',false,'date_from','date','From date');
			addSelectionOption('collapseFilters2',false,'date_to','date','To date');
			addSelectionOption('collapseFilters2',false,'selectmultiple','farming','Farming',data['farming']);
			addSelectionOption('collapseFilters2',false,'selectmultiple','provinces','Province',data['provinces']);
			// addSelectionOption('collapseClips','select','city','City',data['city']);
			
			$('.selectpicker').selectpicker('refresh');
		}
	});
}

function addSeparator(containerId,id,title,border){
	var c = border ? 'bt' : '';
	$('#'+containerId).append("<div class='separator "+c+"' id='"+id+"'>"+title+"</div>");
}

function addOptionGroup(containerId,groupId,title,toOpen){
	var open = ""; if(toOpen) open = "in";
	$('#'+containerId)
		.append("<div class='panel panel-default'>"
					+"<div class='panel-heading selOptHeading'>"
						+"<div class='panel-title selOptTitle'>"
							+"<a class='selOptTitleLink' data-toggle='collapse' href='#"+groupId+"'>"+title+"</a>" //<span id='badge_"+id+"' class='badge pull-right'>42</span>
						+"</div>"
					+"</div>"
					+"<div id='"+groupId+"' class='panel-collapse collapse "+open+"'>"
						+"<div class='panel-body'></div>"
					+"</div>"
				+"</div>");
}

function addLayerOption(groupId,name,label,checked){
	var id = "ly_" + name;
	if(checked) checked = 'checked'; else checked = '';
	$("#"+groupId+" .panel-body")
		.append("<div id='ig_"+id+"' class='input-group'> <span class='input-group-addon inputLabel layerOption'> "
				+"<input class='updateMap' type='checkbox' id='"+id+"_cb' "+checked+"> "+label+" </span></div>");
}
function addLayerRadioOption(groupId,radioName,name,label,checked){
	var id = "ly_" + name;
	if(checked) checked = 'checked'; else checked = '';
	$("#"+groupId+" .panel-body")
		.append("<div id='ig_"+id+"' class='input-group'> <span class='input-group-addon inputLabel layerOption'> "
				+"<input class='updateMap' type='checkbox' class='selOptCheckEnabler radioOpt' name='"+radioName+"' id='"+id+"_cb' "+checked+"> "+label+" </span></div>");
}

function addSelectionOption(groupId,checkboxRequired,type,name,label,parameters,addCustomOption){
	var content, prefix, classAttr = "";
	var checked = ""; var multiple = "";
	var disabled = checkboxRequired ? "disabled" : "";
	
	switch(type){
		case 'fulltext':
			prefix = "ss_";
			content = "<input type='text' name='ss_"+name+"' id='ss_"+name+"' class='selOptInput form-control' />";
			checked = "checked";
			break;
		case 'text':
			prefix = "tx_";
			content = "<input type='text' name='tx_"+name+"' id='tx_"+name+"' class='selOptInput form-control' "+disabled+" />";
			break;
		case 'number_from':
			prefix = "nf_";
			content = "<input title='"+parameters.tooltip+"' type='number' name='nf_"+name+"' id='nf_"+name+"' min="+parameters.min+" max="+parameters.max+" step="+parameters.step+" class='selOptInput form-control' "+disabled+" />";
			break;
		case 'number_to':
			prefix = "nt_";
			content = "<input title='"+parameters.tooltip+"' type='number' name='nt_"+name+"' id='nt_"+name+"' min="+parameters.min+" max="+parameters.max+" step="+parameters.step+" class='selOptInput form-control' "+disabled+" />";
			break;
		case 'selectmultiple':
			prefix = "sm_";
			var options = "";
			parameters.forEach(function(par){
				options += "<option value='" + par.name + "'>"+par.name+"</option>"; //par.label dentro a option
			});
			content = "<select multiple name='sm_"+name+"' id='sm_"+name+"' class='selectpicker form-control selOptInput selOptSelect' data-size='10' "+disabled+" data-live-search='true'>"+options+"</select>";
			if(addCustomOption) 
				content	+=" <span id='addOpt_sm_"+name+"' class='input-group-addon addSelOpt'><i class='glyphicon glyphicon-plus'></i></span> ";
			break;
		case 'select':
			prefix = "se_";
			var options = "";
			parameters.forEach(function(par){
				options += "<option value="+par.value+">"+par.name+"</option>";
			});
			content = "<select name='se_"+name+"' id='se_"+name+"' class='selectpicker form-control selOptInput selOptSelect' data-size='10' "+disabled+" data-live-search='true'>"+options+"</select>";
			if(addCustomOption) 
				content	+=" <span id='addOpt_se_"+name+"' class='input-group-addon addSelOpt'><i class='glyphicon glyphicon-plus'></i></span> ";
			break;
		case 'slider':
			prefix = "sl_";
			//var step = Math.round((parameters.max-parameters.min)/1000);
			var step = 1;
			content = "<span class='sliderCont form-control' "+disabled+" >"
							+" <input id='sl_"+name+"' type='text' class='selOptInput span2 slider' value='' name='sl_"+name+"' disabled "
							//	+" data-slider-min='"+parameters.min+"' data-slider-max='"+parameters.max+"' "
							//	+" data-slider-step='"+step+"' data-slider-value='["+parameters.min+","+parameters.max+"]' "
								+"/>"
						+"</span>";
			break;
		case 'date':
			prefix = "dt_";
			classAttr = "date";
			if(groupId=='collapseFilters') classAttr += ' updateMap';
			checked = true;
			content = "<input type='text' name='dt_"+name+"' id='dt_"+name+"' class='selOptInput form-control' value='17/08/2016' "+disabled+" />"
						+" <span class='input-group-addon'><i class='glyphicon glyphicon-calendar'></i></span> ";
			break;
		case 'date_from':
			prefix = "da_";
			classAttr = "date";
			checked = true;
			content = "<input type='text' name='da_"+name+"' id='da_"+name+"' class='selOptInput form-control' value='08/06/2016' "+disabled+" />"
						+" <span class='input-group-addon'><i class='glyphicon glyphicon-calendar'></i></span> ";
			break;
		case 'date_to':
			prefix = "dz_";
			//$("#ig_"+id).addClass('date');
			classAttr = "date";
			checked = true;
			content = "<input type='text' name='dz_"+name+"' id='dz_"+name+"' class='selOptInput form-control' value='28/06/2016' "+disabled+" />"
						+" <span class='input-group-addon'><i class='glyphicon glyphicon-calendar'></i></span> ";
			break;
			
	}
	
	var id = prefix+name;
	
	var cb = checkboxRequired 
				? "<input type='checkbox' id='"+id+"_cb' class='updateMap selOptCheckEnabler' "+checked+"> " 
				: "<input type='checkbox' id='"+id+"_cb' class='updateMap selOptCheckEnabler' checked disabled > ";
	
	$("#"+groupId+" .panel-body").append("<div id='ig_"+id+"' class='input-group "+classAttr+"'> <span class='input-group-addon inputLabel'> "
											+ cb + label+" </span> "
											+content+"</div>");
	
}

function resetOptions() {
	$('.selOptCheckEnabler').each(function() { 
		if(this.checked){
			$(this).prop("checked",false);
			$(this).trigger('change');
		}; 
	});
	$('.selOptInput').val("");
	
	if(typeof($('#to_topic'))!='undefined'){
		$('#to_topic option[data-id="-2"]').prop('selected', 'selected');
		$('#to_topic').selectpicker('refresh');
	}
	return false;
}