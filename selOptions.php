<div id="chartOptions" class="cont contLeft">
	<div class="chartTitle">
		<span id="chartOptionsTitleText" >Parameters</span>
	</div>
	<div id="chartOptionsCont" class="chart" style="padding: 5px;">
		<form id="formOptions" method="post" action="" onsubmit=""> <!-- submit solitamente gestito via jquery e chiamata ajax -->
			<div class="panel-group" id="selectionOptions" style="margin-bottom:0px;">
				
				<div id="allOptionsContainer">
					<!-- qui è dove vengono aggiunti i parametri attraverso populateSelectionOptions() in sparkcost.js -->
				</div>

			</div>
		</form>
	</div>
</div>