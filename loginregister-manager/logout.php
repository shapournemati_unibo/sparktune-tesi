<?php require_once(dirname(dirname(__FILE__)) . '/load-config.php');

//logout
$user->logout();

//logged in return to index page
header('Location: '. URL);
exit;
?>
