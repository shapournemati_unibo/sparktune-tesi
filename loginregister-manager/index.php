<?php
require_once(dirname(dirname(__FILE__)) . '/load-config.php');

//if logged in redirect to redirect-page
if( $user->is_logged_in() ) {
	if (isset($_GET["redirect"])) {
		header('Location: ' . urldecode($_GET["redirect"]));
	} else {
		header('Location: ' . URL);
	}
} else {
	header('Location: ' . $_SERVER['HTTP_HOST'].AUTHPATH.'/signup.php');
}
