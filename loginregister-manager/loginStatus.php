<?php
require_once(dirname(dirname(__FILE__)) . '/load-config.php');

class LoginStatus {

  public static function getUserId() {
    if (LoginStatus::checkLogin()) {
      return $GLOBALS['user']->getUserId();
    }
  }

  public static function isLogged() {
    return $GLOBALS['user']->is_logged_in();
  }

  public static function checkLogin() {
    if(!LoginStatus::isLogged()){
      $current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
      if (strpos($current_url, AUTHPATH)) {
        $current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].RELPATH);
      }
      header('Location: '. AUTHPATH . 'login.php?redirect=' . $current_url);
    } else {
      return true;
    }
  }
}

?>
