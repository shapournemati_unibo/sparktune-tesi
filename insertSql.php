<?php
require_once(dirname(__FILE__) . '/load-config.php');
include_once(dirname(__FILE__) . '/src/view/GrammarDerivationView.php');
include_once(dirname(__FILE__) . '/src/GPSJGrammarParser/PhisicalPlan.php');
include_once(dirname(__FILE__) . '/src/costModel/CostModel.php');
include_once(dirname(__FILE__) . '/src/costModel/Database.php');
include_once(dirname(__FILE__) . '/src/costModel/Table.php');
include_once(dirname(__FILE__) . '/user/DatabaseQuery.php');

error_reporting(E_ALL);
ini_set("display_errors", 1);

//DatabaseQuery::insertSQL(32,"SELECT l_orderkey, COUNT(*)\nFROM tpch_100gb.lineitem\nGROUP BY l_orderkey");
DatabaseQuery::insertSQL(51,"SELECT
  l_orderkey,
  sum(l_extendedprice*(1-l_discount)) as revenue,
  o_orderdate,
  o_shippriority
FROM
  tpch_100gb.customer,
  tpch_100gb.orders,
  tpch_100gb.lineitem
WHERE
  c_mktsegment=BUILDING
  and c_custkey = o_custkey
  and l_orderkey = o_orderkey
  and o_orderdate<1995-03-15
  and l_shipdate>1995-03-15
GROUP BY
  l_orderkey,
  o_orderdate,
  o_shippriority
");
DatabaseQuery::insertSQL(52,"SELECT
  sum(l_extendedprice*l_discount) as revenue
FROM
  tpch_100gb.lineitem
WHERE
  l_shipdate>=1994-01-01
  and l_shipdate<1995-01-01
  and l_discount between 0.05 and 0.07
  and l_quantity<24
");
DatabaseQuery::insertSQL(53,"SELECT
  c_custkey,
  c_name,
  sum(l_extendedprice * (1 - l_discount)) as revenue,
  c_acctbal,
  n_name,
  c_address,
  c_phone,
  c_comment
FROM
  tpch_100gb.customer,
  tpch_100gb.orders,
  tpch_100gb.lineitem,
  tpch_100gb.nation
WHERE
  c_custkey = o_custkey
  and l_orderkey = o_orderkey
  and o_orderdate>=1993-10-01
  and o_orderdate<1994-01-01
  and l_returnflag=R
  and c_nationkey = n_nationkey
GROUP BY
  c_custkey,
  c_name,
  c_acctbal,
  c_phone,
  n_name,
  c_address,
  c_comment");
?>
