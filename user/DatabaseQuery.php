<?php
require_once(dirname(dirname(__FILE__)) . '/load-config.php');
include_once ABSPATH.'connection.php';

class DatabaseValues {

    // Tables
    const TABLE_USER = 'User';
    const TABLE_CLUSTER = 'Cluster';
    const TABLE_THROUGHPUT = 'Throughput';
    const TABLE_QUERY = 'Query';
    const TABLE_DATABASE = 'Database';
    const TABLE_TABLE = 'Table';
    const TABLE_ATTRIBUTE = 'Attribute';
    const TABLE_WORKLOAD = 'Workload';
    const TABLE_INTERROGATION = 'Interrogation';

    // Table User
    const U_UserId = 'usr_id';
    const U_USERNAME = 'usr_username';
    const U_PASSWORD = 'usr_password';

    // Table Cluster
    const CL_ClusterId = 'cl_id';
    const CL_UserId = 'cl_userId';
    const CL_Name = 'cl_name';
    const CL_N_R = 'cl_#R';
    const CL_N_RN = 'cl_#RN';
    const CL_N_C = 'cl_#C';
    const CL_Rf = 'cl_rf';
    const CL_N_SB = 'cl_#SB';
    const CL_SCmp = 'cl_sCmp';
    const CL_FCmp = 'cl_fCmp';
    const CL_HSel = 'cl_hSel';
    const CL_IntraRSpeed = 'cl_intraRSpeed';
    const CL_ExtraRSpeed = 'cl_extraRSpeed';

    // Table throughput
    const TR_Cluster = 'th_clusterId';
    const TR_Type = 'th_type';
    const TR_Process = 'th_process';
    const TR_Value = 'th_value';

    const TR_Type_DR = 'dr';
    const TR_Type_DW = 'dw';

    // Table Query
    const Q_DAG = 'DAG';
    const Q_N_EC = '#EC';
    const Q_N_E = '#E';

    // Table Database
    const DB_ID = 'db_id';
    const DB_UserID = 'db_userId';
    const DB_Name = 'db_name';

    // Table Table
    const TBL_ID = 'tbl_id';
    const TBL_DatabaseId = 'tbl_databaseId';
    const TBL_Name = 'tbl_name';
    const TBL_Cardinality = 'tbl_cardinality';
    const TBL_Size = 'tbl_size';
    const TBL_N_Partitions = 'tbl_#partitions';
    const TBL_CompressionEnabled = 'tbl_compressionEnabled';

    // Table Attribute
    const ATT_ID = 'att_id';
    const ATT_TableId = 'att_tableId';
    const ATT_Name = 'att_name';
    const ATT_Cardinality = 'att_cardinality';
    const ATT_AvgLength = 'att_avgLength';
    const ATT_Highest = 'att_highestValue';
    const ATT_Lowest = 'att_lowestValue';

    //Table Workload
    const WL_ID = 'wl_id';
    const WL_USERID = 'wl_userid';
    const WL_NAME = 'wl_name';
    const WL_VERSION = 'wl_version';

    //Table Interrogation
    const IN_ID = 'in_id';
    const IN_WORKLOAD = 'in_workload';
    const IN_TEXT = 'in_text';
    const IN_SQL = 'in_sql';

    public static function getHumanName($name) {
      switch ($name) {
        case DatabaseValues::DB_ID:
        case DatabaseValues::TBL_DatabaseId:
          $humanName = 'Database ID';
          break;
        case DatabaseValues::TBL_ID:
        case DatabaseValues::ATT_TableId:
          $humanName = 'Table ID';
          break;
        case DatabaseValues::ATT_ID:
          $humanName = 'Attribute ID';
          break;
        case DatabaseValues::CL_ClusterId:
        case DatabaseValues::TR_Cluster:
          $humanName = 'Cluster ID';
          break;
        case DatabaseValues::DB_UserID:
        case DatabaseValues::CL_UserId:
          $humanName = 'User ID';
          break;
        // Table Cluster
        case DatabaseValues::CL_Name:
          $humanName = 'Name';
          break;
        case DatabaseValues::CL_N_R:
          $humanName = '#R';
          break;
        case DatabaseValues::CL_N_RN:
          $humanName = '#RN';
          break;
        case DatabaseValues::CL_N_C:
          $humanName = '#C';
          break;
        case DatabaseValues::CL_Rf:
          $humanName = 'rf';
          break;
        case DatabaseValues::CL_N_SB:
          $humanName = '#SB';
          break;
        case DatabaseValues::CL_SCmp:
          $humanName = 'sCmp';
          break;
        case DatabaseValues::CL_FCmp:
          $humanName = 'fCmp';
          break;
        case DatabaseValues::CL_HSel:
          $humanName = 'hSel';
          break;
        case DatabaseValues::CL_IntraRSpeed:
          $humanName = 'IntraRSpeed';
          break;
        case DatabaseValues::CL_ExtraRSpeed:
          $humanName = 'ExtraRSpeed';
          break;

        // Table throughput
        case DatabaseValues::TR_Type:
          $humanName = 'Type';
          break;
        case DatabaseValues::TR_Process:
          $humanName = 'Process';
          break;
        case DatabaseValues::TR_Value:
          $humanName = 'Value';
          break;

        // Table Database
        case DatabaseValues::DB_Name:
          $humanName = 'Name';
          break;

        // Table Table
        case DatabaseValues::TBL_Name:
          $humanName = 'Name';
          break;
        case DatabaseValues::TBL_Cardinality:
          $humanName = 'Cardinality';
          break;
        case DatabaseValues::TBL_Size:
          $humanName = 'Size';
          break;
        case DatabaseValues::TBL_N_Partitions:
          $humanName = '#partitions';
          break;
        case DatabaseValues::TBL_CompressionEnabled:
          $humanName = 'Compression enabled';
          break;

        // Table Attribute
        case DatabaseValues::ATT_Name:
          $humanName = 'Name';
          break;
        case DatabaseValues::ATT_Cardinality:
          $humanName = 'Cardinality';
          break;
        case DatabaseValues::ATT_AvgLength:
          $humanName = 'Avg length';
          break;
        case DatabaseValues::ATT_Highest:
          $humanName = 'Highest value';
          break;
        case DatabaseValues::ATT_Lowest:
          $humanName = 'Lowest value';
          break;

        default:
          $humanName=$name;
          break;
      }
      return $humanName;
    }
}

class DatabaseQuery {

  public static function getCluster($clusterId) {
    $res = NULL;
    $query = "SELECT *
              FROM `" . DatabaseValues::TABLE_CLUSTER . "`
              WHERE `" . DatabaseValues::CL_ClusterId . "`=" . $clusterId;
    $conn = openConnection();
    $result = mysqli_query($conn, $query);
    if ($result->num_rows > 0) {
      $res = $result->fetch_assoc();
    }
    closeConnection($conn);
    return $res;
  }

  public static function insertThroughputs($clusterId, $type, $throughputs) {
    $query = "INSERT INTO `" . DatabaseValues::TABLE_THROUGHPUT . "`
            (`" . DatabaseValues::TR_Cluster . "`,
              `" . DatabaseValues::TR_Type . "`,
              `" . DatabaseValues::TR_Process . "`,
              `" . DatabaseValues::TR_Value . "`)";
    $first = true;
    foreach ($throughputs as $process => $value) {
      if ($first) {
        $query .= "VALUES";
        $first = false;
      } else {
        $query .= ", ";
      }
      $query .= "('" . $clusterId
               . "', '" . $type
               . "', '" . $process
               . "', '" . $value . "')";
    }
    $query .= " ON DUPLICATE KEY UPDATE `" . DatabaseValues::TR_Value . "`=VALUES(`" . DatabaseValues::TR_Value . "`);";
echo $query;
    $conn = openConnection();
    $res = mysqli_query($conn, $query) == true;
    closeConnection($conn);
    return $res;
  }

  public static function updateThroughputs($clusterId, $type, $throughputs) {
    return DatabaseQuery::insertThroughputs($clusterId, $type, $throughputs);
  }

  public static function getThroughputs($clusterID, $type) { // Type can be 'dr' or 'dw'
    $res = array();
    $query = "SELECT *
              FROM `" . DatabaseValues::TABLE_THROUGHPUT . "`
              WHERE `" . DatabaseValues::TR_Cluster . "`=" . $clusterID . " AND `" . DatabaseValues::TR_Type . "`='" . $type . "'";
    $conn = openConnection();
    $result = mysqli_query($conn, $query);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $res[$row[DatabaseValues::TR_Process]] = $row[DatabaseValues::TR_Value];
      }
    }
    closeConnection($conn);
    return $res;
  }

  public static function getWorkloads($userId){
    $res = array();
    $query = "SELECT *
              FROM `" . DatabaseValues::TABLE_WORKLOAD . "`
              WHERE `" . DatabaseValues::WL_USERID . "`=" . $userId;
    $conn = openConnection();
    $result = mysqli_query($conn, $query);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $res[$row[DatabaseValues::WL_ID]] = $row;
      }
    }
    closeConnection($conn);
    return $res;
  }

  public static function getWorkloadVersion($Id){
    $res;
    $query = "SELECT *
              FROM `" . DatabaseValues::TABLE_WORKLOAD . "`
              WHERE `" . DatabaseValues::WL_ID . "`=" . $Id;
    $conn = openConnection();
    $result = mysqli_query($conn, $query);
    if ($result->num_rows > 0) {
      // output data of each row
      $res = $result->fetch_assoc();
    }
    closeConnection($conn);
    return $res[DatabaseValues::WL_VERSION];
  }

  public static function insertWorkload($userId,$name,$version){
    $query = "INSERT INTO `" . DatabaseValues::TABLE_WORKLOAD . "`
            (`" . DatabaseValues::WL_NAME . "`,
            `" . DatabaseValues::WL_VERSION . "`,
              `" . DatabaseValues::WL_USERID . "`)
            VALUES
            ('" . $name
            . "', '" . $version
             . "', '" . $userId . "')";

    $conn = openConnection();
    if(mysqli_query($conn, $query)) {
      $res = mysqli_insert_id($conn);
    }
    else {
      $res = false;
    }
    closeConnection($conn);
    return $res;

  }

  public static function deleteWorkload($workloadId){
    $query = "DELETE FROM `" . DatabaseValues::TABLE_WORKLOAD . "`
                WHERE `" . DatabaseValues::WL_ID . "`='$workloadId'";

    $conn = openConnection();
    $res = mysqli_query($conn, $query) == true;
    closeConnection($conn);
    return $res;

  }

  public static function getQueries($workloadID){
    $res = array();
    $query = "SELECT *
              FROM `" . DatabaseValues::TABLE_INTERROGATION . "`
              WHERE `" . DatabaseValues::IN_WORKLOAD . "`=" . $workloadID . "
              ORDER BY " . DatabaseValues::IN_ID;
    $conn = openConnection();
    $result = mysqli_query($conn, $query);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $res[$row[DatabaseValues::IN_ID]] = $row[DatabaseValues::IN_TEXT];
      }
    }
    closeConnection($conn);
    return $res;
  }

  public static function getQuerySQL($queryID){
    $res = array();
    $query = "SELECT *
              FROM `" . DatabaseValues::TABLE_INTERROGATION . "`
              WHERE `" . DatabaseValues::IN_ID . "`='$queryID'";
    $conn = openConnection();
    $result = mysqli_query($conn, $query);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $res = $row[DatabaseValues::IN_SQL];
      }
    }
    closeConnection($conn);
    return $res;
  }

  public static function insertQuery($workloadID,$text,$sql){
    $query = "INSERT INTO `" . DatabaseValues::TABLE_INTERROGATION . "`
            (`" . DatabaseValues::IN_WORKLOAD . "`,
              `" . DatabaseValues::IN_TEXT . "`,
              `" . DatabaseValues::IN_SQL . "`)
            VALUES
            ('" . $workloadID
            . "', '" . $text
             . "', '" . $sql . "')";

    $conn = openConnection();
    if(mysqli_query($conn, $query)) {
      $res = mysqli_insert_id($conn);
    }
    else {
      $res = false;
    }
    closeConnection($conn);
    return $res;
  }

  public static function insertSQL($id,$sql){
    $query = "UPDATE `" . DatabaseValues::TABLE_INTERROGATION . "`
              SET
              `" . DatabaseValues::IN_SQL . "`='" . $sql . "'
              WHERE `" . DatabaseValues::IN_ID . "`=" . $id . ";";
  $conn = openConnection();
  mysqli_query($conn, $query);
  closeConnection($conn);
  }

  public static function getClusters($userId) {
    $res = array();
    $query = "SELECT *
              FROM `" . DatabaseValues::TABLE_CLUSTER . "`
              WHERE `" . DatabaseValues::CL_UserId . "`=" . $userId;
    $conn = openConnection();
    $result = mysqli_query($conn, $query);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $res[$row[DatabaseValues::CL_ClusterId]] = $row;
      }
    }
    closeConnection($conn);
    return $res;
  }

  public static function deleteCluster($clusterId) {
    $query = "DELETE FROM `" . DatabaseValues::TABLE_CLUSTER . "`
                WHERE `" . DatabaseValues::CL_ClusterId . "`='$clusterId'";

    $conn = openConnection();
    $res = mysqli_query($conn, $query) == true;
    closeConnection($conn);
    return $res;
  }

  public static function insertCluster($userId, $name, $nR, $nRN, $nC, $rf, $nSB, $sCmp, $fCmp, $hSel, $intraRSpeed, $extraRSpeed) {
    $query = "INSERT INTO `" . DatabaseValues::TABLE_CLUSTER . "`
            (`" . DatabaseValues::CL_Name . "`,
              `" . DatabaseValues::CL_UserId . "`,
              `" . DatabaseValues::CL_N_R . "`,
              `" . DatabaseValues::CL_N_RN . "`,
              `" . DatabaseValues::CL_N_C . "`,
              `" . DatabaseValues::CL_Rf . "`,
              `" . DatabaseValues::CL_N_SB . "`,
              `" . DatabaseValues::CL_SCmp . "`,
              `" . DatabaseValues::CL_FCmp . "`,
              `" . DatabaseValues::CL_HSel . "`,
              `" . DatabaseValues::CL_IntraRSpeed . "`,
              `" . DatabaseValues::CL_ExtraRSpeed . "`)
            VALUES
            ('" . $name
             . "', '" . $userId
             . "', '" . $nR
             . "', '" . $nRN
             . "', '" . $nC
             . "', '" . $rf
             . "', '" . $nSB
             . "', '" . $sCmp
             . "', '" . $fCmp
             . "', '" . $hSel
             . "', '" . $intraRSpeed
             . "', '" . $extraRSpeed . "')";

    $conn = openConnection();
    if(mysqli_query($conn, $query)) {
      $res = mysqli_insert_id($conn);
    }
    else {
      $res = false;
    }
    closeConnection($conn);
    return $res;
  }

  public static function updateCluster($clusterID, $name, $nR, $nRN, $nC, $rf, $nSB, $sCmp, $fCmp, $hSel, $intraRSpeed, $extraRSpeed) {
    $query = "UPDATE `" . DatabaseValues::TABLE_CLUSTER . "`
              SET
              `" . DatabaseValues::CL_Name . "`='" . $name . "',
              `" . DatabaseValues::CL_N_R . "`='" . $nR . "',
              `" . DatabaseValues::CL_N_RN . "`='" . $nRN . "',
              `" . DatabaseValues::CL_N_C . "`='" . $nC . "',
              `" . DatabaseValues::CL_Rf . "`='" . $rf . "',
              `" . DatabaseValues::CL_N_SB . "`='" . $nSB . "',
              `" . DatabaseValues::CL_SCmp . "`='" . $sCmp . "',
              `" . DatabaseValues::CL_FCmp . "`='" . $fCmp . "',
              `" . DatabaseValues::CL_HSel . "`='" . $hSel . "',
              `" . DatabaseValues::CL_IntraRSpeed . "`='" . $intraRSpeed . "',
              `" . DatabaseValues::CL_ExtraRSpeed . "`='" . $extraRSpeed . "'
              WHERE `" . DatabaseValues::CL_ClusterId . "`=" . $clusterID . ";";

    $conn = openConnection();
    $res = mysqli_query($conn, $query) == true;
    closeConnection($conn);
    return $res;
  }

  public static function getDatabase($databaseId) {
    $query = "SELECT *
              FROM `" . DatabaseValues::TABLE_DATABASE . "`
              WHERE `" . DatabaseValues::DB_ID . "`=". $databaseId;
    $conn = openConnection();
    $result = mysqli_query($conn, $query);
    if ($result->num_rows > 0) {
      $res = $result->fetch_assoc();
    }
    closeConnection($conn);
    return $res;
  }

  public static function getDatabases($userId) {
    $res = array();
    $query = "SELECT *
              FROM `" . DatabaseValues::TABLE_DATABASE . "`
              WHERE `" . DatabaseValues::DB_UserID . "`=". $userId;
    $conn = openConnection();
    $result = mysqli_query($conn, $query);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $res[$row[DatabaseValues::DB_ID]] = $row;
      }
    }
    closeConnection($conn);
    return $res;
  }

  public static function getTables($databaseId) {
    $res = array();
    $query = "SELECT *
              FROM `" . DatabaseValues::TABLE_TABLE . "`
              WHERE `" . DatabaseValues::TBL_DatabaseId . "`=". $databaseId;
    $conn = openConnection();
    $result = mysqli_query($conn, $query);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $res[$row[DatabaseValues::TBL_ID]] = $row;
      }
    }
    closeConnection($conn);
    return $res;
  }

  public static function getAttributes($tableId) {
    $res = array();
    $query = "SELECT *
              FROM `" . DatabaseValues::TABLE_ATTRIBUTE . "`
              WHERE `" . DatabaseValues::ATT_TableId . "`=". $tableId;
    $conn = openConnection();
    $result = mysqli_query($conn, $query);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $res[$row[DatabaseValues::ATT_ID]] = $row;
      }
    }
    closeConnection($conn);
    return $res;
  }

  public static function deleteDatabase($databaseId) {
    $query = " DELETE FROM `" . DatabaseValues::TABLE_DATABASE . "`
                WHERE `" . DatabaseValues::DB_ID . "`='$databaseId'";

    $conn = openConnection();
    $res = mysqli_query($conn, $query) == true;
    closeConnection($conn);
    return $res;
  }

  public static function insertDatabase($userId, $dbName) {
    if (empty($dbName)) {
      return false;
    }
    $query = "INSERT INTO `" . DatabaseValues::TABLE_DATABASE . "`
            (`" . DatabaseValues::DB_Name . "`,
            `" . DatabaseValues::DB_UserID . "`)
            VALUES
            ('" . $dbName
             . "', '" . $userId . "')";

    $conn = openConnection();
    if(mysqli_query($conn, $query)) {
      $res = mysqli_insert_id($conn);
    }
    else {
      $res = false;
    }
    closeConnection($conn);
    return $res;
  }

  public static function insertTable(
          $dbID,
          $tableName,
          $tableCardinality,
          $tableSize,
          $tableNPartitions) {
    $query = "INSERT INTO `" . DatabaseValues::TABLE_TABLE . "`
            (`" . DatabaseValues::TBL_Name . "`,
            `" . DatabaseValues::TBL_Cardinality . "`,
            `" . DatabaseValues::TBL_Size . "`,
            `" . DatabaseValues::TBL_N_Partitions . "`,
            `" . DatabaseValues::TBL_DatabaseId . "`)
            VALUES
            ('" . $tableName
             . "', '" . $tableCardinality
             . "', '" . $tableSize
             . "', '" . $tableNPartitions
             . "', '" . $dbID . "')";

    $conn = openConnection();
    if(mysqli_query($conn, $query)) {
      $res = mysqli_insert_id($conn);
    }
    else {
      $res = false;
    }
    closeConnection($conn);
    return $res;
  }

  public static function insertAttribute(
          $tableID,
          $attName,
          $attCardinality,
          $attMediumLength,
          $attLow,
          $attHigh) {
    $query = "INSERT INTO `" . DatabaseValues::TABLE_ATTRIBUTE . "`
            (`" . DatabaseValues::ATT_Name . "`,
            `" . DatabaseValues::ATT_Cardinality . "`,
            `" . DatabaseValues::ATT_AvgLength . "`,
            `" . DatabaseValues::ATT_Highest . "`,
            `" . DatabaseValues::ATT_Lowest . "`,
            `" . DatabaseValues::ATT_TableId . "`)
            VALUES
            ('" . $attName
             . "', '" . $attCardinality
             . "', '" . $attMediumLength
             . "', '" . $attLow
             . "', '" . $attHigh
             . "', '" . $tableID . "')";

    $conn = openConnection();
    if(mysqli_query($conn, $query)) {
      $res = mysqli_insert_id($conn);
    }
    else {
      $res = false;
    }
    closeConnection($conn);
    return $res;
  }
}

?>
