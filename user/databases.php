<?php
require_once(dirname(dirname(__FILE__)) . '/load-config.php');
include dirname(__FILE__).'/DatabaseQuery.php';
require_once ABSAUTHPATH."loginStatus.php";

if (isset($_GET["message"])) {
  $message = $_GET["message"];
}

if(isset($_POST['insert'])) {
  $dbID = DatabaseQuery::insertDatabase(LoginStatus::getUserId(), $_POST[DatabaseValues::DB_Name]);
  if ($dbID) {
    // Insert each table
    for ($i=0; $i < sizeof($_POST[DatabaseValues::TBL_Name]); $i++) {
      // Insert the table
      $tblID = DatabaseQuery::insertTable(
        $dbID,
        $_POST[DatabaseValues::TBL_Name][$i],
        $_POST[DatabaseValues::TBL_Cardinality][$i],
        $_POST[DatabaseValues::TBL_Size][$i]*1024*1024,
        $_POST[DatabaseValues::TBL_N_Partitions][$i],
        $_POST[DatabaseValues::TBL_CompressionEnabled] == "compressed" ? 1 : 0
      );
      if ($tblID) {
        // Insert each attribute of that table
        for ($j=0; $j < sizeof($_POST[DatabaseValues::ATT_Name . "-$i"]); $j++) {
          // Insert the attribute
          $attID = DatabaseQuery::insertAttribute(
            $tblID,
            $_POST[DatabaseValues::ATT_Name . "-$i"][$j],
            $_POST[DatabaseValues::ATT_Cardinality . "-$i"][$j],
            $_POST[DatabaseValues::ATT_AvgLength . "-$i"][$j],
            $_POST[DatabaseValues::ATT_Highest . "-$i"][$j],
            $_POST[DatabaseValues::ATT_Lowest . "-$i"][$j]
          );
          if (!$attID) {
            header('Location: database.php?action=error&message=' . urlencode('ERROR uploading the attribute' . $_POST[DatabaseValues::ATT_Name . "-$i"][$j]));
          }
        }
      } else {
        header('Location: database.php?action=error&message=' . urlencode("ERROR uploading the table " . $_POST[DatabaseValues::TBL_Name][$i]));
      }
    }
    header('Location: database.php?action=inserted');
  } else {
    header('Location: database.php?action=error&message=' . urlencode("ERROR uploading the database " . $_POST[DatabaseValues::DB_Name]));
  }
} elseif (isset($_GET['delete'])) {
  DatabaseQuery::deleteDatabase($_GET['delete']);
  header('Location: database.php?action=deleted');
} elseif (isset($_GET['edit'])) {
  // TODO
}
?>

<!DOCTYPE html>
<html>
  <head>
    <?php include ABSTEMPLATEPATH . 'head.php';
    echo '<link rel="stylesheet" href="'.TEMPLATEPATH.'/styles/style.css"/>';
     ?>
    <title>Databases</title>
    <script type="text/javascript">

      var MAX_TABLES = 10;
      var MAX_ATTRIBUTES = 20;

      function createTableRow(rowID) {
        var row = document.createElement('tr');
        var col = document.createElement('td');
        var col1 = document.createElement('td');
        var col2 = document.createElement('td');
        var col3 = document.createElement('td');
        var col4 = document.createElement('td');
        var col5 = document.createElement('td');
        row.appendChild(col);
        row.appendChild(col1);
        row.appendChild(col2);
        row.appendChild(col3);
        row.appendChild(col4);
        row.appendChild(col5);
        col.innerHTML = `<input type="checkbox" name="chk[]" checked="checked" />`;
        col1.innerHTML =
          `<label class="field_label">
            Name:
            <input type="text" name="<?php echo DatabaseValues::TBL_Name; ?>[]" autocomplete="off"
            title="Aphabetic characters are allowed."
            placeholder="Name">
          </label>`;
        col2.innerHTML =
          `<label class="field_label">
            Cardinality:
            <input type="number" name="<?php echo DatabaseValues::TBL_Cardinality; ?>[]" min=0 autocomplete="off"
            title="Only integers are allowed."
            placeholder="Cardinality">
          </label>`;
        col3.innerHTML =
          `<label class="field_label">
            Size(MB):
            <input type="number" name="<?php echo DatabaseValues::TBL_Size; ?>[]" min=0 autocomplete="off"
            title="Only integers are allowed."
            placeholder="Size(MB)">
          </label>`;
        col4.innerHTML =
          `<label class="field_label">
            Partitions number:
            <input type="number" name="<?php echo DatabaseValues::TBL_N_Partitions; ?>[]" min=0 autocomplete="off"
            title="Only integers are allowed."
            placeholder="Partitions number">
          </label>`;
        col5.innerHTML =
          `<label class="field_label">
            Table is compressed:
            <input type="checkbox" name="<?php echo DatabaseValues::TBL_CompressionEnabled; ?>[]" value="compressed"
            placeholder="Table is compressed">
          </label>`;
        row.id = rowID;
        return row;
      }

      function createAttributeRow(rowID) {
        var row = document.createElement('tr');
        var col = document.createElement('td');
        var col1 = document.createElement('td');
        var col2 = document.createElement('td');
        var col3 = document.createElement('td');
        var col4 = document.createElement('td');
        var col5 = document.createElement('td');
        row.appendChild(col);
        row.appendChild(col1);
        row.appendChild(col2);
        row.appendChild(col3);
        row.appendChild(col4);
        row.appendChild(col5);
        col.innerHTML = `<input type="checkbox" name="chk[]" checked="checked" />`;
        col1.innerHTML =
          `<label class="field_label">
            Name:
            <input type="text" name="<?php echo DatabaseValues::ATT_Name; ?>" autocomplete="off"
            title="Aphabetic characters are allowed."
            placeholder="Name">
          </label>`;
        col2.innerHTML =
          `<label class="field_label">
            Cardinality:
            <input type="number" name="<?php echo DatabaseValues::ATT_Cardinality; ?>" min=0 autocomplete="off"
            title="Only integers are allowed."
            placeholder="Cardinality">
          </label>`;
        col3.innerHTML =
          `<label class="field_label">
            Average length:
            <input type="number" name="<?php echo DatabaseValues::ATT_AvgLength; ?>" min=0 step="0.01" autocomplete="off"
            title="Only integers are allowed."
            placeholder="Average length">
          </label>`;
        col4.innerHTML =
          `<label class="field_label">
            Highest value:
            <input type="number" name="<?php echo DatabaseValues::ATT_Highest; ?>" min=0 step="0.01" autocomplete="off"
            title="Only integers are allowed."
            placeholder="Highest value">
          </label>`;
        col5.innerHTML =
          `<label class="field_label">
            Lowest value:
            <input type="number" name="<?php echo DatabaseValues::ATT_Lowest; ?>" min=0 step="0.01" autocomplete="off"
            title="Only integers are allowed."
            placeholder="Lowest value">
          </label>`;
        row.id = rowID;
        return row;
      }

      function createTablesTable(tablesID, attributesID) {
        var fieldset = document.createElement('fieldset');
        var fieldsetLegend = document.createElement('legend');
        var buttonsP = document.createElement('p');
        var button1 = document.createElement('input');
        var button2 = document.createElement('input');
        var message = document.createElement('p');
        var table = document.createElement('table');
        fieldset.appendChild(fieldsetLegend);
        fieldset.appendChild(buttonsP);
        buttonsP.appendChild(button1);
        buttonsP.appendChild(button2);
        buttonsP.appendChild(message);
        fieldset.appendChild(table);
        fieldsetLegend.innerHTML = `Tables`;
        buttonsP.className = "actions";
        button1.type = `button`;
        button1.value = `Add Table`;
        button1.onclick = function() {addTable(tablesID, attributesID);};
        button2.type = `button`;
        button2.value = `Remove Tables`;
        button2.onclick = function() {deleteTables(tablesID)};
        message.innerHTML = `(All actions apply only to entries with check marked check boxes only.)`;
        table.id = tablesID;
        table.class = `form`;
        return fieldset;
      }

      function createAttributesTable(attributesID) {
        var fieldset = document.createElement('fieldset');
        var fieldsetLegend = document.createElement('legend');
        var buttonsP = document.createElement('p');
        var button1 = document.createElement('input');
        var button2 = document.createElement('input');
        var message = document.createElement('p');
        var table = document.createElement('table');
        fieldset.appendChild(fieldsetLegend);
        fieldset.appendChild(buttonsP);
        buttonsP.appendChild(button1);
        buttonsP.appendChild(button2);
        buttonsP.appendChild(message);
        fieldset.appendChild(table);
        fieldsetLegend.innerHTML = `Attributes`;
        buttonsP.className = "actions";
        button1.type = `button`;
        button1.value = `Add attribute`;
        button1.onclick = function() {addAttribute(attributesID);};
        button2.type = `button`;
        button2.value = `Remove attributes`;
        button2.onclick = function() {deleteAttributes(attributesID)};
        message.innerHTML = `(All actions apply only to entries with check marked check boxes only.)`;
        table.id = attributesID;
        table.class = `form`;
        return fieldset;
      }

      function addRow(tableID, newRow, rowIdx, rowLimit) {
        var table = document.getElementById(tableID);
				if (rowIdx < rowLimit) {
          newRow.id = newRow.id + rowIdx;
          table.appendChild(newRow);
          return newRow;
        }
			}

      function addTitleColumn(columnID, columnTxt, verticalOrientation, rowSpan) {
        var descriptionTxt = document.getElementById(columnID);
        if (descriptionTxt == null) {
          var description = document.createElement('td');
          descriptionTxt = document.createElement('h3');
          description.appendChild(descriptionTxt);
          description.rowSpan = rowSpan;
          descriptionTxt.innerHTML = columnTxt;
          descriptionTxt.id = columnID;
          return description;
        }
        if (verticalOrientation) {
          descriptionTxt.className = "verticalTxt";
        } else {
          descriptionTxt.className = "";
        }
      }

      function addTitleColumnAttribute(columnID, verticalOrientation) {
        return addTitleColumn(columnID, "Attributes", verticalOrientation, 0);
      }

      function addTable(tablesID, attributesID) {
        var table = document.getElementById(tablesID);
        var index = table.rows.length / 2;
        var tableRow = addRow(tablesID, createTableRow("table-"), index, MAX_TABLES);
        if (tableRow) {
          var row = document.createElement('tr');
          var col = document.createElement('td');
          var attributesTbl = createAttributesTable(attributesID + index);
          table.appendChild(row);
          row.appendChild(col);
          col.appendChild(attributesTbl);
          row.id = index;
          col.colSpan = table.rows[0].cells.length;

          addAttribute(attributesID + index);
        } else {
          alert("Maximum tables per database is " + MAX_TABLES + ".");
        }
      }

      function addAttribute(attributesID) {
        var table = document.getElementById(attributesID);
        var attIndex = table.rows.length;
        var attributeRow = createAttributeRow("attribute-");
        if (addRow(attributesID, attributeRow, attIndex, MAX_ATTRIBUTES)) {
          var tblIndex = $(attributeRow).closest("tr").parent().closest("tr").attr('id');
          $(attributeRow).find('input').each(function(i) {
            $(this).attr('name', $(this).attr('name') + "-" + tblIndex + "[]");
          });
          if (attIndex == 0) {
            var col = addTitleColumnAttribute("attributesTitle-" + tblIndex, false);
            attributeRow.appendChild(col);
          } else {
            addTitleColumnAttribute("attributesTitle-" + tblIndex, true);
          }
        } else {
          alert("Maximum attributes per table is " + MAX_ATTRIBUTES + ".");
        }
      }

      function deleteRows(tableID, rowBlockNum = 1) {
				var table = document.getElementById(tableID);
				var rowCount = table.rows.length;
				for(var i=0; i<rowCount/rowBlockNum; i+=rowBlockNum) {
					var row = table.rows[i];
					var chkbox = row.cells[0].childNodes[0];
					if(null != chkbox && true == chkbox.checked) {
						if (rowCount <= rowBlockNum) { 						// limit the user from removing all the fields
							alert("Cannot remove all the rows.");
							break;
						}
            if (i == 0) {
              var lastElem = row.cells[row.cells.length-1];
              var nextRow = table.rows[i+1];
              nextRow.appendChild(lastElem);
            }
            for (var j = 0; j < rowBlockNum; j++) {
  						table.deleteRow(i + j);
  						rowCount--;
  						i--;
            }
					}
				}
        if (rowCount <= 2) {
          $(table).find(".verticalTxt").removeClass("verticalTxt");
        }
			}

      function deleteAttributes(tableID) {
        deleteRows(tableID);
      }

      function deleteTables(tableID) {
        deleteRows(tableID, 2);
      }

      $(document).ready(function() {
        var tablesID = "tablesForm";
        var attributesID = "attributesForm";
        document.getElementById("database").appendChild(createTablesTable(tablesID, attributesID));
        addTable(tablesID, attributesID);
      });

    </script>
  </head>
  <body style="position: relative; ">
    <div class="mdl-js-layout mdl-layout--fixed-header">
		<?php include ABSTEMPLATEPATH .'header.php'; ?>
    <div id="container">

      <?php
        $databases = DatabaseQuery::getDatabases(LoginStatus::getUserId());
        if (count($databases) == 0) {
          echo '<div class="message-warning">' . "You don't have any database yet" . '</div>';
        }
        if (isset($message)) {
          echo '<div class="message-error">' . $message . '</div>';
        }
        if (isset($_GET['action'])) {
          echo '<div class="message-ok">' . "The database has been " . $_GET['action'] . " correctly." . '</div>';
        }
        if ($databases && count($databases) > 0) {
          echo "<table>";
          echo "<tr>
                  <th>" . DatabaseValues::getHumanName(DatabaseValues::DB_Name) . "</th>
                  <th>Tables</th>
                  <th>Actions</th>
                </tr>";
          foreach ($databases as $key => $row) {
            $databaseId = $row[DatabaseValues::DB_ID];
            $confirm = "Are you sure you want to delete the database " . $databaseId . "?";
            echo "<tr>
                   <td>" . $row[DatabaseValues::DB_Name] . "</td>
                   <td>" . printDatabaseTables($databaseId) . "</td>
                   <td>
                    <div class='actions'>
                      <img class='clickable' onclick=" . '"' . "javascript:if (confirm('$confirm')) {document.location.href = '?delete=$databaseId';}" . '"' . " src='". RESPATH . "delete.png'  width='15' height='15' vspace='5' align='middle'>
                    </div>
                   </td>
                 </tr>";
          }
          echo "</table>";
        }

        function printDatabaseTables($databaseId) {
          $strRes = "";
          $tables = DatabaseQuery::getTables($databaseId);
          if ($tables && count($tables) > 0) {
            $strRes.= "<table>";
            $strRes.= "<tr class=expandeable>
                    <th>" . DatabaseValues::getHumanName(DatabaseValues::TBL_Name) . "</th>
                    <th>" . DatabaseValues::getHumanName(DatabaseValues::TBL_Cardinality) . "</th>
                    <th>" . DatabaseValues::getHumanName(DatabaseValues::TBL_Size) . "</th>
                    <th>" . DatabaseValues::getHumanName(DatabaseValues::TBL_N_Partitions) . "</th>
                    <th>" . DatabaseValues::getHumanName(DatabaseValues::TBL_CompressionEnabled) . "</th>
                    <th>Attributes</th>
                  </tr>";
            foreach ($tables as $key => $row) {
              $strRes.= "<tr class=compactable>
                     <td>" . $row[DatabaseValues::TBL_Name] . "</td>
                     <td>" . formatNumber($row[DatabaseValues::TBL_Cardinality]) . "</td>
                     <td>" . bytes($row[DatabaseValues::TBL_Size]) . "</td>
                     <td>" . $row[DatabaseValues::TBL_N_Partitions] . "</td>
                     <td>" . $row[DatabaseValues::TBL_CompressionEnabled] . "</td>
                     <td>" . printTableAttributes($row[DatabaseValues::TBL_ID]) . "</td>
                   </tr>";
            }
            $strRes.= "</table>";
          } else {
              $strRes.= "0 tables";
          }
          return $strRes;
        }

        function printTableAttributes($tableId) {
          $strRes = "";
          $attributes = DatabaseQuery::getAttributes($tableId);
          if ($attributes && count($attributes) > 0) {
            $strRes.= "<table>";
            $strRes.= "<tr class=expandeable>
                    <th>" . DatabaseValues::getHumanName(DatabaseValues::ATT_Name) . "</th>
                    <th>" . DatabaseValues::getHumanName(DatabaseValues::ATT_Cardinality) . "</th>
                    <th>" . DatabaseValues::getHumanName(DatabaseValues::ATT_AvgLength) . "</th>
                    <th>" . DatabaseValues::getHumanName(DatabaseValues::ATT_Highest) . "</th>
                    <th>" . DatabaseValues::getHumanName(DatabaseValues::ATT_Lowest) . "</th>
                  </tr>";
            foreach ($attributes as $key => $row) {
              $strRes.= "<tr class=compactable>
                     <td>" . $row[DatabaseValues::ATT_Name] . "</td>
                     <td>" . formatNumber($row[DatabaseValues::ATT_Cardinality]) . "</td>
                     <td>" . $row[DatabaseValues::ATT_AvgLength] . "</td>
                     <td>" . $row[DatabaseValues::ATT_Highest] . "</td>
                     <td>" . $row[DatabaseValues::ATT_Lowest] . "</td>
                   </tr>";
            }
            $strRes.= "</table>";
          } else {
              $strRes.= "0 tables";
          }
          return $strRes;
        }

        function formatNumber($number) {
          return number_format($number);
        }

        function bytes($a) {
          $unim = array("B","KB","MB","GB","TB","PB");
          $c = 0;
          while ($a>=1024) {
            $c++;
            $a = $a/1024;
          }
          return number_format($a,($c ? 2 : 0),",",".")." ".$unim[$c];
        }
      ?>

      <form id="database" class="border" action="" method="post">
        <label class="field_label">
          Database name:
          <input type="text" name="<?php echo DatabaseValues::DB_Name; ?>" autocomplete="off"
          title="Only aphabetic characters are allowed."
          placeholder="Name">
        </label>

        <div class="actions">
          <input type="submit" value="Insert" name="insert">
        </div>
      </form>
    </div>
    </div>
  </body>
</html>
