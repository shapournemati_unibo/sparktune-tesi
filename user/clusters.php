<?php
require_once(dirname(dirname(__FILE__)) . '/load-config.php');
include dirname(__FILE__).'/DatabaseQuery.php';
require_once ABSAUTHPATH."loginStatus.php";

if (isset($_GET["message"])) {
  $message = $_GET["message"];
}

if(isset($_POST['insert'])) {
  $clusterId = DatabaseQuery::insertCluster(
        LoginStatus::getUserId(),
        $_POST[DatabaseValues::CL_Name],
        $_POST[DatabaseValues::CL_N_R],
        $_POST[DatabaseValues::CL_N_RN],
        $_POST[DatabaseValues::CL_N_C],
        $_POST[DatabaseValues::CL_Rf],
        $_POST[DatabaseValues::CL_N_SB],
        $_POST[DatabaseValues::CL_SCmp],
        $_POST[DatabaseValues::CL_FCmp],
        $_POST[DatabaseValues::CL_HSel],
        $_POST[DatabaseValues::CL_IntraRSpeed]*1024*1024/8,
        $_POST[DatabaseValues::CL_ExtraRSpeed]*1024*1024/8);
  if ($clusterId == false) {
    $message="ERROR inserting cluster";
    reloadEdit();
  } else {
    $drThArray = explode(",", str_replace(' ', '', $_POST[DatabaseValues::TR_Type_DR]));
    $dwThArray = explode(",", str_replace(' ', '', $_POST[DatabaseValues::TR_Type_DW]));
    $drTh = DatabaseQuery::insertThroughputs($clusterId, DatabaseValues::TR_Type_DR, $drThArray);
    $dwTh = DatabaseQuery::insertThroughputs($clusterId, DatabaseValues::TR_Type_DW, $dwThArray);
    if ($drTh && $dwTh) {
      header('Location: ?action=inserted');
    } else {
      $message="ERROR inserting throughput values";
      reloadEdit();
    }
  }
} elseif(isset($_POST['update'])) {
  if (!DatabaseQuery::updateCluster(
        $_POST["editID"],
        $_POST[DatabaseValues::CL_Name],
        $_POST[DatabaseValues::CL_N_R],
        $_POST[DatabaseValues::CL_N_RN],
        $_POST[DatabaseValues::CL_N_C],
        $_POST[DatabaseValues::CL_Rf],
        $_POST[DatabaseValues::CL_N_SB],
        $_POST[DatabaseValues::CL_SCmp],
        $_POST[DatabaseValues::CL_FCmp],
        $_POST[DatabaseValues::CL_HSel],
        $_POST[DatabaseValues::CL_IntraRSpeed]*1024*1024/8,
        $_POST[DatabaseValues::CL_ExtraRSpeed]*1024*1024/8)) {
    $message="ERROR updating cluster";
    $editID = $_POST["editID"];
    reloadEdit();
  } else {
    $drThArray = explode(",", str_replace(' ', '', $_POST[DatabaseValues::TR_Type_DR]));
    $dwThArray = explode(",", str_replace(' ', '', $_POST[DatabaseValues::TR_Type_DW]));
    $drTh = DatabaseQuery::updateThroughputs($_POST["editID"], DatabaseValues::TR_Type_DR, $drThArray);
    $dwTh = DatabaseQuery::updateThroughputs($_POST["editID"], DatabaseValues::TR_Type_DW, $dwThArray);
    if ($drTh && $dwTh) {
      header('Location: ?action=updated');
    } else {
      $message="ERROR updating throughputs";
      $editID = $_POST["editID"];
      reloadEdit();
    }
  }
} elseif (isset($_GET['delete'])) {
  if (!DatabaseQuery::deleteCluster($_GET['delete'])) {
    header('Location: ?action=error&message=' . urlencode("ERROR deleting cluster" . $_GET['delete']));
  } else {
    header('Location: ?action=deleted');
  }
} elseif (isset($_GET['edit'])) {
  $cluster = DatabaseQuery::getCluster($_GET['edit']);
  if (!$cluster) {
    header('Location: ?action=error&message=' . urlencode("ERROR editing cluster" . $_GET['edit']));
  } else {
    $editID = $cluster[DatabaseValues::CL_ClusterId];
    reloadEdit($cluster);
  }
}

function reloadEdit($cluster = NULL) {
  if ($cluster == NULL) {
    $GLOBALS['name'] = $_POST[DatabaseValues::CL_Name];
    $GLOBALS['nR'] = $_POST[DatabaseValues::CL_N_R];
    $GLOBALS['nRN'] = $_POST[DatabaseValues::CL_N_RN];
    $GLOBALS['nC'] = $_POST[DatabaseValues::CL_N_C];
    $GLOBALS['rf'] = $_POST[DatabaseValues::CL_Rf];
    $GLOBALS['nSB'] = $_POST[DatabaseValues::CL_N_SB];
    $GLOBALS['sCmp'] = $_POST[DatabaseValues::CL_SCmp];
    $GLOBALS['fCmp'] = $_POST[DatabaseValues::CL_FCmp];
    $GLOBALS['hSel'] = $_POST[DatabaseValues::CL_HSel];
    $GLOBALS['intraRSpeed'] = $_POST[DatabaseValues::CL_IntraRSpeed];
    $GLOBALS['extraRSpeed'] = $_POST[DatabaseValues::CL_ExtraRSpeed];
    $GLOBALS['drThroughput'] = $_POST[DatabaseValues::TR_Type_DR];
    $GLOBALS['dwThroughput'] = $_POST[DatabaseValues::TR_Type_DW];
  } else {
    $GLOBALS['name'] = $cluster[DatabaseValues::CL_Name];
    $GLOBALS['nR'] = $cluster[DatabaseValues::CL_N_R];
    $GLOBALS['nRN'] = $cluster[DatabaseValues::CL_N_RN];
    $GLOBALS['nC'] = $cluster[DatabaseValues::CL_N_C];
    $GLOBALS['rf'] = $cluster[DatabaseValues::CL_Rf];
    $GLOBALS['nSB'] = $cluster[DatabaseValues::CL_N_SB];
    $GLOBALS['sCmp'] = $cluster[DatabaseValues::CL_SCmp];
    $GLOBALS['fCmp'] = $cluster[DatabaseValues::CL_FCmp];
    $GLOBALS['hSel'] = $cluster[DatabaseValues::CL_HSel];
    $GLOBALS['intraRSpeed'] = $cluster[DatabaseValues::CL_IntraRSpeed]*8/1024/1024;
    $GLOBALS['extraRSpeed'] = $cluster[DatabaseValues::CL_ExtraRSpeed]*8/1024/1024;
    $GLOBALS['drThroughput'] = implode(",", DatabaseQuery::getThroughputs($cluster[DatabaseValues::CL_ClusterId], DatabaseValues::TR_Type_DR));
    $GLOBALS['dwThroughput'] = implode(",", DatabaseQuery::getThroughputs($cluster[DatabaseValues::CL_ClusterId], DatabaseValues::TR_Type_DW));
  }
}
?>

<!doctype html>
<html>
  <head>
    <title>Spark cost</title>

    <!-- Il file contiene una serie di librerie utilizzate in tutto l'applcativo-->
    <?php
      include ABSTEMPLATEPATH . 'head.php';
      echo '<link rel="stylesheet" href="'.TEMPLATEPATH.'/styles/style.css"/>';
    ?>

	</head>
	<body style="<!--background-image: url('img/bg10.png');--> position: relative; ">

		<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
		<?php include ABSTEMPLATEPATH .'header.php'; ?>

    <div>  <!-- contenuto pagine -->

      <?php
        $clusters = DatabaseQuery::getClusters(LoginStatus::getUserId());
        if (count($clusters) == 0) {
          echo '<div class="message-warning">' . "You don't have any cluster yet" . '</div>';
        }
        if ($message) {
          echo '<div class="message-error">' . $message . '</div>';
        }
        if (isset($_GET['action'])) {
          echo '<div class="message-ok">' . "The cluster has been " . $_GET['action'] . " correctly." . '</div>';
        }
        if ($clusters && count($clusters) > 0) {
          echo "<table>";
          echo "<tr>
                  <th>" . DatabaseValues::getHumanName(DatabaseValues::CL_Name) . "</th>
                  <th>" . DatabaseValues::getHumanName(DatabaseValues::CL_N_R) . "</th>
                  <th>" . DatabaseValues::getHumanName(DatabaseValues::CL_N_RN) . "</th>
                  <th>" . DatabaseValues::getHumanName(DatabaseValues::CL_N_C) . "</th>
                  <th>" . DatabaseValues::getHumanName(DatabaseValues::CL_Rf) . "</th>
                  <th>" . DatabaseValues::getHumanName(DatabaseValues::CL_N_SB) . "</th>
                  <th>" . DatabaseValues::getHumanName(DatabaseValues::CL_SCmp) . "</th>
                  <th>" . DatabaseValues::getHumanName(DatabaseValues::CL_FCmp) . "</th>
                  <th>" . DatabaseValues::getHumanName(DatabaseValues::CL_HSel) . "</th>
                  <th>" . DatabaseValues::getHumanName(DatabaseValues::CL_IntraRSpeed) . "</th>
                  <th>" . DatabaseValues::getHumanName(DatabaseValues::CL_ExtraRSpeed) . "</th>
                  <th>Actions</th>
                </tr>";
          foreach ($clusters as $key => $row) {
            $clusterId = $row[DatabaseValues::CL_ClusterId];
            $confirm = "Are you sure you want to delete the cluster " . $clusterId . "?";
            echo "<tr>
             <td>" . $row[DatabaseValues::CL_Name] . "</td>
             <td>" . $row[DatabaseValues::CL_N_R] . "</td>
             <td>" . $row[DatabaseValues::CL_N_RN] . "</td>
             <td>" . $row[DatabaseValues::CL_N_C] . "</td>
             <td>" . $row[DatabaseValues::CL_Rf] . "</td>
             <td>" . $row[DatabaseValues::CL_N_SB] . "</td>
             <td>" . $row[DatabaseValues::CL_SCmp] . "</td>
             <td>" . $row[DatabaseValues::CL_FCmp] . "</td>
             <td>" . $row[DatabaseValues::CL_HSel] . "</td>
             <td>" . $row[DatabaseValues::CL_IntraRSpeed]*8/1024/1024 . "</td>
             <td>" . $row[DatabaseValues::CL_ExtraRSpeed]*8/1024/1024 . "</td>
             <td>
              <div class='actions'>
                <a href='?edit=" . $row[DatabaseValues::CL_ClusterId] . "'>
                  <img src='". RESPATH . "edit.png' width='15' vspace='5' align='middle'>
                </a>
                <img class='clickable' onclick=" . '"' . "javascript:if (confirm('$confirm')) {document.location.href = '?delete=$clusterId';}" . '"' . " src='". RESPATH . "delete.png' width='15' height='15' vspace='5' align='middle'>
              </div>
             </td>
             </tr>";
          }
          echo "</table>";
        }
      ?>

      <form action="" class="border" method="post">
        <?php
        if (isset($editID)) {
          echo '<input type="hidden" name="editID" value="' . $editID . '">';
        }
        ?>
        <label class="field_label">
          Name:
          <input type="text" <?php
            echo 'name="'. DatabaseValues::CL_Name .'"';
            if (isset($name)) {
              echo 'value="' . $name . '"';
            }
            ?> autocomplete="off"
          title="Aphabetic characters are allowed."
          placeholder="Cluster name">
        </label>

        <label class="field_label">
          Rack number:
          <input type="number" <?php
            echo 'name="'. DatabaseValues::CL_N_R .'"';
            if (isset($nR)) {
              echo 'value="' . $nR . '"';
            }
            ?> min=0 autocomplete="off"
          title="Only numbers are allowed."
          placeholder="Rack number">
        </label>

        <label class="field_label">
          Nodes in a rack:
          <input type="number" <?php
            echo 'name="'. DatabaseValues::CL_N_RN .'"';
            if (isset($nRN)) {
              echo 'value="' . $nRN . '"';
            }
            ?> min=0 autocomplete="off"
          title="Only numbers are allowed."
          placeholder="Nodes in a rack">
        </label>

        <label class="field_label">
          Cores in a worker:
          <input type="number" <?php
            echo 'name="'. DatabaseValues::CL_N_C .'"';
            if (isset($nC)) {
              echo 'value="' . $nC . '"';
            }
            ?> min=0 autocomplete="off"
          title="Only numbers are allowed."
          placeholder="Cores in a worker">
        </label>

        <label class="field_label">
          Redundancy factor:
          <input type="number" <?php
              echo 'name="'. DatabaseValues::CL_Rf .'"';
            if (isset($rf)) {
              echo 'value="' . $rf . '"';
            }
            ?> min=0 autocomplete="off"
          title="Only numbers are allowed."
          placeholder="Redundancy factor">
        </label>

        <label class="field_label">
          Number of buckets used for shuffeling:
          <input type="number" <?php
            echo 'name="'. DatabaseValues::CL_N_SB .'"';
            if (isset($nSB)) {
              echo 'value="' . $nSB . '"';
            }
            ?> min=0 autocomplete="off"
          title="Only numbers are allowed."
          placeholder="Number of buckets">
        </label>

        <label class="field_label">
          Percentage of data reduction during a shuffle operation (sCmp):
          <input type="number" <?php
            echo 'name="'. DatabaseValues::CL_SCmp .'"';
            if (isset($sCmp)) {
              echo 'value="' . $sCmp . '"';
            }
            ?> min=0 max=1 step="0.01" autocomplete="off"
          title="Only numbers are allowed."
          placeholder="sCmp">
        </label>

        <label class="field_label">
          Percentage of data reduction of the file (fCmp):
          <input type="number" <?php
            echo 'name="'. DatabaseValues::CL_FCmp .'"';
            if (isset($fCmp)) {
              echo 'value="' . $fCmp . '"';
            }
            ?> min=0 max=1 step="0.01" autocomplete="off"
          title="Only numbers are allowed."
          placeholder="fCmp">
        </label>

        <label class="field_label">
          Intra rack speed (Mbps):
          <input type="number" <?php
            echo 'name="'. DatabaseValues::CL_IntraRSpeed .'"';
            if (isset($intraRSpeed)) {
              echo 'value="' . $intraRSpeed . '"';
            }
            ?> min=0 step="0.01" autocomplete="off"
          title="Only numbers are allowed."
          placeholder="intra rack speed (Mbps):">
        </label>

        <label class="field_label">
          Extra rack speed (Mbps):
          <input type="number" <?php
            echo 'name="'. DatabaseValues::CL_ExtraRSpeed .'"';
            if (isset($extraRSpeed)) {
              echo 'value="' . $extraRSpeed . '"';
            }
            ?> min=0 step="0.01" autocomplete="off"
          title="Only numbers are allowed."
          placeholder="extra rack speed (Mbps):">
        </label>

        <label class="field_label">
          Disk read throughputs:
          <input type="text" <?php
            echo 'name="'. DatabaseValues::TR_Type_DR .'"';
            if (isset($drThroughput)) {
              echo 'value="' . $drThroughput . '"';
            }
            ?> autocomplete="off"
          title="Values separated by comma."
          placeholder="Disk read throughputs">
        </label>

        <label class="field_label">
          Disk write throughputs:
          <input type="text" <?php
            echo 'name="'. DatabaseValues::TR_Type_DW .'"';
            if (isset($dwThroughput)) {
              echo 'value="' . $dwThroughput . '"';
            }
            ?> autocomplete="off"
          title="Values separated by comma."
          placeholder="Disk write throughputs">
        </label>

        <label class="field_label">
          Constant selectivity for having clauses (hSel):
          <input type="number" <?php
            echo 'name="'. DatabaseValues::CL_HSel .'"';
            if (isset($hSel)) {
              echo 'value="' . $hSel . '"';
            }
            ?> min=0 max=1 step="0.01" autocomplete="off"
          title="Only numbers are allowed."
          placeholder="Constant selectivity for HAVING">
        </label>

        <div class="actions">
          <?php
          if (isset($editID)) {
            // Update the current cluster
            echo '<input type="submit" value="Update" name="update">';
          } else {
            // Insert new cluster
            echo '<input type="submit" value="Insert" name="insert">';
          }
          ?>
        </div>
      </form>
    </div>

		</div>





	</body>
</html>
