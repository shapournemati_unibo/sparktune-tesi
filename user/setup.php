<?php
require_once(dirname(dirname(__FILE__)) . '/load-config.php');
require_once ABSAUTHPATH."loginStatus.php";
include dirname(__FILE__).'/DatabaseQuery.php';

  if(isset($_GET['delete'])){
    $deleteWL = $_GET['delete'];
    DatabaseQuery::deleteWorkload($deleteWL);
  }

  if(isset($_POST['workload']) && isset($_POST['Spark_Version']) && isset($_POST['workloadName']) && isset($_POST['sql'])){
    $workload = json_decode($_POST['workload']);
    $sql = json_decode($_POST['sql']);
    $sparkVersion = $_POST['Spark_Version'];
    $workloadName = $_POST['workloadName'];

    $id = DatabaseQuery::insertWorkload(LoginStatus::getUserId(),$workloadName,$sparkVersion);

    $i = 0;
    foreach($workload as $key => $value){
      DatabaseQuery::insertQuery($id,$value,$sql[$i]);
      $i++;
    }
  }




?>

<!doctype html>
<html>
  <head>
    <title>Spark cost</title>

    <!-- Il file contiene una serie di librerie utilizzate in tutto l'applcativo-->
    <?php
      include ABSTEMPLATEPATH . 'head.php';
      echo '<link rel="stylesheet" href="'.TEMPLATEPATH.'/styles/style.css"/>';
    ?>

    <!-- Script contenente l'handler per la lista dinamica di query e il submit del workload-->
    <script type="text/javascript">
    jQuery(function($){

      $(document).ready(function() {
      $(document).on('click','#addQueryBtn', function() {
          var $list = $("#queryList");
          var $new_row = $('<div>'+
              '<img class="clickable remove" src="/sparkcost2/res/delete.png" width="15" height="15" vspace="5" align="middle">'+
              '</div>').appendTo($list);
          $new_row.prepend('<p hidden class="physicalPlan">'+  document.getElementById("inputTextArea").value +'</p>' +
          '<p hidden class="sqlQuery">'+  document.getElementById("inputSQL").value +'</p>' +
          '<label class="num clickable open-modal">Query '+ ($new_row.index() + 1)  +'</label>');

          $("#modalContainer").append('<div id="modal'+($new_row.index() + 1)+'" class="modal">'+
                          '<nav class="modal-content">' +
                          '<span class="close">&times;</span>' +
                          '<p>'+document.getElementById("inputTextArea").value+'</p>' +
                          '</nav></div>')
      });

      $(document).on('click','.open-modal',function(){
        var $index = $(this).closest('div').index()+1;
        $("#modal"+$index).css('display','block');
      });

      $(document).on('click', '.remove', function() {
          var $row = $(this).closest('div'),
              $list = $("#queryList"),$modal = $("#modal"+($row.index()+1));
          $row.remove();
          $modal.remove();

          $list.find('div').each(function(i,v) {
              $(v).find('label.num').text("Query "+ (i+1));
          });

          $("#modalContainer").find('div').each(function(i,v) {
              $(v).prop('id','modal'+(i+1));
          });

      });

      $(document).on('click','.close',function(){
        $("#modalContainer").find('div').each(function(i,v) {
            $(v).css('display','none');
        });
      });

      $(document).on('click','#submitBtn',function(){
        var workload = new Array();
        $("#queryList").find('p.physicalPlan').each(function(i,v) {
            workload.push($(v).text());
        });
        var sqlQuery = new Array();
        $("#queryList").find('p.sqlQuery').each(function(i,v) {
            sqlQuery.push($(v).text());
        });
        localStorage.setItem("workload" , workload);
        $("#workload").val(JSON.stringify(workload));
        $("#sql").val(JSON.stringify(sqlQuery));
        document.getElementById('form').submit()
      });
  });

    });

    </script>

	</head>
	<body style="<!--background-image: url('img/bg10.png');--> position: relative; ">
    <div id="modalContainer"></div>
		<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
		<?php include ABSTEMPLATEPATH .'header.php'; ?>

    <div>  <!-- contenuto pagine -->
      <?php
      # ------------------------------------------------------
      #
      # Print the instructions
      #
      # ------------------------------------------------------
      echo "<section class='full-width'>" . PHP_EOL;
      echo "<div class='section_title'>Instructions</div> </br>" . PHP_EOL;
      echo "1 - Fetch the phisical plan generated by Spark SQL (prepend 'EXPLAIN' before the query); </br>" . PHP_EOL;
      echo "2 - Copy it inside the following section, then load the plan by pressing the relative button; </br>" . PHP_EOL;
      echo "3 - Choose the database and the cluster (if you don't have any, con to the respective pages and insert at least one of each); </br>" . PHP_EOL;
      echo "4 - Compile the query parameters, then compute the cost of the query simply by pressing the compute button; </br>" . PHP_EOL;
      echo "</section>" . PHP_EOL;

      # ------------------------------------------------------
      #
      # Print all the form features and components
      #
      # ------------------------------------------------------
      echo '<form  class="cost-model-form" id="form" method="post" action="">';
      # Print the query insert form
      echo "<section>" . PHP_EOL;
      echo '<div class="section_title">Phisical plan generated by Spark</div>' . PHP_EOL;
      echo printFormQuery() . PHP_EOL;
      echo "</section>" . PHP_EOL;


      # ------------------------------------------------------
      #
      # Print the structure for the interactive query list
      #
      # ------------------------------------------------------
      echo "<section>" . PHP_EOL;
      echo '<div class="section_title">Workload Name<input type="text" name="workloadName"><select name="Spark_Version">
      <option value="sp1.5.0">Spark 1.5</option>
      <option value="sp2.2.0" selected>Spark 2.2</option>
      </select></div>' . PHP_EOL;
      echo '';
      echo '<div id="queryList"></div>'. PHP_EOL;
      echo '<div class="actions">';
      echo '<input type="button" id="submitBtn" name="action" value="Submit Workload"/>';
      echo '</div>';
      echo "</section>" . PHP_EOL;
      echo "</form>";

      $workloads = DatabaseQuery::getWorkloads(LoginStatus::getUserId());
      $confirm = "Are you sure you want to delete the workload?";

      #Print the form for the workload deletion
      echo '<form style="width:40%;margin:auto;" class="cost-model-form" id="formDelete" method="post" action="">';
      echo "<section>" . PHP_EOL;
      echo '<div class="section_title">Delete Workload</div>' . PHP_EOL;
      foreach ($workloads as $key => $row) {
        $wlID = $row[DatabaseValues::WL_ID];
        echo '<div><label class="num">'. $row[DatabaseValues::WL_NAME] .'</label>';
        echo "<img style='margin-right:3em;' class='clickable' onclick=" . '"' . "javascript:if (confirm('$confirm')) {document.location.href = '?delete=$wlID';}" . '"' . " src='". RESPATH . "delete.png' width='15' height='15' vspace='5' align='right'></div>";
      }
      echo "</section>" . PHP_EOL;
      echo "</form>";


      function printFormQuery($defaultQuery = NULL) {
        $res = '<textarea id="inputTextArea" rows="4" cols="50" name="query" autocomplete="off"';
        $res .= 'title="Alphabetic characters are allowed."';
        $res .= 'placeholder="Physical plan">';
        if ($defaultQuery) {
          $res .= $defaultQuery;
        }
        $res .= '</textarea>';
        $res .= '<textarea id="inputSQL" rows="4" cols="50" name="sqlText" autocomplete="off"';
        $res .= 'placeholder="SQL query (optional)">';
        $res .= '</textarea>';
        $res .= '<input type="hidden" id="workload" name="workload">';
        $res .= '<input type="hidden" id="sql" name="sql">';
        $res .= '<div class="actions">' . PHP_EOL;
        $res .= '<button type="button" id="addQueryBtn">Add Query</button>';
        //$res .= '<input type="button" id="submitBtn" name="action" value="Submit Workload"/>';
        $res .= '</div>';
        return $res;
      }
       ?>


    </div>
    </div>





  </body>
</html>
