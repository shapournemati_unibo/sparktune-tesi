/*
-------------------------------------
Constants and parameters

relative origin represents the point [-10,-10] where we place our
imaginary origin [0,0]
-------------------------------------
*/

var origin = [480, 450], relativeOrigin = -10, scale = 20, points = [],
 yLine = [], xGrid = [], beta = 0, alpha = 0, key = function(d){ return d.id; }, startAngle = Math.PI/4, minAxix = 0, minRange = 0;
var svg    = d3.select('svg').call(d3.drag().on('drag', dragged).on('start', dragStart).on('end', dragEnd)).append('g');
var color = d3.scaleLinear();
var mx, my, mouseX, mouseY;

/*
-------------------------------------
Variables repesenting our enviroment, built using a grid, a line for the
Y axix and a surface for our 3d function
-------------------------------------
*/

var grid3d = d3._3d()
    .shape('GRID', 20)
    .origin(origin)
    .rotateY( startAngle)
    .rotateX(-startAngle)
    .scale(scale);

var yScale3d = d3._3d()
    .shape('LINE_STRIP')
    .origin(origin)
    .rotateY( startAngle)
    .rotateX(-startAngle)
    .scale(scale);

var surface = d3._3d()
    .scale(scale)
    .x(function(d){ return d.x; })
    .y(function(d){ return d.y; })
    .z(function(d){ return d.z; })
    .origin(origin)
    .rotateY(startAngle)
    .rotateX(-startAngle)
    .shape('SURFACE', 20);


/*
-------------------------------------
Our function that processes data and takes the responsibility of the rendering
-------------------------------------
*/

function processData(data, tt){

  /* ----------- SURFACE ----------- */
  var planes = svg.selectAll('path.graph').data(data[2], function(d){ return d.plane; });

          planes
              .enter()
              .append('path')
              .attr('class', '_3d graph')
              .attr('fill', colorize)
              .attr('opacity', 0)
              .attr('stroke-opacity', 0.1)
              .merge(planes)
              .attr('stroke', 'black')
              .transition().duration(tt)
              .attr('opacity', 1)
              .attr('fill', colorize)
              .attr('d', surface.draw);

          planes.exit().remove();


    /* ----------- GRID ----------- */

    var xGrid = svg.selectAll('path.grid').data(data[0], key);

    xGrid
        .enter()
        .append('path')
        .attr('class', '_3d grid')
        .merge(xGrid)
        .attr('stroke', 'black')
        .attr('stroke-width', 0.3)
        .attr('fill', function(d){ return d.ccw ? 'lightgrey' : '#717171'; })
        .attr('fill-opacity', 0.9)
        .attr('d', grid3d.draw);

    xGrid.exit().remove();


    /* ----------- y-Scale ----------- */

    var yScale = svg.selectAll('path.yScale').data(data[1]);

    yScale
        .enter()
        .append('path')
        .attr('class', '_3d yScale')
        .merge(yScale)
        .attr('stroke', 'black')
        .attr('stroke-width', .5)
        .attr('d', yScale3d.draw);

    yScale.exit().remove();

     /* ----------- y-Scale Text ----------- */

    var yText = svg.selectAll('text.yText').data(data[1][0]);

    yText
        .enter()
        .append('text')
        .attr('class', '_3d yText')
        .attr('dx', '.3em')
        .merge(yText)
        .each(function(d){
            d.centroid = {x: d.rotated.x, y: d.rotated.y, z: d.rotated.z};
        })
        .attr('x', function(d){ return d.projected.x; })
        .attr('y', function(d){ return d.projected.y; })
        .text(function(d){ return -d[1]/* <= 0 ? d[1] : '';*/ });
        /*
        here the text is reversed in order to put the positive y axis upwards.
        all the data on Y axis should be reversed too (apply -y)
        */

    yText.exit().remove();

    d3.selectAll('._3d').sort(d3._3d().sort);
}

function posPointX(d){
    return d.projected.x;
}

function posPointY(d){
    return d.projected.y;
}

function colorize(d){
    var _y = (d[0].y + d[1].y + d[2].y + d[3].y)/4;


    /*
    Since it is not possible to create a non-square grid/surface
    we simply uncolor the unwanted part but still calculate the entire square
    to ensure the curve smoothness.
    minRange is the actual VALUE of the lowest between X and Z axix,
    while minAxix just indicates which is the lowest between the two.
    These values are set in the method 'setPlaneRange' while instantiating
    the figure dimensions.
    */

    if(minAxix == 0){

      if(d[0].z > minRange + relativeOrigin){
        var c = d3.color("steelblue");
        c.opacity = 0;
        return c;
      } else {
        return d.ccw ? d3.interpolateSpectral(color(_y)) : d3.color(d3.interpolateSpectral(color(_y))).darker(2.5);
      }

    } else {

      if(d[0].x > minRange + relativeOrigin){
        var c = d3.color("steelblue");
        c.opacity = 0;
        return c;
      } else {
        return d.ccw ? d3.interpolateSpectral(color(_y)) : d3.color(d3.interpolateSpectral(color(_y))).darker(2.5);
      }

    }


}

/*
-------------------------------------
Before rendering the graph, the grid dimension and plane range should be set
-------------------------------------
*/
function setAxisDimension(gridDimension, yHeight){

  grid3d.shape("GRID",gridDimension);

  //grid and Y line
  var cnt = 0;
  xGrid = [], yLine = [];
  for(var z = relativeOrigin; z < gridDimension + relativeOrigin; z++){
      for(var x = relativeOrigin; x < gridDimension + relativeOrigin; x++){
          xGrid.push([x, 0, z]);
      }
  }

  d3.range(0, yHeight, 1).forEach(function(d){ yLine.push([relativeOrigin, -d, relativeOrigin]); });
}

/*
-------------------------------------
The step for each of X and Z axix is intended to be 1.

The range of the colors is set to be between the minimum and the maximum of the
plotted plane.
-------------------------------------
*/
function setPlaneRange(Xrange, Zrange ,equation){
  points = [];

  var Max;

  if(Xrange > Zrange){
    Max = Xrange;
    minAxix = 0; //0 stands for Z axix
    minRange = Zrange;
  } else {
    Max = Zrange;
    minAxix = 1; //1 stands for X axix
    minRange = Xrange;
  }

  for(var z = 0; z < Max; z++){
      for(var x = 0; x < Max; x++){
          points.push({x: x + relativeOrigin, y: -equation(x, z), z: z + relativeOrigin}); //data on y axix is reversed (negative side is upward)
      }
  }

  var yMin = d3.min(points, function(d){ return d.y; });
  var yMax = d3.max(points, function(d){ return d.y; });

  color.domain([yMin, yMax]);

  surface.shape('SURFACE', Max)


}

function render(){

  var data = [
      grid3d(xGrid),
      yScale3d([yLine]),
      surface(points)
  ];
  processData(data, 1000);
}

/*
-------------------------------------
Functions taking the responsibility of our 3d rotation on drag
-------------------------------------
*/

function dragStart(){
    mx = d3.event.x;
    my = d3.event.y;
}

function dragged(){
    mouseX = mouseX || 0;
    mouseY = mouseY || 0;
    beta   = (d3.event.x - mx + mouseX) * Math.PI / 230 ;
    alpha  = (d3.event.y - my + mouseY) * Math.PI / 230  * (-1);
    var data = [
        grid3d.rotateY(beta + startAngle).rotateX(alpha - startAngle)(xGrid),
        yScale3d.rotateY(beta + startAngle).rotateX(alpha - startAngle)([yLine]),
        surface.rotateY(beta + startAngle).rotateX(alpha - startAngle)(points)
    ];
    processData(data, 0);
}

function dragEnd(){
    mouseX = d3.event.x - mx + mouseX;
    mouseY = d3.event.y - my + mouseY;
}


/*
-------------------------------------
Sample init function
-------------------------------------
*/
function init(){

setAxisDimension(20,10);

var eq = function(x, z){
    return Math.cos(Math.sqrt(x*x+z*z)/5*Math.PI);
};

setPlaneRange(10,15,eq);

render();

}
