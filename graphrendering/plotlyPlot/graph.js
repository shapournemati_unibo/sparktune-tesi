var graph, xCoords, yCoords, zCoords,xName='X',yName='Y',zName='Z',title='Workload 3D Surface';

function setXName(name){
  xName = name;
}

function setYName(name){
  yName = name;
}

function setZName(name){
  zName = name;
}

function setTitle(newTitle){
  title = newTitle;
}

function setXCoords(data){
  xCoords = data;
}

function setYCoords(data){
  yCoords = data;
}

function setZCoordsByValue(data){
  zCoords = data;
}

function renderGraph(){

  // fill in 'text' array for hover
  var text = yCoords.map((yi, i) => xCoords.map((xi, j) => `
  ${xName}: ${xi}<br>
  ${yName}: ${yi}<br>
  ${zName}: ${zCoords[i][j]}
  `))

  var data = [{
    hoverinfo: 'text',
    text: text,
    z: zCoords,
    x: xCoords,
    y: yCoords,
    type: 'surface'
  }];

  var layout = {
    scene: {
		xaxis:{title: xName},
		yaxis:{title: yName},
		zaxis:{title: zName},
		},
  title: title,
  autosize: true,
  margin: {
    l: 65,
    r: 50,
    b: 65,
    t: 90,
  }
};

Plotly.newPlot('graphDiv', data, layout);


}

function plot2DGraph(xData,yData,comments,myTitle){
  var trace = {
    x: xData,
    y: yData,
    mode: 'markers',
    marker:{
      color:yData
    },
    text: comments
  };

  var layout = {
  xaxis:{title: 'Time(M)'},
  yaxis:{title: 'Cost($)'},
  title: myTitle
};
  var data = [trace];
  Plotly.newPlot('graph2D', data, layout);
}

function downloadCSV(){
  // Building the CSV from the Data two-dimensional array
// Each column is separated by ";" and new line "\n" for next row
var csvContent = xName+'\n';
csvContent += yName+'\n';
csvContent += zName+'\n';
xCoords.forEach(function(item,index){
  csvContent += index < xCoords.length-1 ? item + ';' : item+'\n';
});
yCoords.forEach(function(item,index){
  csvContent += index < yCoords.length-1 ? item + ';' : item+'\n';
});
zCoords.forEach(function(infoArray, index) {
  dataString = infoArray.join(';');
  csvContent += index < zCoords.length ? dataString + '\n' : dataString;
});

// The download function takes a CSV string, the filename and mimeType as parameters
// Scroll/look down at the bottom of this snippet to see how download is called
var download = function(content, fileName, mimeType) {
  var a = document.createElement('a');
  mimeType = mimeType || 'application/octet-stream';

  if (navigator.msSaveBlob) { // IE10
    navigator.msSaveBlob(new Blob([content], {
      type: mimeType
    }), fileName);
  } else if (URL && 'download' in a) { //html5 A[download]
    a.href = URL.createObjectURL(new Blob([content], {
      type: mimeType
    }));
    a.setAttribute('download', fileName);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  } else {
    location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
  }
}

download(csvContent, 'download.csv', 'text/csv;encoding:utf-8');
}
