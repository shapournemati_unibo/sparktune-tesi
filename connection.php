<?php
require_once(dirname(__FILE__) . '/load-config.php');

function openConnection() {
  // Create connection
  $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME, DBPORT);
  // Check connection
  if ($conn->connect_errno) {
      echo "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
  }
  return $conn;
}

function closeConnection($connection) {
  // Close connection
  $connection->close();
}
?>
