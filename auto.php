<?php
require_once(dirname(__FILE__) . '/load-config.php');
include_once(dirname(__FILE__) . '/src/view/GrammarDerivationView.php');
include_once(dirname(__FILE__) . '/src/GPSJGrammarParser/PhisicalPlan.php');
include_once(dirname(__FILE__) . '/src/costModel/CostModel.php');
include_once(dirname(__FILE__) . '/src/costModel/Database.php');
include_once(dirname(__FILE__) . '/src/costModel/Table.php');
include_once(dirname(__FILE__) . '/user/DatabaseQuery.php');

error_reporting(E_ALL);
ini_set("display_errors", 1);

$queries = DatabaseQuery::getQueries(20);
$version = DatabaseQuery::getWorkloadVersion(20);


foreach ($queries as $key => $value) {
  echo '<br><br>';
  for($i = 1; $i <= 6; $i++){
    for($j=2;$j<=8;$j+=2){
      $parameters = getClusterParameters(1, $i, $j);
      $physicalDatabase = getDatabase(1);
      $grammarDerivation = (new PhisicalPlan($value, $version))->getGrammarDerivationJson();
      $costModel = new CostModel($parameters,$physicalDatabase,$grammarDerivation);

      echo seconds($costModel->getCost()) . ';';
    }
  }


}


function getDatabase($databaseId) {
  $databaseSql = DatabaseQuery::getDatabase($databaseId);
  $database = new Database($databaseSql[DatabaseValues::DB_Name]);
  $tablesSql = DatabaseQuery::getTables($databaseSql[DatabaseValues::DB_ID]);
  foreach ($tablesSql as $tableId => $tableSql) {
    // $name, $cardinality, $size, $nPartitions
    $table = new Table(
      $tableSql[DatabaseValues::TBL_Name],
      $tableSql[DatabaseValues::TBL_Cardinality],
      $tableSql[DatabaseValues::TBL_Size],
      $tableSql[DatabaseValues::TBL_N_Partitions]
    );
    $attributesSql = DatabaseQuery::getAttributes($tableId);
    foreach ($attributesSql as $attributeId => $attributeSql) {
      // $name, $cardinality, $length, $high, $low
      $attribute = new Attribute(
        $attributeSql[DatabaseValues::ATT_Name],
        $attributeSql[DatabaseValues::ATT_Cardinality],
        $attributeSql[DatabaseValues::ATT_AvgLength],
        $attributeSql[DatabaseValues::ATT_Highest],
        $attributeSql[DatabaseValues::ATT_Lowest]
      );
      $table->addAttribute($attribute);
    }
    $database->addTable($table);
  }
  return $database;
}

function getClusterParameters($clusterId, $nRE, $nEC) {
  $cluster = DatabaseQuery::getCluster($clusterId);
  if (!is_null($cluster)) {
    $parameters = new CostFunctionParameters(
      (int) $cluster[DatabaseValues::CL_N_R],
      (int) $cluster[DatabaseValues::CL_N_RN],
      (int) $cluster[DatabaseValues::CL_N_C],
      (int) $cluster[DatabaseValues::CL_Rf],
      function ($nProc) use ($clusterId) { // discR(#Proc)
        $values = DatabaseQuery::getThroughputs($clusterId, DatabaseValues::TR_Type_DR);
        $maxProc = max(array_keys($values));
        if ($nProc <= $maxProc) {

        return $values[$nProc-1] *1024*1024;
        } else {

          return $values[$maxProc-1] *1024*1024;
        }
      },
      function ($nProc) use ($clusterId) { // discW(#Proc)
        $values = DatabaseQuery::getThroughputs($clusterId, DatabaseValues::TR_Type_DW);
        $maxProc = max(array_keys($values));
        if ($nProc <= $maxProc) {
          return $values[$nProc-1] *1024*1024;
        } else {
          return $values[$maxProc-1] *1024*1024;
        }
      },
      function ($nProc) use ($cluster) { // netI(#Proc)
        return $nProc <= 0 ? $cluster[DatabaseValues::CL_IntraRSpeed] : $cluster[DatabaseValues::CL_IntraRSpeed] / $nProc;
      },
      function ($nProc) use ($cluster) { // netE(#Proc)
        return $nProc <= 0 ? $cluster[DatabaseValues::CL_ExtraRSpeed] : $cluster[DatabaseValues::CL_ExtraRSpeed] / $nProc;
      },
      (int) $cluster[DatabaseValues::CL_N_SB],
      (float) $cluster[DatabaseValues::CL_SCmp],
      (float) $cluster[DatabaseValues::CL_FCmp],
      (float) $cluster[DatabaseValues::CL_HSel],
      $nRE,
      $nEC);
  }
  return $parameters;
}

function seconds($a) {
return formatNumber($a);
}

function seconds_old($a) {
  $sec = $a;
  $unim = array("s","m","h");
  $c = 0;
  while ($a>=60 && $c<sizeof($unim)-1) {
    $c++;
    $a = $a/60;
  }
  return number_format($a,($c ? 2 : 0),",",".")." ".$unim[$c] . " (" . formatNumber($sec) . "s)";
}

function formatNumber($number) {
  return number_format($number, 2, '.'," ");
}
?>
