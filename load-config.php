<?php
ob_start();
if(!isset($_SESSION)) {
    session_start();
}

//set timezone
date_default_timezone_set('Europe/London');

//database credentials
if ( ! defined( 'DBHOST' ) ) {
	define('DBHOST','localhost');
	define('DBUSER','sparkcost_user');
	define('DBPASS','sparkcost_user');
	define('DBNAME','sparkcost');
	define('DBPORT','3307');
}

//application address
if ( ! defined( 'SITEEMAIL' ) ) {
	define('SITEEMAIL','test.sparkcost@gmail.com');
}

if ( ! defined( 'ABSPATH' ) ) {
  /** Define ABSPATH as this file's directory */
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
  /** Define RELPATH as this file's directory */
	define( 'RELPATH', '/sparktune/' );
  /** Define URL for the web complete url */
	define('URL',"http://".$_SERVER['HTTP_HOST'].RELPATH);
}

/** Define RESPATH as this file's directory */
if ( ! defined( 'RESPATH' ) ) {
	define( 'RESPATH', RELPATH.'res/' );
	define( 'ABSRESPATH', ABSPATH.'res/' );
}
/** Define AUTHPATH as this file's directory */
if ( ! defined( 'AUTHPATH' ) ) {
	define( 'AUTHPATH', RELPATH.'loginregister-manager/' );
	define( 'ABSAUTHPATH', ABSPATH.'loginregister-manager/' );
}
/** Define TEMPLATEPATH as this file's directory */
if ( ! defined( 'TEMPLATEPATH' ) ) {
	define( 'TEMPLATEPATH', RELPATH.'template/' );
	define( 'ABSTEMPLATEPATH', ABSPATH.'template/' );
}
/** Define STYLEPATH as this file's directory */
if ( ! defined( 'STYLEPATH' ) ) {
	define( 'STYLEPATH', TEMPLATEPATH.'styles/' );
	define( 'ABSSTYLEPATH', TEMPLATEPATH.'styles/' );
}

try {
	//create PDO connection
	$db = new PDO("mysql:host=".DBHOST.";port=" . DBPORT . ";dbname=".DBNAME, DBUSER, DBPASS);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	//show error
    echo '<p class="bg-danger">'.$e->getMessage().'</p>';
    exit;
}

//include the user class, pass in the database connection
include(ABSAUTHPATH . 'classes/user.php');
include(ABSAUTHPATH . 'classes/phpmailer/mail.php');
$user = new User($db);
?>
